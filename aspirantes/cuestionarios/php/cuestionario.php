<?php
//UTF-8
    header("Content-type: text/html; charset=utf8");

//Conexión
    include("../../php/conect.php");

//Funcion llamada
    $funcion=$_GET['funcion'];

//Funciones
switch($funcion){
    case 'consultarCampos':
        consultarCampos();
        break;
    
    case 'enviarDatos':
        enviarDatos();
        break;
    
    case 'consultarPrograma':
        consultarPrograma();
        break;
        
    case 'consultarPaises':
        consultarPaises();
        break;
    
    case 'consultarEstados':
        consultarEstados();
        break;
    
    case 'consultarUniversidades':
        consultarUniversidades();
        break;
        
    default:
        echo "La funcion que intenta llamar no existe: ".$funcion;
        break;
}

function enviarDatos(){
    //Session
    session_start();

    if(isset($_SESSION['pk'])){

        $Matricula=$_SESSION['pk'];
        //POST

            //Datos de contacto
            $Nombre=$_POST['inputNombre'];
            $ApellidoP=$_POST['inputApellidoPaterno'];
            $ApellidoM=$_POST['inputApellidoMaterno'];
            $Telefono=$_POST['inputTelClavePais']." ".$_POST['inputTelLada']." ".$_POST['inputTelNumero'];
            $Celular=$_POST['inputCelClavePais']." ".$_POST['inputCelLada']." ".$_POST['inputCelNumero'];
            $CorreoAlternativo=$_POST['inputCorreoAlternativo'];
            //Dirección
            $Pais=$_POST['inputPais'];
            $Estado=$_POST['inputEstado'];
            $Municipio=$_POST['inputMunicipio'];
            $Delegacion=$_POST['inputDelegacion'];
            $Calle=$_POST['inputCalle'];
            $Colonia=$_POST['inputColonia'];
            $NumeroExterior=$_POST['inputNumExt'];
            $NumeroInterior=$_POST['inputNumInt'];
            $CodigoPostal=$_POST['inputCP'];
            $Trabaja=$_POST['inputTrabaja'];
            $DireccionCompleta=$_POST['inputDireccionCompleta'];
            //Información Acádemica
            $CarreraMaestria=$_POST['inputCarreraMaestria'];
            $InstitucionAvalaMaestria=$_POST['inputInstitucionAvalaMaestria'];
            $CampusMaestria=$_POST['inputInstitucionCampusMaestria'];
            $PaisAvalaMaestria=$_POST['inputPaisOtorgaMaestria'];
            $PromedioMaestria=$_POST['inputPromedioGeneralMaestria'];
            $CarreraLicenciatura=$_POST['inputCarreraLicenciatura'];
            $InstitucionAvalaLicenciatura=$_POST['inputInstitucionAvalaLicenciatura'];
            $CampusLicenciatura=$_POST['inputInstitucionCampusLicenciatura'];
            $PaisAvalaLicenciatura=$_POST['inputPaisOtorgaLicenciatura'];
            $PromedioLicenciatura=$_POST['inputPromedioGeneralLicenciatura'];
            $CarreraEspecialidad=$_POST['inputCarreraEspecialidad'];
            $InstitucionAvalaEspecialidad=$_POST['inputInstitucionAvalaEspecialidad'];
            $CampusEspecialidad=$_POST['inputInstitucionCampusEspecialidad'];
            $PaisAvalaEspecialidad=$_POST['inputPaisOtorgaEspecialidad'];
            $PromedioEspecialidad=$_POST['inputPromedioGeneralEspecialidad'];
            $EstadoLicenciatura=$_POST['inputEstadoOtorgaLicenciatura'];
            $EstadoEspecialidad=$_POST['inputEstadoOtorgaEspecialidad'];
            $EstadoMaestria=$_POST['inputEstadoOtorgaMaestria'];
            $Sexo=$_POST['inputSexo'];
            $Modalidad=$_POST['inputModalidad'];
            $EgresoLicenciatura=$_POST['inputEstadoEgresoLicenciatura'];
            $EgresoEspecialidad=$_POST['inputEstadoEgresoEspecialidad'];
            $EgresoMaestria=$_POST['inputEstadoEgresoMaestria'];
            //Datos Adicionales
        //    $Maestria=$_POST['inputMaestria'];

        //Sentencia SQL
        $SqlInsertar="update alumnos set nombre=?,apellido_p=?,apellido_m=?,telefono=?,celular=?,correo_alt=?,pais=?,entidad_federativa=?,municipio=?,delegacion=?,calle=?,colonia=?,num_ext=?,num_int=?,cp=?,nombre_carrera_maestria=?,institucion_carrera_maestria=?,campus_carrera_maestria=?,pais_carrera_maestria=?,promedio_gral_maestria=?,nombre_carrera_licenciatura=?,institucion_carrera_licenciatura=?,campus_carrera_licenciatura=?,pais_carrera_licenciatura=?,promedio_gral_licenciatura=?,nombre_carrera_especialidad=?,institucion_carrera_especialidad=?,campus_carrera_especialidad=?,pais_carrera_especialidad=?,promedio_gral_especialidad=?,trabaja=?,direccion_completa=?,estado_institucion_licenciatura=?,estado_institucion_especialidad=?,estado_institucion_maestria=?,sexo=?,modalidad=?,estado_egreso_especialidad=?,estado_egreso_licenciatura=?,estado_egreso_maestria=? where pk_matricula=?;";
        
//        $SqlInsertar="update alumnos set telefono='{$Telefono}',celular='{$Celular}',correo_alt='$CorreoAlternativo',pais='{$Pais}',entidad_federativa='{$Estado}',municipio='{$Municipio}',delegacion='{$Delegacion}',calle='{$Calle}',colonia='{$Colonia}',num_ext='{$NumeroExterior}',num_int='{$NumeroInterior}',cp='{$CodigoPostal}',nombre_carrera_maestria='{$CarreraMaestria}',institucion_carrera_maestria='{$InstitucionAvalaMaestria}',campus_carrera_maestria='{$CampusMaestria}',pais_carrera_maestria='{$PaisAvalaMaestria}',promedio_gral_maestria='{$PromedioMaestria}',nombre_carrera_licenciatura='{$CarreraLicenciatura}',institucion_carrera_licenciatura='{$InstitucionAvalaLicenciatura}',campus_carrera_licenciatura='{$CampusLicenciatura}',pais_carrera_licenciatura='{$PaisAvalaLicenciatura}',promedio_gral_licenciatura='{$PromedioLicenciatura}',nombre_carrera_especialidad='{$CarreraEspecialidad}',institucion_carrera_especialidad='{$InstitucionAvalaEspecialidad}',campus_carrera_especialidad='{$CampusEspecialidad}',pais_carrera_especialidad='{$PaisAvalaEspecialidad}',promedio_gral_especialidad='{$PromedioEspecialidad}',trabaja='{$Trabaja}',direccion_completa='{$DireccionCompleta}',estado_institucion_licenciatura='{$EstadoLicenciatura}',estado_institucion_especialidad='{$EstadoEspecialidad}',estado_institucion_maestria='{$EstadoMaestria}',sexo='{$Sexo}',modalidad='{$Modalidad}',estado_egreso_especialidad='{$EgresoEspecialidad}',estado_egreso_licenciatura='{$EgresoLicenciatura}',estado_egreso_maestria='{$EgresoMaestria}' where pk_matricula='{$Matricula}';";

        //Array de datos
        
//        echo $SqlInsertar;
        $Datos=array($Nombre,$ApellidoP,$ApellidoM,$Telefono,$Celular,$CorreoAlternativo,$Pais,$Estado,$Municipio,$Delegacion,$Calle,$Colonia,$NumeroExterior,$NumeroInterior,$CodigoPostal,$CarreraMaestria,$InstitucionAvalaMaestria,$CampusMaestria,$PaisAvalaMaestria,$PromedioMaestria,$CarreraLicenciatura,$InstitucionAvalaLicenciatura,$CampusLicenciatura,$PaisAvalaLicenciatura,$PromedioLicenciatura,$CarreraEspecialidad,$InstitucionAvalaEspecialidad,$CampusEspecialidad,$PaisAvalaEspecialidad,$PromedioEspecialidad,$Trabaja,$DireccionCompleta,$EstadoLicenciatura,$EstadoEspecialidad,$EstadoMaestria,$Sexo,$Modalidad,$EgresoEspecialidad,$EgresoLicenciatura,$EgresoMaestria,$Matricula);
        
        $hecho="iniciando";

        //Inserción de datos
        if($stm = executeQueryArray($SqlInsertar,$Datos)){
            limpiarStm($stm);
            if($stm2 = executeQuery("update apartados set cuestionario=1 where fk_matricula='".$Matricula."';")){
                limpiarStm($stm2);
                
                //comprobar la nacionalidad y sus archivos
                //Si es mexicano
                if($Pais == "México"){
                    $hecho = "true";
                }else{
                    //consulta de documentos
                    $sql="select file_apostillado_certificado_licenciatura,file_apostillado_certificado_maestria from documentos where fk_matricula='".$Matricula."';";
                    $rs=resultQuery($sql);
                    $apostilladoLicenciatura=$rs[0]['file_apostillado_certificado_licenciatura'];
                    $apostilladoMaestria=$rs[0]['file_apostillado_certificado_maestria'];
                    
                    //Si falta algun documento
                    if($apostilladoMaestria!=1 || $apostilladoLicenciatura!=1){
                        //Actualiza la tabla de apartados de documentos
                        $sql1="update apartados set documentos=2 where fk_matricula='".$Matricula."';";
                        if($s=executeQuery($sql1)){
                            limpiarStm($s);
                            //Actualiza el estado del alumno
                            $sql1="update alumnos set estado='En proceso' where pk_matricula='".$Matricula."';";
                            if($s=executeQuery($sql1)){
                                limpiarStm($s);
                                $hecho = "true";
                            }else $hecho = "false1";
                        }else $hecho = "false2";
                    }
                }
                
                //Si trabaja o no
                if($Trabaja=="No"){
                    //consulta de documentos
                    $sql="select file_carta_jefe,fk_programa from documentos inner join alumnos on (pk_matricula = fk_matricula) where fk_matricula='".$Matricula."';";
                    $rs=resultQuery($sql);
                    $cartaJefe=$rs[0][0];
                    $Programa=$rs[0][1];
                    
                    //Si falta algun documento
                    if($cartaJefe != 1 && $Programa=="8"){
                        //Actualiza la tabla de apartados de documentos
                        $sql1="update apartados set documentos=2 where fk_matricula='".$Matricula."';";
                        if($s=executeQuery($sql1)){
                            limpiarStm($s);
                            //Actualiza el estado del alumno
                            $sql1="update alumnos set estado='En proceso' where pk_matricula='".$Matricula."';";
                            if($s=executeQuery($sql1)){
                                limpiarStm($s);
                                $hecho= "true";
                            }else $hecho = "false3";
                        }else $hecho = "false4";
                    }
                }else{
                    $hecho="true";
                }
                
                echo $hecho;
//                echo $SqlInsertar;
                ///////////////
            }else echo "errorApartados";
                    
        }else echo "false5";
    }else echo "salir";
}

function consultarCampos(){
    //Session
    session_start();

    if(isset($_SESSION['pk'])){

            $Matricula=$_SESSION['pk'];

            //Sentencia SQL para buscar campos
            $Sentencia="select * from alumnos where pk_matricula='".$Matricula."';";

            //Ejecutar busqueda
            echo resultQueryJson($Sentencia);
        }else echo "salir";
}

function consultarPrograma(){
    //Session
    session_start();
    
    if(isset($_SESSION['pk'])){
                //matricula
                $Matricula=$_SESSION['pk'];
                
                //consulta de datos
                $sqlDatos="SELECT alumnos.nombre,programas.pk_programa,programas.nombre,periodos.fecha_inicio,periodos.fecha_fin FROM `alumnos` INNER JOIN programas ON (alumnos.fk_programa=programas.pk_programa) INNER JOIN periodos ON (programas.pk_programa = periodos.fk_programa) WHERE pk_matricula='".$Matricula."';";
                
//                echo $sqlDatos;
                echo resultQueryJson($sqlDatos);
        }else echo "salir";
    }

function consultarPaises(){
    //Session
    session_start();

    if(isset($_SESSION['pk'])){

            //Sentencia SQL para buscar campos
            $Sentencia="select * from pais order by paisnombre;";

            //Ejecutar busqueda
            echo resultQueryJson($Sentencia);
        }else echo "salir";
}

function consultarEstados(){
    //Session
    session_start();
    header("Content-Type: text/html;charset=utf-8");
    if(isset($_SESSION['pk'])){
            
            //Pais
            $Pais=htmlspecialchars($_GET['pais']); ///Modificacion local
            $Pais=utf8_decode($Pais); ///Modificacion local
            $Pais="México";
            
            //Sentencia SQL para buscar campos
            $SQL="SELECT estado.estadonombre FROM pais inner join estado on (pais.id = estado.ubicacionpaisid) where pais.paisnombre='".$Pais."' order by estado.estadonombre;";

//            echo $SQL;
            //Ejecutar busqueda
            echo resultQueryJson($SQL);
        }else echo "salir";
}

function consultarUniversidades(){
    //Session
    session_start();

    if(isset($_SESSION['pk'])){
            
            //Pais
          $Estado=$_GET['estado'];
          if($Estado == "México")
          $Estado = "Estado de México";

            //Sentencia SQL para buscar campos
          $SQL="SELECT universidad from universidades where estado='".$Estado."' order by universidad;";

            
            //Ejecutar busqueda
           echo resultQueryJson($SQL);
        }else echo "salir";
}


?>