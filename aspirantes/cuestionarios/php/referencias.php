<?php
//UTF-8
    header("Content-type: text/html; charset=utf8");

//Conexión
    include("../../php/conect.php");

//Funcion llamada
    $funcion=$_GET['funcion'];

//Session
session_start();
if(isset($_SESSION['pk'])){
    //Funciones
    switch($funcion){
        case 'enviarFormulario':
            enviarFormulario();
            break;

        case 'consultarRecomendaciones':
            consultarRecomendaciones();
            break;

        case 'enviarRecomendacion':
            enviarRecomendacion();
            break;

        default:
            echo "La funcion que intenta llamar no existe: ".$funcion;
            break;
    }
}else echo "salir";

//Enviar correo del formulario
function enviarFormulario(){
    //Matricula
        $Matricula=$_SESSION['pk'];
    //ObtenerDatos
        $Correo;
        if(isset($_POST['inputReferenciaUno'])){
            $Correo=$_POST['inputReferenciaUno'];
            $Recomendador=$_POST['inputNombreUno'];
        }else if(isset($_POST['inputReferenciaDos'])){
            $Correo=$_POST['inputReferenciaDos'];
            $Recomendador=$_POST['inputNombreDos'];
        } 
    
        if($_GET['n'] == "Uno")
            $Referencia = 1;
        else if ($_GET['n'] == "Dos") $Referencia=2;
    
        $Clave= generarClave(6);
    //Consultar informacion del aspirante
    //Nombre de la carrera,siglas y nombre del aspirante
    $SQL="Select alumnos.nombre,alumnos.apellido_p,alumnos.apellido_m,alumnos.correo,programas.nombre,programas.siglas from alumnos inner join programas on (alumnos.fk_programa = programas.pk_programa) where alumnos.pk_matricula ='".$Matricula."';";
    
    //Ejecutar consulta
    $Datos=resultQuery($SQL);
    
    foreach($Datos as $dato){
        $Nombre = $dato[0]." ".$dato[1]." ".$dato[2];
        $CorreoAspirante = $dato[3];
        $NombreCarrera = $dato[4];
        $Siglas = $dato[5];
    }
    
    //Envio de Correo
    // titulo
    $titulo = 'Proceso de Admisión';

    // mensaje
    $mensaje = '<html>
    <head>
        <title>Proceso de admisión</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>

    <body>
        <table style="width: 600px; padding: 10px; margin:0 auto; border-collapse: collapse;">
            <tr>
                <td style="text-align: center">
                    <img src="http://aspirantes.posgradosfcn-uaq.com/img/logos/icouaq.png" width="100px" height="100px">
                </td>
                <td >
                    <h2  style="text-align: center; color: #3498db; margin: 0 0 12px; font-size: 30px; font-family: inherit">Carta de recomendación.</h2>
                    <h4 style="text-align: center; color: #3498db; margin: 0 0 12px; font-size: 20px; font-family: inherit">'.$NombreCarrera.'</h4>
                </td>
                <td style="text-align: center">
                    <img src="http://aspirantes.posgradosfcn-uaq.com/img/logos/icofc.png" width="100px" height="100px">
                </td>
            </tr>
        </table>
        <br>
        <table style="width: 600px; margin:0 auto; border-collapse: collapse;" >

            <tr>
                <td style="background-color: #ecf0f1">
                    <div style="color: #34495e; margin: 4% 10% 2%; text-align: justify;font-family: sans-serif">
                        <h3 style="color: #e67e22; margin: 0 0 7px">Hola '.$Recomendador.' usted fue elegido por '.$Nombre.' para contestar un pequeño formulario de recomendación.</h3>
                        <p style="margin: 2px; font-size: 15px">
                            '.$Nombre.' se encuentra en el proceso de selección de aspirantes para ingresar a  '.$NombreCarrera.' en la Facultad de Ciencias Naturales de la Universidad Autónoma de Querétaro y nos ha proporcionado su dirección de correo electrónico para enviarle el formulario de recomendación y asi el aspirante pueda continuar con el proceso de admisión. 
                            <br>
                        </p>
                        <ul style="font-size: 14px;  margin: 10px 0">
                            <li>Es necesario que responda el formulario de lo contrario el aspirante no podrá continuar con el proceso de admisión.</li>
                            <li>En caso de no estar de acuerdo en contestar el formulario favor de avisar al aspirante que lo selecciono para realizar el cambio y que otra persona pueda recomendarla.</li>
                            <li>Sus datos no serán divulgados por ningún motivo.</li>
                        </ul>
                        <hr>
                        <div style="text-align: center">
                         <img src="http://aspirantes.posgradosfcn-uaq.com/img/logos/'.$Siglas.'.png" width="200px" height="200px">
                        </div>
                        <br clear="all">
                        <div style="width: 100%; text-align: center">
                            <a style="text-decoration: none; border-radius: 5px; padding: 11px 23px; color: white; background-color: #3498db" href="http://aspirantes.posgradosfcn-uaq.com/cuestionarios/formRecomendacion.php?n='.$Referencia.'&m='.$Matricula.'&c='.$Clave.'">Responder el formulario</a>
                        </div>
                        <p style="color: #b3b3b3; font-size: 12px; text-align: center;margin: 30px 0 0">UAQ, Facultad de Ciencias Naturales.</p>
                    </div>
                </td>
            </tr>
        </table>
    </body>
    </html>';

    // Para enviar un correo HTML, debe establecerse la cabecera Content-type
    $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

    // Cabeceras adicionales
    $cabeceras .= 'To:'.$Nombre.'<'.$Correo.'>'."\r\n";
    $cabeceras .= 'From: Facultad de Ciencias Naturales <fcn@posgradosfcn-uaq.com>' . "\r\n";
    
    //Enviar Correo
    $SQL="update recomendaciones set estado=?,clave=?,correo=? where fk_matricula=? and fk_referencia=?";
    $Datos = array("En espera",$Clave,$Correo,$Matricula,$Referencia);
    if($s = executeQueryArray($SQL,$Datos)){
        limpiarStm($s);
        
        //Correo
        $enviado = mail($Correo, $titulo, $mensaje, $cabeceras);
        if($enviado){
            //Cambiar estado de apartados
            $sql="update apartados set referencias='2' where fk_matricula='".$Matricula."';";
            if(executeQuery($sql)){
                echo "Hecho";
            }else{
                echo "error";
            }
        }else{
            echo "error";
        }
    }else{
        echo "error";
    }
}

//Generar clave aleatoria
function generarClave($length) { 
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length); 
}

//Consultar el estado de las recomendaciones
function consultarRecomendaciones(){
    //Matricula
    $Matricula=$_SESSION['pk'];
    
    //Consulta SQL
    $SQL="Select estado,correo from recomendaciones where fk_matricula='".$Matricula."';";
    
    echo resultQueryJson($SQL);
}

//Registra la recomendacion
function enviarRecomendacion(){
    //Obtener Datos
    $Matricula=$_POST['Matricula'];
    $Descripcion=$_POST['inputDescripcion'];
    $Conocimientos=$_POST['radioConocimientos'];
    $Dedicacion=$_POST['radioDedicacion'];
    $Comunicacion=$_POST['radioComunicacion'];
    $Iniciativa=$_POST['radioIniciativa'];
    $Perseverancia=$_POST['radioPerseverancia'];
    $Actitud=$_POST['radioActitud'];
    $Habilidades=$_POST['inputHabilidadesDebilidades'];
    $Nombre=$_POST['inputNombreRecomendador'];
    $Lugar=$_POST['inputLugarTrabajo'];
//    $Direccion=$_POST['inputDireccion'];
    $Telefono=$_POST['inputTelClavePais'].$_POST['inputTelLada'].$_POST['inputTelNumero'];
    $Clave=$_POST['Clave'];
    $Referencia=$_POST['Referencia'];
    
    //SQL
    $SQL="update recomendaciones set descripcion=?,conocimientos=?,dedicacion=?,comunicacion=?,iniciativa=?,perseverancia=?,actitud=?,habilidades_debilidades=?,nombre_recomendante=?,lugar_trabajo=?,telefono=?,estado=?,clave=? where fk_matricula=? and estado='En espera' and clave=?;";
    
    $Datos=array($Descripcion,$Conocimientos,$Dedicacion,$Comunicacion,$Iniciativa,$Perseverancia,$Actitud,$Habilidades,$Nombre,$Lugar,$Telefono,"Respondida","",$Matricula,$Clave);
    
    $estado;
    
    if($s=executeQueryArray($SQL,$Datos)){
        $estado="Hecho";
    }else{
        $estado="Error";
    }
    
    //Verificar que esten las dos recomendaciones respondidas
    $SQLcontar="Select estado from recomendaciones where fk_matricula='".$Matricula."';";
    
    $Estados=resultQuery($SQLcontar);
    $contar="validado";
    
    foreach($Estados as $estado){
        if($estado['estado'] != "Respondida"){
            $contar="no validado";
        }
    }
    
    if($contar == "validado"){
        $SQLactualizar="update apartados set referencias='1' where fk_matricula='".$Matricula."';";
        if(executeQuery($SQLactualizar)){
            $estado="Hecho";
        }else $estado="Error";
    }else if($contar == "no validado"){
        $SQLactualizar="update apartados set referencias='2' where fk_matricula='".$Matricula."';";
        if(executeQuery($SQLactualizar)){
            $estado="Hecho";
        }else $estado="Error";
    }
    
    echo $estado;
}
?>