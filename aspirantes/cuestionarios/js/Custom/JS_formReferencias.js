/*****************************************************
DOCUMENTO LISTO
******************************************************/
$(document).ready(function () {
    var disponible = $("#opcion").val();
    console.log(disponible);
    if (disponible == "Disponible") {
        //Mostrar Pagina
        document.getElementById('cargando').style.display = 'none';
        $('html, body').css({
            'overflow': 'auto'
        });
    } else {
        swal("Atención", "El aspirante ha realizado algunos cambios de recomendación y el formulario ya no esta disponible para usted.", "warning", {
            button: "Aceptar",
        }).then((value) => {
            location.href = "../";
        });
    }
});

/*****************************************************
VALIDA EL FORMULARIO COMPLETO
******************************************************/
$("#btnEnviar").click(function () {
    $validator = $("#formularioRecomendacion").validate({
        messages: {
            radioConocimientos: ".   .  .Seleccione una opción.",
            radioDedicacion: ".   .  .Seleccione una opción.",
            radioComunicacion: ".   .  .Seleccione una opción.",
            radioIniciativa: ".   .  .Seleccione una opción.",
            radioPerseverancia: ".   .  .Seleccione una opción.",
            radioActitud: ".   .  .Seleccione una opción.",
            politica: "Es necesario que lea y acepte las políticas de privacidad."
        }
    });

    var $valid = $("#formularioRecomendacion").valid();

    if ($valid) {
        enviarRecomendacion();
    } else {
        $validator.focusInvalid();
    };
});

/*****************************************************
ENVIA LA PETICIÓN AL SERVIDOR Y REGISTRA
LA RECOMENDACIÓN
******************************************************/
function enviarRecomendacion() {
    contar=0;
    var url = "./php/referencias.php?funcion=enviarRecomendacion"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: $("#formularioRecomendacion").serialize(), // Adjuntar los campos del formulario enviado.
        success: function (data) {
            //si se guardaron los datos correctamente
            if (data == "Hecho") {
                swal("Respuesta guardada", "Gracias por responder el formulario.", "success", {
                    button: "Aceptar",
                }).then((value) => {
                    location.href = "../";
                });
            }else if(data=="Error"){
                
                if(contar>3){
                   swal("Disculpe el inconveniente", "No se ha podido guardar su respuesta, por favor recargue la pagina e inténtelo de nuevo.", "error", {
                    button: "Aceptar",
                }).then((value) => {
                    location.reload(true);
                }); 
                }
                
                enviarRecomendacion();
                contar++;
            }
        }
    });
}
