/*****************************************************
DOCUMENTO LISTO
******************************************************/ 
$(document).ready(function () {
     consultarCampos();
 });

/*****************************************************
VARIABLES GLOBALES
******************************************************/
 var fullApartados = false;
 var nombreCarrera = "";
 var estado;

/*****************************************************
CONSULTA LA INFROMACIÓN DEL ASPIRANTE Y CALCULA
LOS DIAS RESTANTES DEL PROGRAMA
******************************************************/
 function consultarCampos() {
     //Informacion del aspirante
     var url = "./php/index.php?funcion=aspirante"; // El script a dónde se realizará la petición.
     $.ajax({
         type: "POST",
         url: url,
         success: function (data) {
             console.log("data: "+data);
             if (data != "salir") {
                 var datos = JSON.parse(data);
                 console.log(datos);
                 ///Enviar datos al formulario
                 $("#nombreCarrera").html(datos[0].nombre);
                 $("#nombreAspirante").html(datos[0][0]);
                 nombreCarrera = datos[0].nombre;
                 estado = datos[0].estado;
                 apartados(nombreCarrera);
             } else location.href = "../login.php";
         }
     });



     //Calcular dias restantes
     var url = "./php/index.php?funcion=diasRestantes"; // El script a dónde se realizará la petición.
     $.ajax({
         type: "POST",
         url: url,
         success: function (data) {
             if (data != "salir") {
                 var datos = JSON.parse(data);
                 console.log(datos);
                 var diasRestantes = datos[0].dias_restantes;
                 var fechaLimite = datos[0].fecha_limite;
                 if (diasRestantes == 1) {
                     $("#diasRestantes").html("Queda un día restante para que el registro de aspirantes termine.");
                 } else if (diasRestantes > 1) {
                     $("#diasRestantes").html("Quedan " + diasRestantes + " días restantes para que el registro de aspirantes termine.");
                 } else if (diasRestantes == 0) {
                     $("#diasRestantes").html("Hoy es el ultimo día para completar el registro de aspirantes.");
                 } else if (diasRestantes < 0) {
                     swal("Tiempo terminado", "La fecha limite (" + fechaLimite + ") para proporcionar la información se ha cumplido.", "warning", {
                         button: "Aceptar",
                     }).then((value) => {
                         location.href = "php/salir.php";
                     });
                 }
             } else location.href = "../login.php";
         }
     });
 }

/*****************************************************
VALIDA EL FORMULARIO DEL MODAL DE CONTRASEÑA
Y REALIZA EL CAMBIO DE CONTRASEÑA
******************************************************/
 $("#btnCambiarContraseña").click(function () {
     $("#procesando").addClass("procesando");
     console.log("cargando");
     $("#formularioContraseña").validate({
         messages: {
             inputContraseñaNuevaVerificar: "Verifique que la contraseña coincida con la anterior."
         }
     });
     var $valid = $("#formularioContraseña").valid();

     if ($valid) {
         var url = "./php/index.php?funcion=cambiarContraseña"; // El script a dónde se realizará la petición.
         $.ajax({
             type: "POST",
             url: url,
             data: $("#formularioContraseña").serialize(), // Adjuntar los campos del 
             success: function (data) {
                 if (data != "salir") {
                    $("#procesando").removeClass("procesando");
                     
                     switch (data) {
                         case "true":
                             swal("Contraseña cambiada.", "Se actualizo correctamente su contraseña.", "success", {
                                 button: "Aceptar",
                             }).then((value) => {
                                 $("#modalContraseña").modal('hide');
                                 $("#inputContraseñaNueva").val(null);
                                 $("#inputContraseñaNuevaVerificar").val(null);
                                 $("#inputContraseñaAntigua").val(null);
                             });
                             break;
                         case "false":
                             swal("Verifique su contraseña actual.", "No se pudo actualizar su contraseña debido a que su contraseña actual no es correcta.", "error", {
                                 button: "Aceptar",
                             });
                             break;
                         default:
                             swal("Lo sentimos.", "No se pudo actualizar su contraseña por favor recargue la pagina e intentelo de nuevo.", "error", {
                                 button: "Aceptar",
                             });
                             break;
                     }

                 } else location.href = "../login.php";
             }
         });
     } else {
         $validator.focusInvalid();
     }

 });

/*****************************************************
VERIFICA EL ESTADO DE LOS APARTADOS DEL ASPIRANTE
Y QUE ESTE EN TIEMPO Y FORMA DE PROPORCIONAR
INFORMACIÓN PARA EL PROCESO DE ADMISIÓN
******************************************************/
 function apartados(nombreCarrera) {
     //Informacion de los apartados contestados
     var url = "./php/index.php?funcion=apartados"; // El script a dónde se realizará la petición.
     $.ajax({
         type: "POST",
         url: url,
         success: function (data) {
             if (data != "salir") {
                 var datos = JSON.parse(data);
                 console.log(datos);

                 if (datos[0].cuestionario == 1) {
                     $("#iconoCuestionario").removeClass("text-info").addClass("text-success");
                     $("#mensajeCuestionario").html("Usted ha completado este apartado, si lo contesta de nuevo sobrescribra su información.");
                     $("#smallIconoCuestionario").removeClass("fa-spinner").removeClass("fa-spin").addClass("fa-check-circle");
                     $("#smallMensajeCuestionario").html("Completado");
                     $("#btnDocumentos").removeClass("disabled");

                     if (datos[0].documentos == 0) {
                         $("#mensajeDocumentos").html("Estamos en espera de que nos proporcione estos documentos.");
                     }
                 }

                 if (datos[0].documentos == 1) {
                     $("#iconoDocumentos").removeClass("text-info").addClass("text-success");
                     $("#mensajeDocumentos").html("Usted ha completado este apartado, si adjunta un documento sobrescribira el anterior.");
                     $("#smallIconoDocumentos").removeClass("fa-spinner").removeClass("fa-spin").addClass("fa-check-circle");
                     $("#smallMensajeDocumentos").html("Completado");
                 }

                 if (datos[0].documentos == 2) {
                     $("#iconoDocumentos").removeClass("text-info").addClass("text-warning");
                     $("#mensajeDocumentos").html("Aun faltan documentos por adjuntar y seguimos en espera de sus documentos.");
                 }

                 if (datos[0].referencias == 1) {
                     $("#iconoReferencias").removeClass("text-info").addClass("text-success");
                     $("#mensajeReferencias").html("Hemos recibido las recomendaciones correctamente.");
                     $("#smallIconoReferencias").removeClass("fa-spinner").removeClass("fa-spin").addClass("fa-check-circle");
                     $("#smallMensajeReferencias").html("Completado");
                 } else if (datos[0].referencias == 2) {
                     $("#iconoReferencias").removeClass("text-info").addClass("text-warning");
                     $("#mensajeReferencias").html("Estamos esperando que responan los formularios de recomendación.");
                 }

                 if (datos[0].referencias == 1 && datos[0].cuestionario == 1 && datos[0].documentos == 1) {
                     $("#mensaje").addClass("alert-success").append("<p><strong>Proceso terminado</strong><br>Gracias por haber proporcionado toda la información necesaria.El coordinador de " + nombreCarrera + " revisara personalmente dicha información y <strong>se le enviará un correo electrónico con el resultado de tu postulación.</strong></p>");

                     if (estado == "En proceso") {
                         apartadosCompletos();
                     }
                 } else {
                     $("#mensaje").addClass("alert-warning").append("<p><strong>Atención.</strong><br>Su postulación será enviada  a coordinación solo cuando nos haya proporcionado toda la información solicitada en los <strong>tres apartados de arriba.</strong></p>");
                 }

                 //Mostrar Pagina
                 document.getElementById('cargando').style.display = 'none';
                 $('html, body').css({
                     'overflow': 'auto'
                 });
                 
                 //Bloquar apartados cuando se acepto o rechazo
                 if(estado=="Aceptado" || estado=="No aceptado"){
                     $("#btnCuestionario").addClass("disabled");
                     $("#btnDocumentos").addClass("disabled");
                     $("#btnReferencias").addClass("disabled");
                 }

             } else location.href = "../login.php";
         }
     });
 }

/*****************************************************
CAMBIA EL ESTADO DEL ASPIRANTE A "En espera" CUANDO
DETECTA QUE YA HA CONTESTADO LOS TRES APARTADOS
******************************************************/
 function apartadosCompletos() {
     //Calcular dias restantes
     var url = "./php/index.php?funcion=apartadosCompletos"; // El script a dónde se realizará la petición.
     $.ajax({
         type: "POST",
         url: url,
         success: function (data) {
             if (data != "salir") {
                 if (data == "echo") {
                     swal("Proceso terminado", "Gracias por haber proporcionado toda la información necesaria.El coordinador de " + nombreCarrera + " revisara personalmente dicha información y se le enviará un correo electrónico con el resultado de tu postulación.", "success", {
                         button: "Aceptar",
                     });
                 } else {
                     swal("Lo sentimos", "El servidor no esta respondiendo correctamente por favor recarga la pagina para completar tu proceso.", "error", {
                         button: "Aceptar",
                     }).then((value) => {
                         location.href = "./";
                     });
                 }
             } else location.href = "../login.php";
         }
     });
 }
