/*****************************************************
DOCUMENTO LISTO
******************************************************/
$(document).ready(function () {
    consultarCampos(); //Llena los datos que han sido contestados
//    consultarPrograma(); //Establece las configuraciones especiales de cada programa
    //            verCamposCarreras(); //Muestra u oculta los apartados especiales

    //Configuracion del formulario con wizard y la validacion
    var $validator = $("#formCuestionario").validate();

    /*****************************************************
    ESTABLECE LAS CONFIGURACIONES DEL WIZZARD Y 
    VALIDACIONES DEL FORMULARIO
    ******************************************************/
    $('#rootwizard').bootstrapWizard({

        'tabClass': 'nav nav-tabs',
        'onNext': function (tab, navigation, index) {
            var $valid = $("#formCuestionario").valid();

            if (!$valid) {
                $validator.focusInvalid();
                var $valid = $("#barProgreso").removeClass("bg-success").addClass("bg-danger");
                return false;
            } else {
                var $valid = $("#barProgreso").removeClass("bg-danger").addClass("bg-success");
            }
        },
        'onTabClick': function (tab, navigation, index) {
            //                    $("#formCuestionario").validate();
            //                    var $valid = $("#formCuestionario").valid();
            //
            //                    if (!$valid) {
            //                        $validator.focusInvalid();
            //                        var $valid = $("#barProgreso").removeClass("bg-success").addClass("bg-danger");
            //                        return false;
            //                    } else {
            //                        var $valid = $("#barProgreso").removeClass("bg-danger").addClass("bg-success");
            //                    }

            return false;
        },
        'onTabShow': function (tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index;
            var $percent = ($current / $total) * 100;
            $('#rootwizard .progress-bar').css({
                width: $percent + '%'
            });
        }
    });

    /*****************************************************
    VALIDA TODO EL CUESTIONARIO Y ENVIA LA PETICIÃ“N
    AJAX A EL SERVIDOR PARA REGISTRAR LA INFROMACIÃ“N
    ******************************************************/
    $('#rootwizard .finish').click(function () {
        var $validator = $("#formCuestionario").validate();
        var $valid = $("#formCuestionario").valid();

        if ($valid) {

            completarBarra(); //Llena la barra al 100%

            showDialog(); //Muestra el gif de procesando
            var url = "./php/cuestionario.php?funcion=enviarDatos"; // El script a dónde se realizarÃ¡ la petición.
            $.ajax({
                type: "POST",
                url: url,
                data: $("#formCuestionario").serialize(), // Adjuntar los campos del formulario enviado.
                success: function (data) {
                    hideDialog(); //oculta el gif de procesando
                    if (data == "true") {
                        swal("Guardado", "Se guardarón su datos correctamente.", "success", {
                            button: "Aceptar",
                        }).then((value) => {
                            location.href = "index.html#about";
                        });
                    } else {
                        //                                swal("Error", "Lo sentimos no se guardaron sus datos por favor inténtelo de nuevo.", "error", {
                        //                                    button: "Aceptar",
                        //                                });
                        swal("Error", data, "error", {
                            button: "Aceptar",
                        });
                    }
                    hideDialog();
                }
            });
            return false;
        } else {
            $validator.focusInvalid();
        }
    });
});

/*****************************************************
VARIABLES GLOBALES
******************************************************/
var paisG;
var paisGMaestria;
var paisGLicenciatura;
var paisGEspecialidad;
var estadoG;
var estadoLicenciatura;
var estadoMaestria;
var estadoEspecialidad;
var institucionLicenciatura;
var institucionMaestria;
var institucionEspecialidad;

/*****************************************************
MUESTRA EL GIF PROCESANDO
******************************************************/
function showDialog() {
    $("#botones").addClass("procesando");
}

/*****************************************************
OCULTA EL GIF PROCESANDO
******************************************************/
function hideDialog() {
    $("#botones").removeClass("procesando");
}

/*****************************************************
ESTABLECE LA BARRA DE AVANCE EN EL 100%
******************************************************/
function completarBarra() {
    $('#rootwizard .progress-bar').css({
        width: '100%'
    });
}

/*****************************************************
CONSULTA LOS DATOS DEL ASPIRANTE Y LOS COLOCA
EN LOS CAMPOS CORRESPONDIENTES EN CASO DE YA
HABERLOS CONTESTADO ANTES
******************************************************/
function consultarCampos() {
    var url = "./php/cuestionario.php?funcion=consultarCampos"; // El script a dónde se realizarÃ¡ la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {

            if (data != "salir") {
                var datos = JSON.parse(data);
                //                        //console.log(datos);

                ///Enviar datos al formulario
                $("#inputNombre").val(datos[0].nombre);
                $("#inputApellidoPaterno").val(datos[0].apellido_p);
                $("#inputApellidoMaterno").val(datos[0].apellido_m);
                $("#inputCorreo").val(datos[0].correo);
                var telNumber = datos[0].telefono.split(' ');
                $("#inputTelClavePais").val(telNumber[0]);
                $("#inputTelLada").val(telNumber[1]);
                $("#inputTelNumero").val(telNumber[2]);
                var celNumber = datos[0].celular.split(' ');
                $("#inputCelClavePais").val(celNumber[0]);
                $("#inputCelLada").val(celNumber[1]);
                $("#inputCelNumero").val(celNumber[2]);
                $("#inputCorreoAlternativo").val(datos[0].correo_alt);
                $("#inputPais").val(datos[0].pais);
                $("#inputEstado").val(datos[0].entidad_federativa);
                $("#inputMunicipio").val(datos[0].municipio);
                $("#inputDelegacion").val(datos[0].delegacion);
                $("#inputCalle").val(datos[0].calle);
                $("#inputColonia").val(datos[0].colonia);
                $("#inputNumExt").val(datos[0].num_ext);
                $("#inputNumInt").val(datos[0].num_int);
                $("#inputCP").val(datos[0].cp);
                $("#inputCarreraMaestria").val(datos[0].nombre_carrera_maestria);
                $("#inputInstitucionAvalaMaestria").val(datos[0].institucion_carrera_maestria);
                $("#inputInstitucionCampusMaestria").val(datos[0].campus_carrera_maestria);
                $("#inputPaisOtorgaMaestria").val(datos[0].pais_carrera_maestria);
                $("#inputPromedioGeneralMaestria").val(datos[0].promedio_gral_maestria);
                $("#inputCarreraLicenciatura").val(datos[0].nombre_carrera_licenciatura);
//                $("#inputInstitucionAvalaLicenciatura").val(datos[0].institucion_carrera_licenciatura);
                $("#inputInstitucionCampusLicenciatura").val(datos[0].campus_carrera_licenciatura);
                
                $("#inputPromedioGeneralLicenciatura").val(datos[0].promedio_gral_licenciatura);
                $("#inputCarreraEspecialidad").val(datos[0].nombre_carrera_especialidad);
                $("#inputInstitucionAvalaEspecialidad").val(datos[0].institucion_carrera_especialidad);
                $("#inputInstitucionAvalaLicenciatura").val(datos[0].institucion_carrera_licenciatura);
                $("#inputInstitucionCampusEspecialidad").val(datos[0].campus_carrera_especialidad);
                $("#inputPaisOtorgaEspecialidad").val(datos[0].pais_carrera_especialidad);
                $("#inputPromedioGeneralEspecialidad").val(datos[0].promedio_gral_especialidad);
                
                $("#inputTrabaja").val(datos[0].trabaja);
                $("#inputDireccionCompleta").val(datos[0].direccion_completa);
                $("#inputSexo").val(datos[0].sexo);
                $("#inputModalidad").val(datos[0].modalidad);
                $("#inputEstadoEgresoEspecialidad").val(datos[0].estado_egreso_especialidad);
                $("#inputEstadoEgresoLicenciatura").val(datos[0].estado_egreso_licenciatura);
                $("#inputEstadoEgresoMaestria").val(datos[0].estado_egreso_maestria);
                
                showInfoModalidad(datos[0].modalidad);
                
                
                //varaibels globales de paises y estados para mostrar en los select
                paisG = datos[0].pais;
                paisGMaestria = datos[0].pais_carrera_maestria;
                paisGLicenciatura = datos[0].pais_carrera_licenciatura;
                paisGEspecialidad = datos[0].pais_carrera_especialidad;
                estadoG = datos[0].entidad_federativa;
                estadoLicenciatura=datos[0].estado_institucion_licenciatura;
                estadoEspecialidad=datos[0].estado_institucion_especialidad;
                estadoMaestria=datos[0].estado_institucion_maestria;
                institucionLicenciatura=datos[0].institucion_carrera_licenciatura;
                institucionEspecialidad=datos[0].institucion_carrera_especialidad;
                institucionMaestria=datos[0].institucion_carrera_maestria;
                
                //verificar instituciones de procedencia
               console.log("Verificar estado: "+paisGLicenciatura);
               console.log("estado lic: "+estadoLicenciatura);
                verificarInstitucionProcedencia(paisGLicenciatura,"Licenciatura");
                verificarInstitucionProcedencia(paisGEspecialidad,"Especialidad");
                verificarInstitucionProcedencia(paisGMaestria,"Maestria");
                
                //consulta de estados
                if(paisGLicenciatura!=""){
//                    consultarEstados(paisGLicenciatura,"#inputEstadoOtorgaLicenciatura",0);
                }
                
                //verifica si muestra o no los campos de direccion
                verificarDireccion(paisG);

                //llena el select de estados de procedencia
                if(paisG != ""){
                    consultarEstados(paisG,"#inputEstado",0);
                }
                
//                verCamposCarreras();
                
                

            } else location.href = "../login.php";
        }
    });

    /*****************************************************
    LLENA LOS SELECT PAIS CON TODOS LOS PAISES DEL MUNDO
    ******************************************************/
    var url = "./php/cuestionario.php?funcion=consultarPaises"; // El script a dónde se realizarÃ¡ la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {

            if (data != "salir") {
                var datos = JSON.parse(data);
                //console.log(datos);
                $.each(datos, function (index) {
                    //Nombre del pais
                    var pais = datos[index].paisnombre;

                    //Pais de procedencia
                    if (pais != undefined) {
                        if (paisG == pais) {
                            $("#inputPais").append("<option value='" + pais + "' selected>" + pais + "</option>");
                        } else {
                            $("#inputPais").append("<option value='" + pais + "'>" + pais + "</option>");
                        }
                    }

                    //Pais otorga maestria
                    if (pais != undefined) {
                        if (paisGMaestria == pais) {
                            $("#inputPaisOtorgaMaestria").append("<option value='" + pais + "' selected>" + pais + "</option>");
                        } else {
                            $("#inputPaisOtorgaMaestria").append("<option value='" + pais + "'>" + pais + "</option>");
                        }
                    }

                    //Pais otorga licenciatura
                    if (pais != undefined) {
                        if (paisGLicenciatura == pais) {
                            $("#inputPaisOtorgaLicenciatura").append("<option value='" + pais + "' selected>" + pais + "</option>");
                        } else {
                            $("#inputPaisOtorgaLicenciatura").append("<option value='" + pais + "'>" + pais + "</option>");
                        }
                    }

                    //Pais otorga especialidad
                    if (pais != undefined) {
                        if (paisGEspecialidad == pais) {
                            $("#inputPaisOtorgaEspecialidad").append("<option value='" + pais + "' selected>" + pais + "</option>");
                        } else {
                            $("#inputPaisOtorgaEspecialidad").append("<option value='" + pais + "'>" + pais + "</option>");
                        }
                    }



                });
            }
            //console.log(datos);
            consultarPrograma();
        }
    });
}

/*****************************************************
MUESTRA LOS CAMPOS DE CARREAR ACTIVOS O DESACTIVADOS
DE ACUERDO A LOS DATOS CONTESTADOS ANTERIORMENTE
******************************************************/
function verCamposCarreras() {
    //Valores de los radio
    var valorMaestria = $('input:radio[name=opcionesMaestria]:checked').val();
    var valorLicenciatura = $('input:radio[name=opcionesLicenciatura]:checked').val();
    var valorEspecialidad = $('input:radio[name=opcionesEspecialidad]:checked').val();

    //Maestria
    if (valorMaestria == "Si") {
        $('#campoMaestria').show(400);
    } else {
        $('#campoMaestria').hide(400);
        //Limpiar Apartado de la maestria
        $("#inputCarreraMaestria").val(null);
        $("#inputInstitucionAvalaMaestria").val(null);
        $("#inputInstitucionCampusMaestria").val(null);
        $("#inputPaisOtorgaMaestria").val(null);
        $("#inputPromedioGeneralMaestria").val(null);
    }


    //Licenciatura
    if (valorLicenciatura == "Si") {
        $('#campoLicenciatura').show(400);
    } else {
        $('#campoLicenciatura').hide(400);
        //Limpiar Apartado de la licenciatura
        $("#inputCarreraLicenciatura").val(null);
        $("#inputInstitucionAvalaLicenciatura").val(null);
        $("#inputInstitucionCampusLicenciatura").val(null);
        $("#inputPaisOtorgaLicenciatura").val(null);
        $("#inputPromedioGeneralLicenciatura").val(null);
    }

    //Especialidad
    if (valorEspecialidad == "Si") {
        $('#campoEspecialidad').show(400);
    } else {
        $('#campoEspecialidad').hide(400);
        //Limpiar Apartado de la especialidad
        $("#inputCarreraEspecialidad").val(null);
        $("#inputInstitucionAvalaEspecialidad").val(null);
        $("#inputInstitucionCampusEspecialidad").val(null);
        $("#inputPaisOtorgaEspecialidad").val(null);
        $("#inputPromedioGeneralEspecialidad").val(null);
    }
}

/*****************************************************
ESTABLECE LAS RESTRICCIONES PARA CONTESTAR O NO
LAS CARRERAS EN INFORMACION ACADEMICA DEPENDIENDO
DEL PROGRAMA ASPIRADO
******************************************************/
function consultarPrograma() {
    var url = "./php/cuestionario.php?funcion=consultarPrograma"; // El script a dónde se realizarÃ¡ la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {
            if (data != "salir") {
                var datos = JSON.parse(data);
                //console.log("Consulta de programa");
                //console.log(datos);

                $("#nombreAspirante").html(datos[0][0]);

                var programa = datos[0].nombre;
                //Si es el doctorado
                if (programa == "Doctorado en Ciencias Biológicas") {
                    //Forzar maestria
                    $("#opMaestriaNoM").addClass("disabled");
                    $("#opMaestriaNo").removeAttr("checked");
                    $("#opMaestriaNo").prop("disabled", true);
                    $("#opMaestriaSi").prop("checked", true);

                    //Forzar Licenciatura
                    $("#opLicenciaturaNoM").addClass("disabled");
                    $("#opLicenciaturaNo").removeAttr("checked");
                    $("#opLicenciaturaNo").prop("disabled", true);
                    $("#opLicenciaturaSi").prop("checked", true);

                    //Si ya contesto la especialidad
                    var especialidad = $("#inputCarreraEspecialidad").val();
                    if (especialidad != "") {
                        //Mostrar Especialidad
                        $("#opEspecialidadNo").removeAttr("checked");
                        $("#opEspecialidadSi").prop("checked", true);
                    }

//                    verCamposCarreras();
                } else {
                    //Forzar Licenciatura
                    $("#opLicenciaturaNoM").addClass("disabled");
                    $("#opLicenciaturaNo").removeAttr("checked");
                    $("#opLicenciaturaNo").prop("disabled", true);
                    $("#opLicenciaturaSi").prop("checked", true);

                    //Si ya contesto la maestria
                    var maestria = $("#inputCarreraMaestria").val();
                    if (maestria != "") {
                        //Mostrar Especialidad
                        $("#opMaestriaNo").removeAttr("checked");
                        $("#opMaestriaSi").prop("checked", true);
                    }

                    //Si ya contesto la especialidad
                    var especialidad = $("#inputCarreraEspecialidad").val();
                    if (especialidad != "") {
                        //Mostrar Especialidad
                        $("#opEspecialidadNo").removeAttr("checked");
                        $("#opEspecialidadSi").prop("checked", true);
                    }

//                    verCamposCarreras();
                }
                verCamposCarreras();
                
                 //Mostrar Pagina
                document.getElementById('cargando').style.display = 'none';
                $('html, body').css({
                    'overflow': 'auto'
                });

            } else location.href = "../login.php";
        }
    });
}

/*****************************************************
EVENTO AL SELECCIONAR UN PAIS DE PROCEDENCIA
******************************************************/
//$("#inputPais").change(function () {
//    consultarEstados($(this).val());
//});
$("#inputPais").change(function () {
    var paisSeleccionado=$(this).val();
    verificarDireccion(paisSeleccionado);
});

/*****************************************************
EVENTO AL SELECCIONAR UN PAIS ACADEMICO
******************************************************/
$("#inputPaisOtorgaLicenciatura").change(function () {
    var paisSeleccionado=$(this).val();
    verificarInstitucionProcedencia(paisSeleccionado,"Licenciatura");
});

$("#inputPaisOtorgaEspecialidad").change(function () {
    var paisSeleccionado=$(this).val();
    verificarInstitucionProcedencia(paisSeleccionado,"Especialidad");
});

$("#inputPaisOtorgaMaestria").change(function () {
    var paisSeleccionado=$(this).val();
    verificarInstitucionProcedencia(paisSeleccionado,"Maestria");
});

/*****************************************************
MUESTRA U OCULTA LOS CAMPOS DE ESTADO PARA CADA CARRERA
******************************************************/
function verificarInstitucionProcedencia(paisSeleccionado,carrera){
    if(paisSeleccionado == "México"){
        //muestra el select de institucion
        $("#select"+carrera).removeClass("hide",400);
        $("#selectInstitucionAvala"+carrera).removeAttr("disabled");
        //oculta el input  de institucion
        $("#inputText"+carrera).addClass("hide",400);
        $("#inputInstitucionAvala"+carrera).attr("disabled","disabled");

        //muestra campos de estado
        $("#inputEstadoOtorga"+carrera).removeClass("hide",400);
        $("#labelEstado"+carrera).removeClass("hide",400);
        $("#HelpEstado"+carrera).removeClass("hide",400);
        
        //Selecciona el estado de carrera que se buscara
        var estadoSeleccionado;
        switch(carrera){
            case 'Licenciatura':
                estadoSeleccionado=estadoLicenciatura;
                break;
            case 'Especialidad':
                estadoSeleccionado=estadoEspecialidad;
                break;
            case 'Maestria':
                estadoSeleccionado=estadoMaestria;
                break;
                
        }
        consultarEstadosCarrera(carrera,estadoSeleccionado);
    }else{
        //oculta el select de institucion
        $("#select"+carrera).addClass("hide",400);
        $("#selectInstitucionAvala"+carrera).attr("disabled","disabled");
        //muestra el input de institucion
        $("#inputText"+carrera).removeClass("hide",400);
        $("#inputInstitucionAvala"+carrera).removeAttr("disabled");
        //oculta campos de estado
        $("#inputEstadoOtorga"+carrera).addClass("hide",400);
        $("#inputEstadoOtorga"+carrera).val("");
        $("#labelEstado"+carrera).addClass("hide",400);
        $("#HelpEstado"+carrera).addClass("hide",400);
        
    }
}

/*****************************************************
EVENTO AL SELECCIONAR UN Estado Academico
******************************************************/
$("#inputEstadoOtorgaLicenciatura").change(function () {
    var estadoSeleccionado=$(this).val();
    
    consultarUniversidades(estadoSeleccionado,"Licenciatura",1);
});

$("#inputEstadoOtorgaEspecialidad").change(function () {
    var estadoSeleccionado=$(this).val();
    
    consultarUniversidades(estadoSeleccionado,"Especialidad",1);
});

$("#inputEstadoOtorgaMaestria").change(function () {
    var estadoSeleccionado=$(this).val();
    
    consultarUniversidades(estadoSeleccionado,"Maestria",1);
});

/*****************************************************
EVENTO AL SELECCIONAR UNA UNIVERSIDAD
******************************************************/
$("#selectInstitucionAvalaLicenciatura").change(function () {
    var opcion=$(this).val();
//        alert(opcion);
    
    if(opcion == "Otra"){
        //muestra el input
        $("#inputTextLicenciatura").removeClass("hide",400);
        $("#inputInstitucionAvalaLicenciatura").removeAttr("disabled");
        //cambiar nombre del select
        $(this).attr("name","a");
        $("#inputInstitucionAvalaLicenciatura").attr("name","inputInstitucionAvalaLicenciatura");
//        $(this).attr("disabled","disabled");
//        $("#inputInstitucionAvalaLicenciatura").attr("disabled","disabled");
    }else{
        //oculta el input
        $("#inputTextLicenciatura").addClass("hide",400);
        $("#inputInstitucionAvalaLicenciatura").attr("name","a");
        //cambiar nombre
        $(this).attr("name","inputInstitucionAvalaLicenciatura");
    }
});

$("#selectInstitucionAvalaEspecialidad").change(function () {
    var opcion=$(this).val();
//        alert(opcion);
    
    if(opcion == "Otra"){
        //muestra el input
        $("#inputTextEspecialidad").removeClass("hide",400);
        $("#inputInstitucionAvalaEspecialidad").removeAttr("disabled");
        //cambiar nombre del select
        $(this).attr("name","a");
        $("#inputInstitucionAvalaEspecialidad").attr("name","inputInstitucionAvalaEspecialidad");
//        $(this).attr("disabled","disabled");
//        $("#inputInstitucionAvalaLicenciatura").attr("disabled","disabled");
    }else{
        //oculta el input
        $("#inputTextEspecialidad").addClass("hide",400);
        $("#inputInstitucionAvalaEspecialidad").attr("name","a");
        //cambiar nombre
        $(this).attr("name","inputInstitucionAvalaEspecialidad");
    }
});

$("#selectInstitucionAvalaMaestria").change(function () {
    var opcion=$(this).val();
//        alert(opcion);
    
    if(opcion == "Otra"){
        //muestra el input
        $("#inputTextMaestria").removeClass("hide",400);
        $("#inputInstitucionAvalaMaestria").removeAttr("disabled");
        //cambiar nombre del select
        $(this).attr("name","a");
        $("#inputInstitucionAvalaMaestria").attr("name","inputInstitucionAvalaMaestria");
//        $(this).attr("disabled","disabled");
//        $("#inputInstitucionAvalaLicenciatura").attr("disabled","disabled");
    }else{
        //oculta el input
        $("#inputTextMaestria").addClass("hide",400);
        $("#inputInstitucionAvalaMaestria").attr("name","a");
        //cambiar nombre
        $(this).attr("name","inputInstitucionAvalaMaestria");
    }
});


/*****************************************************
CONSULTA LOS ESTADOS CORRESPONDIENTES AL PAIS
SELECCIONADO Y LOS COLOCA EN EL SELECT ESTADOS
******************************************************/
function consultarEstados(pais,input,dialog) {
    //console.log("Pais: " + pais);

    //////////////Select Pais
    var url = "./php/cuestionario.php?funcion=consultarEstados&pais=" + pais + ""; // El script a dónde se realizarÃ¡ la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {

            if (data != "salir") {
                var datos = JSON.parse(data);
                console.log(datos);
                $(input).empty();
                $(input).append("<option disabled selected></option>");
                $.each(datos, function (index) {
                    var estado = datos[index].estadonombre;
                    if (estado != undefined) {
                        if (estado == estadoG) {
                            $(input).append("<option value='" + estado + "' selected>" + estado + "</option>");
                        } else {
                            $(input).append("<option value='" + estado + "'>" + estado + "</option>");
                        }
                    }
                });
                
            }
        }
    });
}

function consultarEstadosCarrera(carrera,estadoSeleccionado) {
    //////////////Select Pais
    var url = "./php/cuestionario.php?funcion=consultarEstados&pais=Mexico"; // El script a dónde se realizarÃ¡ la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {

            if (data != "salir") {
                var datos = JSON.parse(data);
                console.log("#inputEstadoOtorga"+carrera);
                $("#inputEstadoOtorga"+carrera).empty();
//                $("#inputEstadoOtorga"+carrera).append("<option disabled selected></option>");
                console.log("compara estado con: "+estadoSeleccionado)
                $.each(datos, function (index) {
                    var estado = datos[index].estadonombre;
                    if (estado != undefined) {
                        if (estado == estadoSeleccionado) {
                            $("#inputEstadoOtorga"+carrera).append("<option value='" + estado + "' selected>" + estado + "</option>");
                            
                            consultarUniversidades(estado,carrera,1);
                        } else {
                            $("#inputEstadoOtorga"+carrera).append("<option value='" + estado + "'>" + estado + "</option>");
                        }
                    }
                });
            }
        }
    });
}

/*****************************************************
CONSULTA LOS ESTADOS CORRESPONDIENTES AL PAIS
SELECCIONADO Y LOS COLOCA EN EL SELECT ESTADOS
******************************************************/
function consultarUniversidades(estado,carrera,dialog) {
    if(dialog==1){
        showDialog();
    }


    //console.log("Pais: " + pais);
    var institucionCarrera;
    switch(carrera){
        case 'Licenciatura':
            institucionCarrera=institucionLicenciatura;
            break;
        case 'Especialidad':
            institucionCarrera=institucionEspecialidad;
            break;
        case 'Maestria':
            institucionCarrera=institucionMaestria;
            break;
                  }

    var universidadExistente=false;
    //////////////Select estado
    var url = "./php/cuestionario.php?funcion=consultarUniversidades&estado=" + estado + ""; // El script a dónde se realizarÃ¡ la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {

            if (data != "salir") {
//                console.log(data);
                var datos = JSON.parse(data);
                $("#selectInstitucionAvala"+carrera).empty();
                $("#selectInstitucionAvala"+carrera).append("<option disabled selected></option>");
                console.log("Se comparlo la universidad con: "+institucionLicenciatura);
                $.each(datos, function (index) {
                    var universidad = datos[index].universidad;
                    if (universidad != undefined) {
                        if (universidad == institucionCarrera) {
                            $("#selectInstitucionAvala"+carrera).append("<option value='" + universidad + "' selected>" + universidad + "</option>");
                            
                            universidadExistente=true;
                        } else {
                            $("#selectInstitucionAvala"+carrera).append("<option value='" + universidad + "'>" + universidad + "</option>");
                        }
                    }
                });
                
                //Si la universidad no se encuentra en la lista
                if(!universidadExistente){
                    ///Se aÃ±ade la opcion de la universidad
                $("#selectInstitucionAvala"+carrera).append("<option value='"+institucionCarrera+"' selected>"+institucionCarrera+"</option>");
                }
                
                ///Se aÃ±ade la opcion de otra
                $("#selectInstitucionAvala"+carrera).append("<option value='Otra'>Otra</option>");
                
                if(dialog==1){
                    hideDialog();
                }
            }
        }
    });
}


function verificarDireccion(paisSeleccionado){
    if(paisSeleccionado == "México"){
        $("#divDireccion").show(700);
        $("#divDireccionCompleta").hide(700);
        consultarEstados(paisSeleccionado,"#inputEstado",1);
    }else{
        $("#divDireccion").hide(700);
        $("#divDireccionCompleta").show(700);
        $("#inputEstado").val(null);
        $("#inputMunicipio").val(null);
        $("#inputDelegacion").val(null);
        $("#inputCalle").val(null);
        $("#inputColonia").val(null);
        $("#inputNumExt").val(null);
        $("#inputNumInt").val(null);
    }
}

$("#inputModalidad").change(function () {
    var modalidad=$(this).val();
    showInfoModalidad(modalidad);
});

function showInfoModalidad(modalidad){
    if(modalidad == "Tiempo completo"){
        $("#infoModalidad").html("<p>Con posibilidad de beca por parte de CONACYT</p>");
        $("#infoModalidad").removeClass("hide",400);
        
    }else if(modalidad == "Tiempo parcial"){
        $("#infoModalidad").html("<p>Sin posibilidad de beca por parte de CONACYT</p>");
        $("#infoModalidad").removeClass("hide",400);
    }
    else{
        $("#infoModalidad").addClass("hide");
    }
}

