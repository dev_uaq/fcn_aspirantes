/*****************************************************
DOCUMENTO LISTO
******************************************************/
$(document).ready(function () {
    consultarRecomendaciones();
    consultarPrograma();
    //Mostrar Pagina
    document.getElementById('cargando').style.display = 'none';
    $('html, body').css({
        'overflow': 'auto'
    });
});



/*****************************************************
VARIABLES GLOBALES
******************************************************/
var correo1;
var correo2;

/*****************************************************
MUESTRA GIF PROCESANDO
******************************************************/
function showDialog() {
    $("#procesando").addClass("procesando");
}

/*****************************************************
OCULTA GIF PROCESANDO
******************************************************/
function hideDialog() {
    $("#procesando").removeClass("procesando");
}

/*****************************************************
OCULTA DOCUMENTOS QUE EL PROGRAMA EN ESPECIICO
NO REQUIRE
******************************************************/
function consultarPrograma() {
    var url = "./php/documentos.php?funcion=consultarPrograma"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {
            if (data != "salir") {
                var datos = JSON.parse(data);

                $("#nombreAspirante").html(datos[0][0]);

                var programa = datos[0].nombre;
                nombre_programa = programa;

                //Si no es el doctorado
                if (programa != "Doctorado en Ciencias Biológicas") {
                    $("#campo_file_titulo_maestria").hide();
                    $("#campo_file_cedula_maestria").hide();
                    console.log("Se deshabilitado Titulo y cedula de Maestria");
                }

                //Si no es MSPAS
                if (programa != "Maestría en Salud y Producción Animal Sustentable") {
                    $("#campo_file_carta_jefe_inmediato").hide();
                    console.log("Se deshabilito carta de jefe inmediato");
                }

                //Si no es MCB o DCB
                if (programa != "Doctorado en Ciencias Biológicas" || programa != "Maestria en Ciencias Biológicas") {
                    $("#campo_file_carta_aceptacion").hide();
                    console.log("Se deshabilito carta de aceptacion");
                }



                //

            } else location.href = "../login.php";
        }
    });
}

/*****************************************************
VALIDA EL FORMULARIO DE REFERENCIA UNO Y ENVIA LA 
PETICION DE REGISTRO
******************************************************/
$("#btnReferenciaUno").click(function () {
    var $validator = $("#formularioReferenciaUno").validate({
        messages: {
            confirmar1: "Por favor verifique que el correo eléctronico coincida."
        }
    });

    var $valid = $("#formularioReferenciaUno").valid();

    if ($valid) {
        enviarFormulario("Uno");
    } else {
        $validator.focusInvalid();
    }
});

/*****************************************************
VALIDA EL FORMULARIO DE REFERENCIA DOS Y ENCIA LA
PETICION DE REGISTRO
******************************************************/
$("#btnReferenciaDos").click(function () {
    var $validator = $("#formularioReferenciaDos").validate({
        messages: {
            confirmar2: "Por favor verifique que el correo eléctronico coincida."
        }
    });

    var $valid = $("#formularioReferenciaDos").valid();

    if ($valid) {
        enviarFormulario("Dos");
    } else {
        $validator.focusInvalid();
    }
});

/*****************************************************
ENVIA LA PETICION AJAX AL ARCHIVO PHP Y REALIZA LOS
CAMBIOS CORRESPONDIENTES EN LA INTERFAZ
******************************************************/
function enviarFormulario(formulario) {
    showDialog();
    var url = "./php/referencias.php?funcion=enviarFormulario&n=" + formulario; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: $("#formularioReferencia" + formulario).serialize(), // Adjuntar los campos del formulario enviado.
        success: function (data) {
            //si la sesion no se ha iniciado
            if (data != "salir") {
                console.log("Respuesta de correo: " + data);
                swal("Formulario enviado", "El formulario ha sido enviado al correo electrónico que proporciono, si por alguna razón el formulario no es respondido usted puede cambiar el correo electrónico de referencia en cualquier momento.", "info", {
                    button: "Aceptar",
                })

                consultarRecomendaciones();

                if (formulario == "Uno") {
                    document.getElementById("formularioReferenciaUno").reset();
                    $("#btnUno").val("mostrar");
                    $("#btnDos").val("ocultar");
                    //Boton Uno
                    $("#formularioReferenciaUno").hide(400);

                    $("#btn_uno").html("Cambiar Referencia").addClass("btn-info").removeClass("btn-danger").addClass("btn-sm").removeClass("btn-block");

                    $("#nota").addClass("alert-warning").removeClass("alert-danger").html("<p><strong>Nota: </strong><br> Antes de enviar el formulario a alguien por favor infórmele de la situación, nosotros enviaremos un correo electrónico explicando los motivos pero queremos evitar cualquier malentendido, gracias.</p>");

                    $("#btn_dos").removeClass("disabled");
                } else {
                    document.getElementById("formularioReferenciaDos").reset();
                    //Boton Dos
                    $("#formularioReferenciaDos").hide(400);

                    $("#btn_dos").html("Cambiar Referencia").addClass("btn-info").removeClass("btn-danger").addClass("btn-sm").removeClass("btn-block");

                    $("#nota").addClass("alert-warning").removeClass("alert-danger").html("<p><strong>Nota: </strong><br> Antes de enviar el formulario a alguien por favor infórmele de la situación, nosotros enviaremos un correo electrónico explicando los motivos pero queremos evitar cualquier malentendido, gracias.</p>");

                    $("#btn_uno").removeClass("disabled");
                }

                hideDialog();

            } else location.href = "../login.php";
        }
    });
}

/*****************************************************
VERIFICA EL ESTADO DE LAS RECOMENDACIONES Y REALIZA
LOS CAMBIOS CORRESPONDIENTES EN LA INTERFAZ
******************************************************/
function consultarRecomendaciones() {
    showDialog();
    var url = "./php/referencias.php?funcion=consultarRecomendaciones"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        //        data: $("#formularioReferencia"+formulario).serialize(), // Adjuntar los campos del formulario enviado.
        success: function (data) {
            //si la sesion no se ha iniciado
            if (data != "salir") {
                hideDialog();
                var datos = JSON.parse(data);

                var ref1 = datos[0].estado;
                var ref2 = datos[1].estado;
                correo1 = datos[0].correo;
                correo2 = datos[1].correo;

                //Validación de que los correos no se repitan
                jQuery.validator.addMethod("correos", function (value, element, correos) {
                    return this.optional(element) || value != correo1 && value != correo2;
                }, jQuery.validator.format("El correo electrónico debe ser diferente de "+correo1+" y de "+correo2));

                switch (ref1) {
                    case "Sin enviar":
                        break;
                    case "En espera":
                        $("#iconoReferenciaUno").addClass("text-warning");
                        $("#PrimerCorreo").html(datos[0].correo);
                        $("#formularioReferenciaUno").hide();
                        $("#btnUno").show();
                        break;
                    case "Respondida":
                        $("#iconoReferenciaUno").addClass("text-success");
                        $("#PrimerCorreo").html(datos[0].correo);

                        $("#formularioReferenciaUno").hide();
                        break;

                }

                switch (ref2) {
                    case "Sin enviar":
                        break;
                    case "En espera":
                        $("#iconoReferenciaDos").addClass("text-warning");
                        $("#SegundoCorreo").html(datos[1].correo);
                        $("#formularioReferenciaDos").hide();
                        $("#btnDos").show();
                        break;
                    case "Respondida":
                        $("#iconoReferenciaDos").addClass("text-success");
                        $("#SegundoCorreo").html(datos[1].correo);
                        $("#formularioReferenciaDos").hide();
                        break;
                }

                if (ref1 == "Respondida" && ref2 == "Respondida") {
                    $("#nota").removeClass("alert-warning").addClass("alert-success").html("<p class='text-center'><strong>Referencias recibidas:</strong><br>Los formularios de recomendación ya fueron respondidos.</p>");
                }

            } else location.href = "../login.php";
        }
    });
}

/*****************************************************
CONSULTA LOS DATOS DEL ASPIRANTE Y EL PROGRAMA
EN EL QUE SE ENCUENTRA
******************************************************/
function consultarPrograma() {
    showDialog();
    var url = "./php/documentos.php?funcion=consultarPrograma"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {
            if (data != "salir") {
                var datos = JSON.parse(data);

                $("#nombreAspirante").html(datos[0][0]);
                var programa = datos[0].nombre;
                hideDialog();
            } else location.href = "../login.php";
        }
    });
}

/*****************************************************
EVENTO AL PULSAR BOTON UNO
******************************************************/
$("#btn_uno").click(function () {
    var valor = $("#btn_uno").val();

    //Si el boton uno esta listo para mostrar
    if ($("#btn_uno").hasClass("btn-info")) {
        $("#btn_uno").val("ocultar");
        $("#formularioReferenciaUno").show(400);

        $("#btn_uno").html("Cancelar").removeClass("btn-info", 400).addClass("btn-danger", 400).removeClass("btn-sm", 400).addClass("btn-block", 400);

        $("#nota").removeClass("alert-warning", 400).addClass("alert-danger", 400).html("<p><strong>Atención: </strong><br>Si cambia el correo electrónico de referencia se deshabilitara el formulario enviado a <strong>" + correo1 + "</strong> y se habilitara para el nuevo correo electrónico que introduzca.</p>");

        $("#btn_dos").addClass("disabled");

    } else if ($("#btn_uno").hasClass("btn-danger")) {
        $("#btn_uno").val("mostrar");
        $("#formularioReferenciaUno").hide(400);

        $("#btn_uno").html("Cambiar Referencia").addClass("btn-info", 400).removeClass("btn-danger", 400).addClass("btn-sm", 400).removeClass("btn-block", 400);

        $("#nota").addClass("alert-warning", 400).removeClass("alert-danger", 400).html("<p><strong>Nota: </strong><br> Antes de enviar el formulario a alguien por favor infórmele de la situación, nosotros enviaremos un correo electrónico explicando los motivos pero queremos evitar cualquier malentendido, gracias.</p>");

        $("#btn_dos").removeClass("disabled");
    }
});

/*****************************************************
EVENTO AL PULSAR BOTON DOS
******************************************************/
$("#btn_dos").click(function () {
    var valor = $("#btn_dos").val();

    if ($("#btn_dos").hasClass("btn-info")) {
        $("#formularioReferenciaDos").show(400);
        $("#btn_dos").val("ocultar");

        $("#btn_dos").html("Cancelar").removeClass("btn-info").addClass("btn-danger", 400).removeClass("btn-sm", 400).addClass("btn-block", 400);

        $("#nota").removeClass("alert-warning", 400).addClass("alert-danger", 400).html("<p><strong>Atención: </strong><br>Si cambia el correo electrónico de referencia se deshabilitara el formulario enviado a <strong>" + correo2 + "</strong> y se habilitara para el nuevo correo electrónico que introduzca.</p>");

        $("#btn_uno").addClass("disabled");
    } else if ($("#btn_dos").hasClass("btn-danger")) {
        $("#formularioReferenciaDos").hide(400);
        $("#btn_dos").val("mostrar");

        $("#btn_dos").html("Cambiar Referencia").addClass("btn-info", 400).removeClass("btn-danger", 400).addClass("btn-sm", 400).removeClass("btn-block", 400);

        $("#nota").addClass("alert-warning", 400).removeClass("alert-danger", 400).html("<p><strong>Nota: </strong><br> Antes de enviar el formulario a alguien por favor infórmele de la situación, nosotros enviaremos un correo electrónico explicando los motivos pero queremos evitar cualquier malentendido, gracias.</p>");

        $("#btn_uno").removeClass("disabled");
    }
});
