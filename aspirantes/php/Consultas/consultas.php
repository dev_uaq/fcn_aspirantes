<?php

/*---------------------------------------
 DEEP MINT WAS HERE <(°_°)>
---------------------------------------*/

include('../conect.php');

//Reanuda la sesión y se traen las variables que hay en ella 
    session_start();
    $correo = $_SESSION['pk'];
    $tipo = $_SESSION['tipo'];
    $programa = $_SESSION['fk'];
    $nombre = $_SESSION['nombre'];
//Array que enviara todos los datos al JS con AJAX
$jsondata = array();

//Ejecutamos la función choose y le enviaamos la acción que solicitamos
choose($_POST['accion']);

/*--------------------------------------
     FUNCIÓN ELEJIR ACCIÓN
--------------------------------------*/
//Función para elejir la acción requerida y así devolver el json correspondiente
function choose($accion){
     global $jsondata;
    switch ($accion) {
                case "consulta-inicial":
                     consulta_Inicial();
                    break;
                case "llenar-DashBoard":
                    llenarDashBoard($_POST['pk_periodo']);
                    break;
                case "llenar-tabla":
                     consulta_Aspirantes($_POST['pk_periodo'],$_POST['opcion'],$_POST['ac-dc']);
                    break;
                case "contar-rows":
                     contar_Aspirantes($_POST['pk_periodo'],$_POST['opcion']);
                    break;
                case "session-aspirante":
                    $_SESSION['pk_aspirante'] = $_POST['pk_aspirante']; 
                     
                    if (isset($_SESSION['pk_aspirante'])) {
                        $jsondata["created"] = true;
                    }else{
                        $jsondata["created"] = false;
                    }
                    break;
                    case "documento_Exist":
                    documento_Exist($_POST['pk_aspirante'],$_POST['documento']);
                    break;
                    case "consulta-periodos":
                    consulta_Periodos();
                    break;
                    case "update_periodo":
                    update_Periodo($_POST['pk_periodo'],sacarNúmeroMes($_POST['new_date']));
                    break;
            
            
                
                }
     //Se envián los datos al JS
    header('Content-type: application/json; charset=utf-8');
  echo json_encode($jsondata, JSON_FORCE_OBJECT);
}


/*-----------------------------------------
   FUNCIÓN CONSULTA INICIAL
-----------------------------------------*/
function consulta_Inicial(){
 global $jsondata, $programa, $nombre;

    //Nombre del usuario
    $jsondata[nombre] = $nombre; 
/*-----------------------------------------
   CONSULTAR NOMBRE DEL PROGRAMA ACADÉMICO
-----------------------------------------*/
//Consulta el nombre del programa al que pertenece el usuario
$QueryOne = "SELECT nombre FROM programas WHERE pk_programa = '$programa'";
$resultsOne = executeQuery($QueryOne)->fetchAll();
 foreach($resultsOne as $rs) {
     $jsondata["programa"] = $rs['nombre']; 
 }


/*--------------------------------------
   CONSULTAR PERIODOS
--------------------------------------*/   
    $squareOne=0;
    $squareTwo=0;
    $rowMaster =array();
    $rowChild = array();
//Consulta de los periodos correspondientes al programa del usuario      
    $QueryThree = "SELECT DISTINCT año FROM periodos WHERE fk_programa = '$programa' ORDER BY año DESC";
if ($resultsThree = executeQuery($QueryThree)->fetchAll()){    
    $jsondata["periodos"] = array();
    foreach($resultsThree as $rsThree) {
    $squareOne=0;
    $year =$rsThree['año'];
    $QueryTwo = "SELECT pk_periodo, fecha_inicio, fecha_fin, periodo_año FROM periodos WHERE fk_programa = '$programa' AND año='$year' ORDER BY  periodo_año ASC";
    if ($resultsTwo = executeQuery($QueryTwo)->fetchAll()){
    $jsondata["success"] = true;
     foreach($resultsTwo as $rs) {
         $row = array();
         $row[0] = $rs['pk_periodo'];
         $row[1] = armarTextoFecha2($rs['fecha_inicio'],$rs['fecha_fin']);
         $row[2] = $rs['periodo_año'];
         $row[3] = $year;
         $row[4] = periodoActivo($rs['pk_periodo']);
        $rowChild[$squareOne]=$row;
            $squareOne++;
     } 
        $rowMaster[$squareTwo]=$rowChild;
        $squareTwo++;
    }else
    {
       $jsondata["success"] = false;
    }
        
    }    
    $jsondata["periodos"] = $rowMaster;
}else{
    $jsondata["success"] = false;
}

 
     
}

/*--------------------------------------
   FUNCIÓN ARMAR TEXTO PERIODO 2
--------------------------------------*/
//Función para armar el texto del tipo "Periodo Enero / Febrero 2017" a travéz de una fecha de inicial y una final
function armarTextoFecha2($fechaUno,$fechaDos){    
 $fecha_Inicio = explode('-',$fechaUno);
 $fecha_Fin = explode('-',$fechaDos);  
    
//Se obtienen los nombres de los meses utilizando la función sacarMes
 $mes_Inicio = sacarMes($fecha_Inicio[1]);
 $mes_Fin = sacarMes($fecha_Fin[1]);
 
//Se arma el texto del periodo

     $textoFecha = $fecha_Inicio[2]." ".$mes_Inicio." ".$fecha_Inicio[0]." hasta ".$fecha_Fin[2]." ".$mes_Fin." ".$fecha_Fin[0];
 
    return $textoFecha;    
}

/*--------------------------------------
     FUNCIÓN OBTENER NOMBRE DEL MES
--------------------------------------*/
//Función para obtener el nombre del mes con el número
function sacarMes($mesNumber){
    $textMes="Any";
    switch ($mesNumber) {
                case "1":
                     $textMes = "Enero";
                    break;
                case "2":
                     $textMes = "Febrero";
                    break;
                case "3":
                     $textMes = "Marzo";
                    break;
                case "4":
                     $textMes = "Abril";
                    break;
                case "5":
                     $textMes = "Mayo";
                    break;
                case "6":
                     $textMes = "Junio";
                    break;
                case "7":
                     $textMes = "Julio";
                    break;
                case "8":
                     $textMes = "Agosto";
                    break;
                case "9":
                     $textMes = "Septiembre";
                    break;
                case "10":
                     $textMes = "Octubre";
                    break;
                case "11":
                     $textMes = "Noviembre";
                    break;
                case "12":
                     $textMes = "Diciembre";
                    break;
                
                }
    return $textMes;
}

/*--------------------------------------
   FUNCIÓN DATOS DASHBOARD
--------------------------------------*/  
function llenarDashBoard($fk_periodo){
global $jsondata;
 $periodo_estado = periodoActivo($fk_periodo);
       switch ($periodo_estado) {
                case "before":
                     $jsondata["success"] = true;
                     $jsondata["estado_periodo"] = "before";
                    break;
                case "after":
                     
                    /*--------------------------------------------------------------
                       CONSULTAR ALUMNOS REGISTRADOS Y ACEPTADOS EN CIERTO PERIODO
                    --------------------------------------------------------------*/    
                    //Consulta la cantidad de alumnos registrados en un periodo especifico y también aquellos que ya han sido aceptados   
                    $alumnosCount=0;
                    $aceptadosCount=0;
                    $QueryOne = "SELECT estado FROM alumnos WHERE fk_periodo = '$fk_periodo'";
                    if ($resultsOne = executeQuery($QueryOne)->fetchAll()){
                    $jsondata["success"] = true;
                     foreach($resultsOne as $rs) {
                        $alumnosCount++;
                         if($rs['estado'] == "Aceptado"){
                             $aceptadosCount++;
                         }
                     } 
                        //Limpiamos el statement de memoria
                       // limpiarStm($resultsTwo);
                        $jsondata["success"] = true; 
                        $jsondata["estado_periodo"] = "after";
                        $jsondata["registrados"] = $alumnosCount;
                        $jsondata["no_aceptados"] = $alumnosCount - $aceptadosCount;
                        $jsondata["aceptados"] = $aceptadosCount;

                    }else
                    {
                       $jsondata["success"] = false; 
                       $jsondata["estado_periodo"] = "after";
                    }
                               
                    break;
                case "just_now":
                     /*--------------------------------------------------------------
                       CONSULTAR ALUMNOS REGISTRADOS, EN ESPERA Y ACEPTADOS EN CIERTO PERIODO
                    --------------------------------------------------------------*/    
                    //Consulta la cantidad de alumnos registrados en un periodo especifico y también aquellos que ya han sido aceptados   
                    $alumnosCount=0;
                    $aceptadosCount=0;
                    $esperaCount=0;
                    $QueryOne = "SELECT pk_matricula, estado FROM alumnos WHERE fk_periodo = '$fk_periodo'";
                    if ($resultsOne = executeQuery($QueryOne)->fetchAll()){
                    $jsondata["success"] = true;
                     foreach($resultsOne as $rs) {
                        $alumnosCount++;
                         if($rs['estado'] == "Aceptado"){
                             $aceptadosCount++;
                         }
                         if($rs['estado'] == "En espera"){
                                      $esperaCount++;                                 
                         } 
                     } 
                        //Limpiamos el statement de memoria
                       // limpiarStm($resultsTwo);
                        $jsondata["success"] = true; 
                        $jsondata["estado_periodo"] = "just_now";
                        $jsondata["registrados"] = $alumnosCount;
                        $jsondata["en_espera"] = $esperaCount;
                        $jsondata["aceptados"] = $aceptadosCount;

                    }else
                    {
                       $jsondata["success"] = false; 
                       $jsondata["estado_periodo"] = "just_now";
                    }
                    break;
                case "error":
                     $jsondata["success"] = false; 
                     $jsondata["estado_periodo"] = "mistake";
                    break;
                
                }

 
}




/*--------------------------------------
   FUNCIÓN PERIODO ACTIVO
--------------------------------------*/  
/*función para determinar si un periodo esta activo retornando un valor distinto para cada situación, "before": periodo aún no vigente, "after": periodo ya pasado, "just_now": periodo activo justo ahora*/
function periodoActivo($id_periodo){
    global $jsondata;
$yes_no_maybe="I don´t now, can you repeat the question?";
/*----------------------------------------------------------------
   CONSULTAR FECHA FINAL DE PERIODO
----------------------------------------------------------------*/ 
$fecha_inicio;
$fecha_fin;
$fecha_actual;
$QueryOne = "SELECT fecha_inicio, fecha_fin FROM periodos WHERE pk_periodo = '$id_periodo'";
if ($resultsOne = executeQuery($QueryOne)->fetchAll()){
     foreach($resultsOne as $rs) {
         $fecha_inicio = $rs['fecha_inicio'];
         $fecha_fin = $rs['fecha_fin'];
     } 
    
    //Limpiamos el statement de memoria
   // limpiarStm($resultsTwo);
    
        /* Se compara la fecha actual y la fecha final y de inicio del periodo para ver si esta habilitado */
        
    date_default_timezone_set('America/Mexico_City');

$fecha_actual = date("Y-m-d");

        if($fecha_actual < $fecha_inicio){
                $yes_no_maybe = "before";
            $jsondata["date"] = $fecha_actual." before"; 
        }else if($fecha_actual > $fecha_fin){
                $yes_no_maybe = "after";
           $jsondata["date"] = $fecha_actual." after";
        }else{
            $yes_no_maybe = "just_now";
            $jsondata["date"] = $fecha_actual." just_now";
        }
}else
{
   $yes_no_maybe="error";
}    
    return $yes_no_maybe;
}

/*-----------------------------------------
   FUNCIÓN CONSULTAR ASPIRANTES
-----------------------------------------*/
/* Deuelve los datos generales de los aspirantes pertenecientes al periodo seleccionado y con el filtro selecionado*/
function consulta_Aspirantes($key_periodo,$option,$acdc){
    global $jsondata;
    $OFFSET = $_POST['offset'];
    $LIMIT = $_POST['limit'];
    //   $jsondata["hi"] = "Im groot"." ".$option." ".$key_periodo;
       switch ($option) {
                case "1":     //Todos los aspirantes de dicho periodo
                        $QueryOne;               
                       if($acdc == "ac"){//Orden ASCENDENTE
                           $QueryOne = "SELECT pk_matricula, nombre, apellido_p, apellido_m FROM alumnos WHERE fk_periodo = '$key_periodo' ORDER BY apellido_p ASC LIMIT ?,?";

                       }else if($acdc == "dc"){//Orden DESCENDENTE
                           $QueryOne = "SELECT pk_matricula, nombre, apellido_p, apellido_m FROM alumnos WHERE fk_periodo = '$key_periodo' ORDER BY apellido_p DESC LIMIT ?,?";
                       }
                         if ($resultsOne = executeQueryLimit($QueryOne,$OFFSET,$LIMIT)->fetchAll()){
                            $jsondata["success"] = true;
                            $jsondata["registros"] = array();
                             foreach($resultsOne as $rs) {
                            $jsondata["registros"][] = $rs;                       
                             }
                            }else{
                                 //$jsondata["nian"] = false;
                            }
                    break;
               case "2":      //Todos los aspirantes de dicho periodo que no han terminado de subir sus datos
                $QueryOne;     
                        $QueryOne;  
                       if($acdc == "ac"){//Orden ASCENDENTE
                           $QueryOne = "SELECT alumnos.pk_matricula, alumnos.nombre, alumnos.apellido_p, alumnos.apellido_m FROM alumnos INNER JOIN apartados ON apartados.fk_matricula = alumnos.pk_matricula WHERE alumnos.estado = 'En proceso' AND alumnos.fk_periodo = '$key_periodo' AND apartados.cuestionario != '1' OR alumnos.fk_periodo = '$key_periodo' AND apartados.documentos != '1' OR alumnos.fk_periodo = '$key_periodo' AND apartados.referencias != '1' ORDER BY apellido_p ASC LIMIT ?,?";
                       }else if($acdc == "dc"){//Orden DESCENDENTE
                           $QueryOne = "SELECT alumnos.pk_matricula, alumnos.nombre, alumnos.apellido_p, alumnos.apellido_m FROM alumnos INNER JOIN apartados ON apartados.fk_matricula = alumnos.pk_matricula WHERE alumnos.estado = 'En proceso' AND alumnos.fk_periodo = '$key_periodo' AND apartados.cuestionario != '1' OR alumnos.fk_periodo = '$key_periodo' AND apartados.documentos != '1' OR alumnos.fk_periodo = '$key_periodo' AND apartados.referencias != '1' ORDER BY apellido_p DESC LIMIT ?,?";
                       }
                         
                         if ($resultsOne = executeQueryLimit($QueryOne,$OFFSET,$LIMIT)->fetchAll()){
                            $jsondata["success"] = true;
                            $jsondata["registros"] = array();
                             foreach($resultsOne as $rs) {
                               
                                              $jsondata["registros"][] = $rs;  
                                    
                             }
                            }else{
                                 $jsondata["success"] = false;
                            }
                    break;
               case "3":      //Todos los aspirantes de dicho periodo que ya tienen listos sus datos
               $QueryOne;  
                       if($acdc == "ac"){//Orden ASCENDENTE
                           $QueryOne = "SELECT alumnos.pk_matricula, alumnos.nombre, alumnos.apellido_p, alumnos.apellido_m FROM alumnos INNER JOIN apartados ON apartados.fk_matricula = alumnos.pk_matricula WHERE alumnos.estado = 'En espera' AND alumnos.fk_periodo = '$key_periodo' AND apartados.cuestionario = '1' AND apartados.documentos = '1' AND apartados.referencias = '1' ORDER BY apellido_p ASC LIMIT ?,? ";
                       }else if($acdc == "dc"){//Orden DESCENDENTE
                           $QueryOne = "SELECT alumnos.pk_matricula, alumnos.nombre, alumnos.apellido_p, alumnos.apellido_m FROM alumnos INNER JOIN apartados ON apartados.fk_matricula = alumnos.pk_matricula WHERE alumnos.estado = 'En espera' AND alumnos.fk_periodo = '$key_periodo' AND apartados.cuestionario = '1' AND apartados.documentos = '1'  AND apartados.referencias = '1' ORDER BY apellido_p DESC LIMIT ?,? ";
                       }
                        
                         if ($resultsOne = executeQueryLimit($QueryOne,$OFFSET,$LIMIT)->fetchAll()){
                            $jsondata["success"] = true;
                            $jsondata["registros"] = array();
                             foreach($resultsOne as $rs) {
                              
                                              $jsondata["registros"][] = $rs;  
                                    
                             }
                            }else{
                                 $jsondata["success"] = false;
                             
                            }
                    break;
               case "4":     //Todos los aspirantes de dicho periodo que ya han sido aceptados
               
               $QueryOne;  
                       if($acdc == "ac"){//Orden ASCENDENTE
                            $QueryOne = "SELECT pk_matricula, nombre, apellido_p, apellido_m FROM alumnos WHERE fk_periodo = '$key_periodo' AND estado = 'Aceptado' ORDER BY apellido_p ASC LIMIT ?,? ";
                       }else if($acdc == "dc"){//Orden DESCENDENTE
                            $QueryOne = "SELECT pk_matricula, nombre, apellido_p, apellido_m FROM alumnos WHERE fk_periodo = '$key_periodo' AND estado = 'Aceptado' ORDER BY apellido_p DESC LIMIT ?,?";
                       }
                        
                         if ($resultsOne = executeQueryLimit($QueryOne,$OFFSET,$LIMIT)->fetchAll()){
                            $jsondata["success"] = true;
                            $jsondata["registros"] = array();
                             foreach($resultsOne as $rs) {
                            $jsondata["registros"][] = $rs;                       
                             }
                            }else{
                                 $jsondata["success"] = false;
                            }
                    break;
       }    
      
                  

}

/*-----------------------------------------
   FUNCIÓN CONTAR REGISTROS
-----------------------------------------*/
/* Deuelve los datos generales de los aspirantes pertenecientes al periodo seleccionado y con el filtro selecionado*/
function contar_Aspirantes($key_periodo,$option){
      global $jsondata;
    $QueryOne="";
       switch ($option) {
                case "1":     //Todos los aspirantes de dicho periodo
                         $QueryOne = "SELECT Count(*) FROM alumnos WHERE fk_periodo = '$key_periodo'";
                    break;
               case "2":      //Todos los aspirantes de dicho periodo que no han terminado de subir sus datos
                         $QueryOne = "SELECT Count(*) FROM alumnos INNER JOIN apartados ON (apartados.fk_matricula = alumnos.pk_matricula) WHERE alumnos.estado = 'En proceso' AND alumnos.fk_periodo = '$key_periodo' AND apartados.cuestionario != '1' OR alumnos.fk_periodo = '$key_periodo' AND apartados.documentos != '1' OR alumnos.fk_periodo = '$key_periodo' AND apartados.referencias != '1'";
                    break;
               case "3":      //Todos los aspirantes de dicho periodo que ya tienen listos sus datos
                        $QueryOne = "SELECT Count(*) FROM alumnos INNER JOIN apartados ON apartados.fk_matricula = alumnos.pk_matricula WHERE alumnos.fk_periodo = '$key_periodo' AND alumnos.estado = 'En espera' AND apartados.cuestionario = '1' AND apartados.documentos = '1' AND apartados.referencias = '1'";
                    break;
               case "4":     //Todos los aspirantes de dicho periodo que ya han sido aceptados
                         $QueryOne = "SELECT Count(*) FROM alumnos WHERE fk_periodo = '$key_periodo' AND estado = 'Aceptado'";
                    break;
       }
    
     if ($resultsOne = executeQuery($QueryOne)->fetchAll()){
                            $jsondata["success"] = true;
                             foreach($resultsOne as $rs) {
                            $jsondata["rows"] = $rs['Count(*)'];                       
                             }
                            }else{
                                 $jsondata["success"] = false;
                            }
      
}

/*-----------------------------------------
   FUNCIÓN VERIFICAR DOCUMENTO EXISTENTE Y DEVOVER CAMPOS REFERENTES AL PAGO
-----------------------------------------*/
/*Verifica que exista el documento indicado*/
function documento_Exist($pk_aspirante,$document){
      global $jsondata;
    //Todos los aspirantes de dicho periodo que ya han sido aceptados
        $QueryOne = "SELECT file_comprobante_pago FROM documentos WHERE fk_matricula = '$pk_aspirante'";
        $QueryTwo = "SELECT pago_banco, pago_fecha, pago_folio, pago_referencia1, pago_expediente FROM alumnos WHERE pk_matricula = '$pk_aspirante'";
                   
            if ($resultsOne = executeQuery($QueryOne)->fetchAll()){
                
                foreach($resultsOne as $rs) {
                    if($rs['file_comprobante_pago']=='1'){ 
                        $jsondata["exist"] = true;  
                             if ($resultsTwo = executeQuery($QueryTwo)->fetchAll()){                                 
                                $jsondata["success"] = true;
                                $jsondata["banco"] = array();  
                                 foreach($resultsTwo as $rsTwo) {
                                $jsondata["banco"][] = $rsTwo; 
                                 }
                                 }else{
                                    $jsondata["success"] = false;
                                 }
                        }else{
                        $jsondata["exist"] = false;   
                    }
               }
            }else{
                 $jsondata["exist"] = false;  
            }
      
}




 ?>
