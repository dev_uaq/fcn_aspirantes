<?php

/*---------------------------------------
 DEEP MINT WAS HERE <(°_°)>
---------------------------------------*/

include('../conect.php');

//Reanuda la sesión y se traen las variables que hay en ella 
    session_start();
    $correo = $_SESSION['pk'];
    $tipo = $_SESSION['tipo'];
    $programa = $_SESSION['fk'];
    $nombre = $_SESSION['nombre'];
//Array que enviara todos los datos al JS con AJAX
$jsondata = array();

//Ejecutamos la función choose y le enviamos la acción que solicitamos
choose($_POST['accion']);

/*--------------------------------------
     FUNCIÓN ELEJIR ACCIÓN
--------------------------------------*/
//Función para elejir la acción requerida y así devolver el json correspondiente
function choose($accion){
     global $jsondata;
    switch ($accion) {
                    case "consulta-periodos":
                    consulta_Periodos();
                    break;
                    case "update_periodo":
                    update_Periodo($_POST['pk_periodo'],fechaNúmerica($_POST['new_date']));
                    break;
                    case "add_periodo":
                    add_Periodo($_POST['inicio'],$_POST['fin']);
                    break;
            
            
                
                }
     //Se envián los datos al JS
    header('Content-type: application/json; charset=utf-8');
  echo json_encode($jsondata, JSON_FORCE_OBJECT);
}


/*--------------------------------------
     FUNCIÓN AÑADIR PERIODO
--------------------------------------*/
function add_periodo($fechaInicio,$fechaFin){
     global $jsondata,$programa;       

    //Descomponemos las fecha de inicio recibida para sacar el año del periodo posteriormente
$year =  explode(' ',$fechaInicio);
$armyInicio = fechaNúmerica($fechaInicio);
$armyFin = fechaNúmerica($fechaFin);
    //Contamos los periodos que corresponden al año y programa indicado para crear el número de periodo
    $QueryThree = "SELECT Count(*) FROM periodos WHERE fk_programa = '$programa' AND año = '$year[2]'";
if ($resultsThree = executeQuery($QueryThree)->fetchAll()){
    foreach($resultsThree as $rsThree) {
        $numPeriodo = $rsThree['Count(*)']+1;
        //Agregamos el nuevo periodo
        $query="INSERT INTO periodos (fk_programa,cupo,fecha_inicio,fecha_fin,año,periodo_año) VALUES(?,?,?,?,?,?)";
       if(executeQueryArray($query,array($programa,"0",$armyInicio,$armyFin,$year[2],$numPeriodo))){
        $jsondata["success"] = true;
       }else{
        $jsondata["success"] = false;
       }
    }
}else{
    $jsondata["success"] = false;
}  
}


/*--------------------------------------
   FUNCIÓN ARMAR TEXTO PERIODO 2
--------------------------------------*/
//Función para armar el texto del tipo "Periodo Enero / Febrero 2017" a travéz de una fecha de inicial y una final
function armarTextoFecha2($fechaUno,$fechaDos){    
 $fecha_Inicio = explode('-',$fechaUno);
 $fecha_Fin = explode('-',$fechaDos);  
    
//Se obtienen los nombres de los meses utilizando la función sacarMes
 $mes_Inicio = sacarMes($fecha_Inicio[1]);
 $mes_Fin = sacarMes($fecha_Fin[1]);
 
//Se arma el texto del periodo

     $textoFecha = $fecha_Inicio[2]." ".$mes_Inicio." ".$fecha_Inicio[0]." hasta ".$fecha_Fin[2]." ".$mes_Fin." ".$fecha_Fin[0];
 
    return $textoFecha;    
}

/*--------------------------------------
     FUNCIÓN OBTENER NOMBRE DEL MES
--------------------------------------*/
//Función para obtener el nombre del mes con el número
function sacarMes($mesNumber){
    $textMes="Any";
    switch ($mesNumber) {
                case "1":
                     $textMes = "Enero";
                    break;
                case "2":
                     $textMes = "Febrero";
                    break;
                case "3":
                     $textMes = "Marzo";
                    break;
                case "4":
                     $textMes = "Abril";
                    break;
                case "5":
                     $textMes = "Mayo";
                    break;
                case "6":
                     $textMes = "Junio";
                    break;
                case "7":
                     $textMes = "Julio";
                    break;
                case "8":
                     $textMes = "Agosto";
                    break;
                case "9":
                     $textMes = "Septiembre";
                    break;
                case "10":
                     $textMes = "Octubre";
                    break;
                case "11":
                     $textMes = "Noviembre";
                    break;
                case "12":
                     $textMes = "Diciembre";
                    break;
                
                }
    return $textMes;
}

/*--------------------------------------
     FUNCIÓN OBTENER FECHA NÚMERICA
--------------------------------------*/
//Función para obtener la fecha en formato de MySql
function fechaNúmerica($date){
    $textMes="Any";
     $fecha = explode(' ',$date);  
    switch ($fecha[1]) {
                case "Enero":
                     $textMes = 1;
                    break;
                case "Febrero":
                     $textMes = 2;
                    break;
                case "Marzo":
                     $textMes = 3;
                    break;
                case "Abril":
                     $textMes = 4;
                    break;
                case "Mayo":
                     $textMes = 5;
                    break;
                case "Junio":
                     $textMes = 6;
                    break;
                case "Julio":
                     $textMes = 7;
                    break;
                case "Agosto":
                     $textMes = 8;
                    break;
                case "Septiembre":
                     $textMes = 9;
                    break;
                case "Octubre":
                     $textMes = 10;
                    break;
                case "Noviembre":
                     $textMes = 11;
                    break;
                case "Diciembre":
                     $textMes = 12;
                    break;
                
                }
    $completeDate = $fecha[2]."-".$textMes."-".$fecha[0];
    return $completeDate;
}

/*--------------------------------------
     FUNCIÓN ACTUALIZAR PERIODO
--------------------------------------*/
//Función para obtener el nombre del mes con el número
function update_periodo($periodo,$fecha){
     global $jsondata;
//Consulta la fecha correspondiente al periodo para evitar que sea menor la fecha fin a la inicial      
    $QueryThree = "SELECT fecha_inicio FROM periodos WHERE pk_periodo = '$periodo'";
    $Query = "UPDATE periodos SET fecha_fin = '$fecha' WHERE pk_periodo = '$periodo'";
if ($resultsThree = executeQuery($QueryThree)->fetchAll()){
     $jsondata["success"] = true;
    foreach($resultsThree as $rsThree) {
        $fecha_Inicio = explode('-',$rsThree['fecha_inicio']);
        $fecha_Fin = explode('-',$fecha);
        if($fecha_Inicio[0]<= $fecha_Fin[0]){
            if($fecha_Inicio[1]< $fecha_Fin[1]){
                 $jsondata["able"] = true;
                    if(executeQuery($Query)){
                     $jsondata["success"] = true;
                    }else{
                         $jsondata["success"] = false;
                    }
            }else if($fecha_Inicio[1]== $fecha_Fin[1]){
                    if($fecha_Inicio[2]< $fecha_Fin[2]){
                            $jsondata["able"] = true;
                            if(executeQuery($Query)){
                             $jsondata["success"] = true;
                            }else{
                                 $jsondata["success"] = false;
                            }
                    }else{
                        $jsondata["able"] = false;
                    }
                   
            }else{
                 $jsondata["able"] = false;
            }
        }else{
            $jsondata["able"] = false;
        }
    }
}else{
    $jsondata["success"] = false;
}

   
}

/*-----------------------------------------
   FUNCIÓN CONSULTA DE PERIODOS
-----------------------------------------*/
function consulta_Periodos(){
 global $jsondata, $programa, $nombre;

    //Nombre del usuario
    $jsondata[nombre] = $nombre; 
/*-----------------------------------------
   CONSULTAR NOMBRE DEL PROGRAMA ACADÉMICO
-----------------------------------------*/
//Consulta el nombre del programa al que pertenece el usuario
$QueryOne = "SELECT nombre FROM programas WHERE pk_programa = '$programa'";
$resultsOne = executeQuery($QueryOne)->fetchAll();
 foreach($resultsOne as $rs) {
     $jsondata["programa"] = $rs['nombre']; 
 }


/*--------------------------------------
   CONSULTAR PERIODOS
--------------------------------------*/   
    $squareOne=0;
    $squareTwo=0;
    $rowMaster =array();
    $rowChild = array();
//Consulta de los periodos correspondientes al programa del usuario      
    $QueryThree = "SELECT DISTINCT año FROM periodos WHERE fk_programa = '$programa' ORDER BY año DESC";
if ($resultsThree = executeQuery($QueryThree)->fetchAll()){    
    $jsondata["periodos"] = array();
    foreach($resultsThree as $rsThree) {
    $squareOne=0;
    $year =$rsThree['año'];
    $QueryTwo = "SELECT pk_periodo, fecha_inicio, fecha_fin, periodo_año FROM periodos WHERE fk_programa = '$programa' AND año='$year' ORDER BY  periodo_año ASC";
    if ($resultsTwo = executeQuery($QueryTwo)->fetchAll()){
    $jsondata["success"] = true;
     foreach($resultsTwo as $rs) {
         $row = array();
         $row[0] = $rs['pk_periodo'];
         $row[1] = armarTextoFecha2($rs['fecha_inicio'],$rs['fecha_fin']);
         $row[2] = $rs['periodo_año'];
         $row[3] = $year;
         $row[4] = periodoActivo($rs['pk_periodo']);
        $rowChild[$squareOne]=$row;
            $squareOne++;
     } 
        $rowMaster[$squareTwo]=$rowChild;
        $squareTwo++;
    }else
    {
       $jsondata["success"] = false;
    }
        
    }    
    $jsondata["periodos"] = $rowMaster;
}else{
    $jsondata["success"] = false;
}

 
     
}





/*--------------------------------------
   FUNCIÓN PERIODO ACTIVO
--------------------------------------*/  
/*función para determinar si un periodo esta activo retornando un valor distinto para cada situación, "before": periodo aún no vigente, "after": periodo ya pasado, "just_now": periodo activo justo ahora*/
function periodoActivo($id_periodo){
    global $jsondata;
$yes_no_maybe="I don´t now, can you repeat the question?";
/*----------------------------------------------------------------
   CONSULTAR FECHA FINAL DE PERIODO
----------------------------------------------------------------*/ 
$fecha_inicio;
$fecha_fin;
$fecha_actual;
$QueryOne = "SELECT fecha_inicio, fecha_fin FROM periodos WHERE pk_periodo = '$id_periodo'";
if ($resultsOne = executeQuery($QueryOne)->fetchAll()){
     foreach($resultsOne as $rs) {
         $fecha_inicio = $rs['fecha_inicio'];
         $fecha_fin = $rs['fecha_fin'];
     } 
    
    //Limpiamos el statement de memoria
   // limpiarStm($resultsTwo);
    
        /* Se compara la fecha actual y la fecha final y de inicio del periodo para ver si esta habilitado */
        
    date_default_timezone_set('America/Mexico_City');

$fecha_actual = date("Y-m-d");

        if($fecha_actual < $fecha_inicio){
                $yes_no_maybe = "before";
            $jsondata["date"] = $fecha_actual." before"; 
        }else if($fecha_actual > $fecha_fin){
                $yes_no_maybe = "after";
           $jsondata["date"] = $fecha_actual." after";
        }else{
            $yes_no_maybe = "just_now";
            $jsondata["date"] = $fecha_actual." just_now";
        }
}else
{
   $yes_no_maybe="error";
}    
    return $yes_no_maybe;
}






 ?>
