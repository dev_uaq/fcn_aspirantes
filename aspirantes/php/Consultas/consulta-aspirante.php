<?php

/*---------------------------------------
 DEEP MINT WAS HERE <(°_°)>
---------------------------------------*/

include('../conect.php');

//Reanuda la sesión y se traen las variables que hay en ella 
    session_start();
    $pk_aspirante = $_SESSION['pk_aspirante'];
    $tipo = $_SESSION['tipo'];
    $programa = $_SESSION['fk'];
    $nombre = $_SESSION['nombre'];

//Array que enviara todos los datos al JS con AJAX
$jsondata = array();

choose($_POST['accion']);

function choose($option){
    global $jsondata;
    switch ($option){
        case "consulta-inicial":
        consultaInicial();
        break;    
        case "consultar-ponderación":
            consultarPonderaciones();
        break;
        case "ponderar":
            ponderar($_POST['itemPk'],$_POST['itemReachValue']);
        break;
        case "aceptar-aspirante":
            aceptarAspirante();
        break;
    }
    
    header('Content-type: application/json; charset=utf-8');
echo json_encode($jsondata, JSON_FORCE_OBJECT);

}

/*-----------------------------------------
 FUNCIÓN CONSULTA INICIAL
 -----------------------------------------*/ 
function consultaInicial(){ global $jsondata,$programa,$pk_aspirante,$nombre;
//Nombre del usuario
    $jsondata[nombre] = $nombre; 
/*-----------------------------------------
   CONSULTAR NOMBRE DEL PROGRAMA ACADÉMICO
-----------------------------------------*/
//Consulta el nombre del programa al que pertenece el usuario
$QueryOne = "SELECT nombre FROM programas WHERE pk_programa = '$programa'";
$resultsOne = executeQuery($QueryOne)->fetchAll();
 foreach($resultsOne as $rs) {
     $jsondata["programa"] = $rs['nombre']; 
 }

/*--------------------------------------------------------------
CONSULTAR ASPIRANTE
--------------------------------------------------------------*/    

                    //Consulta alumno/aspirante
                    $QueryOne = "SELECT * FROM alumnos WHERE pk_matricula = '$pk_aspirante'";
                    if ($resultsOne = executeQuery($QueryOne)->fetchAll()){
                    $jsondata["success"] = true;
                    $jsondata["aspirante"] = array();
                     foreach($resultsOne as $rs) {
                         $jsondata["aspirante"] = $rs;
                     } 
                        

                    }else
                    {
                       $jsondata["success"] = false;
                    }

/*--------------------------------------------------------------
CONSULTAR DOCUMENTOS DEL ASPIRANTE
--------------------------------------------------------------*/    
                    
                    $QueryOne = "SELECT * FROM documentos WHERE fk_matricula = '$pk_aspirante'";
                    if ($resultsOne = executeQuery($QueryOne)->fetchAll()){
                    $jsondata["success_documentos"] = true;
                    $jsondata["aspirante_documentos"] = array();
                     foreach($resultsOne as $rs) {
                         $jsondata["aspirante_documentos"] = $rs;
                     } 
                        

                    }else
                    {
                       $jsondata["success_documentos"] = false;
                    }

/*--------------------------------------------------------------
CONSULTAR RECOMENDACIONES DEL ASPIRANTE
--------------------------------------------------------------*/    
                    
                    
                    $QueryOne = "SELECT * FROM recomendaciones WHERE fk_matricula = '$pk_aspirante'";
                    $QueryTwo = "SELECT referencias FROM apartados WHERE fk_matricula = '$pk_aspirante'";
                    if ($resultsTwo = executeQuery($QueryTwo)->fetchAll()){
                    $jsondata["success_recomendaciones"] = true;
                    $jsondata["aspirante_recomendaciones"] = array();
                     foreach($resultsTwo as $rsTwo) {
                         if($rsTwo['referencias'] =="1"){
                               if ($resultsOne = executeQuery($QueryOne)->fetchAll()){
                                $jsondata["aspirante_recomendaciones"] = array();
                                 foreach($resultsOne as $rs) {
                                     $jsondata["aspirante_recomendaciones"][] = $rs;
                                 }                        

                    }else
                    {
                       $jsondata["success_recomendaciones"] = false;
                    }
                         }else{
                              $jsondata["success_recomendaciones"] = false;
                         }
                     }                       

                    }else
                    {
                       $jsondata["success_recomendaciones"] = false;
                    }

}

/*-----------------------------------------
 FUNCIÓN CONSULTAR PONDERACIONES
 -----------------------------------------*/
function consultarPonderaciones(){/*Función que recibira una lista de los items de ponderación para poblar los sliders*/
    global $jsondata,$programa,$pk_aspirante;
    
    $idAndFlag = periodoYBandera();
/*--------------------------------------------------------------
CONSULTAR ITEMS DE PONDERACIÓN
--------------------------------------------------------------*/    
                    if($idAndFlag[0]=='1'){
                             $Query = "SELECT * FROM ponderacion_aspirante WHERE fk_matricula = '$pk_aspirante'";
                        $jsondata["flag_ponderación"] = true;
                         }else{
                              $Query = "SELECT * FROM ponderaciones WHERE fk_programa = '$programa'";
                        $jsondata["flag_ponderación"] = false;
                         }
                   $QueryOne = $Query;
                    
                    if ($resultsOne = executeQuery($QueryOne)->fetchAll()){
                    $jsondata["success_ponderación"] = true;
                    $jsondata["items_ponderación"] = array();
                     foreach($resultsOne as $rs) {
                         $jsondata["items_ponderación"][] = $rs;
                     } 
                        

                    }else
                    {
                       $jsondata["success_ponderación"] = false;
                    }
    
    //periodo activo
    statusPeriodo($idAndFlag[1]);
    
}

/*--------------------------------------
   FUNCIÓN QUE DEVOLVERA UNA SERIE DE DATOS UTILIES PARA REALIZAR OTRAS CONSULTAS
--------------------------------------*/  
function periodoYBandera(){
    global $jsondata,$pk_aspirante;
    $util = array();
    //Consulta alumno/aspirante
                    $QueryOne = "SELECT fk_periodo, flag_ponderacion FROM alumnos WHERE pk_matricula = '$pk_aspirante'";
                    if ($resultsOne = executeQuery($QueryOne)->fetchAll()){
                    $jsondata["success"] = true;
                    $jsondata["aspirante"] = array();
                     foreach($resultsOne as $rs) {
                         $util[0]=$rs['flag_ponderacion'];                        
                         $util[1] = $rs['fk_periodo'];
                     }                        

                    }else
                    {
                       $jsondata["success"] = false;
                    }
    return $util;
}
/*--------------------------------------
   FUNCIÓN PERIODO ACTIVO
--------------------------------------*/  
function statusPeriodo($id_periodo){
    global $jsondata;
/*función para determinar si un periodo esta activo retornando un valor distinto para cada situación, "before": periodo aún no vigente, "after": periodo ya pasado, "just_now": periodo activo justo ahora*/
$yes_no_maybe="I don´t now, can you repeat the question?";
/*----------------------------------------------------------------
   CONSULTAR FECHA FINAL DE PERIODO
----------------------------------------------------------------*/ 
$fecha_inicio;
$fecha_fin;
$fecha_actual;
$QueryOne = "SELECT fecha_inicio, fecha_fin FROM periodos WHERE pk_periodo = '$id_periodo'";
if ($resultsOne = executeQuery($QueryOne)->fetchAll()){
     foreach($resultsOne as $rs) {
         $fecha_inicio = $rs['fecha_inicio'];
         $fecha_fin = $rs['fecha_fin'];
     } 
    
        /* Se compara la fecha actual y la fecha final y de inicio del periodo para ver si esta habilitado */
        
    date_default_timezone_set('America/Mexico_City');

$fecha_actual = date("Y-m-d");

        if($fecha_actual < $fecha_inicio){
                $yes_no_maybe = "before";
        }else if($fecha_actual > $fecha_fin){
                $yes_no_maybe = "after";
        }else{
            $yes_no_maybe = "just_now";
        }
}else
{
   $yes_no_maybe="error";
}    
    $jsondata["periodo_activo"] = $yes_no_maybe;
}

/*-----------------------------------------
 FUNCIÓN INSERTAR PONDERACIONES
 -----------------------------------------*/
function ponderar($fkItems,$reachValues){
    $flag_ponderacion = periodoYBandera();
    if($flag_ponderacion[0]=='1'){
    actualizarPonderaciones($fkItems,$reachValues);    
        
    }else{
    insertarPonderaciones($reachValues);    
        
    }
        
}
/*-----------------------------------------
 FUNCIÓN INSERTAR PONDERACIONES
 -----------------------------------------*/
function insertarPonderaciones($reachValues){
    global $pk_aspirante,$programa,$jsondata;
    $countIndex=0;
    $QueryOne = "SELECT * FROM ponderaciones WHERE fk_programa = '$programa'";           
    if ($resultsOne = executeQuery($QueryOne)->fetchAll()){
        foreach($resultsOne as $rs) {
                    $query="INSERT INTO ponderacion_aspirante (pk_ponderacion,fk_matricula,ponderacion,valor,valor_alcanzado) VALUES(?,?,?,?,?)";
                   if(executeQueryArray($query,array($rs['pk_ponderacion'],$pk_aspirante,$rs['ponderacion'],$rs['valor'],$reachValues[$countIndex]))){
                    $jsondata["success"] = true;
                   }else{
                    $jsondata["success"] = false;
                   }  
            $countIndex++;
        }
       actualizarFlagPonderacion();
    }else{
        $jsondata["success"] = false;
    }
      
}
/*-----------------------------------------
 FUNCIÓN ACTUALIZAR PONDERACIONES
 -----------------------------------------*/
function actualizarPonderaciones($fkItems,$reachValues){    
     global $pk_aspirante,$programa,$jsondata;
    $countIndex=0;
    $QueryOne = "SELECT * FROM ponderacion_aspirante WHERE fk_matricula = '$pk_aspirante'";           
    if ($resultsOne = executeQuery($QueryOne)->fetchAll()){
        foreach($resultsOne as $rs) {
                    $query="UPDATE ponderacion_aspirante SET valor_alcanzado = '$reachValues[$countIndex]' WHERE pk_ponderacion='$fkItems[$countIndex]'AND fk_matricula = '$pk_aspirante'";
                   if(executeQuery($query)){
                    $jsondata["success"] = true;
                   }else{
                    $jsondata["success"] = false;
                   }  
            $countIndex++;
        }
    }else{
        $jsondata["success"] = false;
    }    
}

/*-----------------------------------------
 FUNCIÓN ACTUALIZAR FLAG PONDERACION
 -----------------------------------------*/
function actualizarFlagPonderacion(){
    global $pk_aspirante;
     $Query = "UPDATE alumnos SET flag_ponderacion = '1' WHERE pk_matricula = '$pk_aspirante'";
                    if(executeQuery($Query)){
                     $jsondata["success"] = true;
                    }else{
                         $jsondata["success"] = false;
                    }
}

/*-----------------------------------------
 FUNCIÓN ACEPTAR ASPIRANTE
 -----------------------------------------*/
function aceptarAspirante(){    
     global $pk_aspirante,$nombre,$jsondata;
    //CheckPointVar
    $CheckPoint = false;
    //Datos del correo
    $titulo;
    $mensaje;
    $cabeceras;    
    
    //Datos del coordinador    
    $NombreCoordi;
    $ApellidoPCoordi;
    $ApellidoMCoordi;
    //Datos del aspirante    
    $Nombre;
    $Correo;
    $ApellidoP;
    $ApellidoM;
    $NombreCarrera;
    $Siglas;
    $Sexo;
    
    $QueryOne = "SELECT * FROM alumnos WHERE pk_matricula = '$pk_aspirante'";
            if ($resultsOne = executeQuery($QueryOne)->fetchAll()){
                     foreach($resultsOne as $rs) {
                         $Nombre = $rs['nombre'];
                         $Correo = $rs['correo'];
                         $ApellidoP = $rs['apellido_p'];
                         $ApellidoM = $rs['apellido_m'];
                         $fk_Programa = $rs['fk_programa'];
                         $Sexo = $rs['sexo'];
                            $QueryTwo = "SELECT nombre,siglas FROM programas WHERE pk_programa = '$fk_Programa'";
                            if ($resultsTwo = executeQuery($QueryTwo)->fetchAll()){
                             foreach($resultsTwo as $rs) {
                                $NombreCarrera = $rs['nombre'];
                                $Siglas = $rs['siglas'];
                             }
                                $CheckPoint = true;
                             }
                         if($CheckPoint){
                            $QueryThree = "SELECT nombre,apellido_p,apellido_m FROM usuarios   WHERE fk_programa = '$fk_Programa'";
                            if ($resultsThree = executeQuery($QueryThree)->fetchAll()){
                             foreach($resultsThree as $rs) {
                                 $NombreCoordi = $rs['nombre'];
                                 $ApellidoPCoordi = $rs['apellido_p'];
                                 $ApellidoMCoordi = $rs['apellido_m'];
                             }                                
                             }else{
                                $CheckPoint = true;
                            }
                         }
                     }
    //Identificar sí es Maestria o Doctorado
                $TextoAceptado;
                if($Sexo == 'Masculino'){
                $TextoAceptado = "Aceptado";
                }else if($Sexo == 'Femenino'){                  
                $TextoAceptado = "Aceptada";
                }else{
                $TextoAceptado = "Aceptado(a)";
                }
    //Identificar sí es Maestria o Doctorado
                $MasterODocto;
                if($Siglas == 'DCB'){
                    $MasterODocto = "el ".$NombreCarrera;
                }else{
                    $MasterODocto = "la ".$NombreCarrera;                    
                }
date_default_timezone_set('America/Mexico_City');
setlocale(LC_TIME,"es_ES");
$Fecha = strftime("Juriquilla, Querétaro a %d de %B del %Y");
      
    // titulo
$titulo = 'Facultad de Ciencias Naturales';

// mensaje
$mensaje = '<html>
<head>
    <title>Proceso de admisi&oacute;n</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body style="background-color:#C8E6C9;">
    <table style="width: 600px; padding: 10px; margin:0 auto; border-collapse: collapse;">
        <tr>
            <td style="text-align: center">
                <img src="http://aspirantes.posgradosfcn-uaq.com/img/logos/icouaq.png" width="100px" height="100px">
            </td>
            <td>
                <h3 style="text-align: center; color: #00C853; margin: 0 0 12px; font-size: 30px; font-family: inherit">Universidad Aut&oacute;noma de Quer&eacute;taro</h3>
                <h3 style="text-align: center; color: #00C853; margin: 0 0 12px; font-size: 20px; font-family: inherit">Facultad de Ciencias Naturales</h3>
            </td>
            <td style="text-align: center">
                <img src="http://aspirantes.posgradosfcn-uaq.com/img/logos/icofc.png" width="100px" height="100px">
            </td>
        </tr>
    </table>
    <br>
    <table style="width: 600px; margin:0 auto; border-collapse: collapse;" >
        <tr>
            <td style="background-color:#FFFFFF;">
                <div style="color: #424242; margin: 4% 10% 2%; text-align: justify;font-family: sans-serif">
                    <h4 style="color: #424242; margin: 0 0 7px; text-align:right">'.$Fecha.'</h4>
                    <h3 style="color: #212121; margin: 0 0 7px">'.$Nombre.' '.$ApellidoP.' '.$ApellidoM.'</h3>
                    <h3 style="color: #212121; margin: 0 0 7px">PRESENTE</h3>
                    <p style="margin: 2px; font-size: 15px">
                    <br>
                    Es para mí un honor comunicarle que ha sido usted <B>'.$TextoAceptado.'</B> para cursar '.$MasterODocto.' en esta Institución. 
                    <br>
                    <br>
                    La reunión oficial de bienvenida tendrá lugar en el auditorio de la Facultad de Ciencias Naturales el día 28 de julio a las 15:00 hrs. Por lo que pedimos su puntual asistencia ya que se le darán indicaciones para llevar a cabo su registro oficial en nuestra Universidad.
                    <br>
                    <br>
                    Reciba a nombre del grupo de académicos del programa una sincera felicitación y nuestros mejores deseos para que logre con éxito esta meta. 
                    </p>
                    <br>
                    <br>
                    <div style="text-align: center">
                    <h3 style="color: #212121; margin: 0 0 7px">ATENTAMENTE</h3>
                    <h3 style="color: #212121; margin: 0 0 7px">'.$NombreCoordi.' '.$ApellidoPCoordi.' '.$ApellidoMCoordi.'</h3>
                    <h3 style="color: #212121; margin: 0 0 7px">Coordinador(a)</h3>
                    <h3 style="color: #212121; margin: 0 0 7px">'.$NombreCarrera.'</h3>
                    </div>
                    <div style="text-align: center">
                     <img src="http://aspirantes.posgradosfcn-uaq.com/img/logos/'.$Siglas.'.png" width="150px" height="150px">
                    </div>                     
                    <p style="color: #b3b3b3; font-size: 12px; text-align: center;margin: 30px 0 0">UAQ, Facultad de Ciencias Naturales.</p>
                </div>
            </td>
        </tr>
    </table>
    <br>
    <br>
    <br>
    <br>
</body>
</html>';

// Para enviar un correo HTML, debe establecerse la cabecera Content-type
$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
$cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

// Cabeceras adicionales
$cabeceras .= 'To:'.$Nombre.'<'.$Correo.'>'."\r\n";
$cabeceras .= 'From: Facultad de Ciencias Naturales <fcn@posgradosfcn-uaq.com>' . "\r\n";

}
    
            if($CheckPoint){
                   $SendEmail = mail($Correo,$titulo,$mensaje,$cabeceras);
                    if($SendEmail){
                        //Sentencia MySQL
                        $query="UPDATE alumnos SET estado = 'En proceso' WHERE pk_matricula = '$pk_aspirante'";
                        if(executeQuery($query)){
                            $jsondata["success"] = true;
                            $jsondata["mailto"] = $Correo;
                        }else{
                            $jsondata["success"] = false;
                            $jsondata["hereIs"] = "Update";
                        } 
                    }else{
                        $jsondata["success"] = false;
                        $jsondata["hereIs"] = "SendEmail";
                    }                  
            }else{
                $jsondata["success"] = false;
                $jsondata["hereIs"] = "Checkpoint";
            }

}

 ?>
