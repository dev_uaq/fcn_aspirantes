<?php
//Obtener funcion
$funcion=$_GET['funcion'];

//Elegir operacion a realizar
switch($funcion){
    case 'añadirPonderacion':
        añadirPonderacion();
        break;
    case 'consultarPonderaciones':
        consultarPonderaciones();
        break;
    case 'consultarPrograma':
        consultarPrograma();
        break;
    case 'llenarModal':
        llenarModal();
        break;
    case 'editarPonderacion':
        editarPonderacion();
        break;
    case 'rangoPonderacion':
        rangoPonderacion();
        break;
    case 'eliminarPonderacion':
        eliminarPonderacion();
        break;
    case 'verificarPeriodoActual':
        verificarPeriodoActual();
        break;
    default:
        echo "No se encontro la funcion que intenta llamar: ".$funcion;
        break;
}

//funciones

//Añade una nueva ponderacion
function añadirPonderacion(){
    //conexion a la base de datos
    include("../conect.php");
    
    //obntener datos del formulario
    $nombre=$_POST['nombrePonderacion'];
    $valor=$_POST['valorPonderacion'];
    session_start();
    $programa=$_SESSION['fk'];
    
    //sentencia SQL
    $sql="insert into ponderaciones (fk_programa,ponderacion,valor) values(?,?,?)";
    
    $datos=array($programa,$nombre,$valor);
    
    if(executeQueryArray($sql,$datos)){
        echo "Hecho";
    }else{
        echo "error";
    }
}

//consulta las ponderaciones de un programa
function consultarPonderaciones(){
    //conexion a la base de datos
    include("../conect.php");
    
    //obntener datos del programa
    session_start();
    if(isset($_SESSION['fk'])){
        $programa=$_SESSION['fk'];

        //sentencia sql
        $sql="select * from ponderaciones where fk_programa='".$programa."';";

        echo resultQueryJson($sql);
    }else echo "salir";
}

//consulta la infromacion del programa
function consultarPrograma(){
    //conexion a la base de datos
    include("../conect.php");
    
    //obntener datos del programa
    session_start();
    if(isset($_SESSION['fk'])){
        $programa=$_SESSION['fk'];
        $nombre = $_SESSION['nombre'];
        
        $result['nombre']=$nombre;
        //sentencia sql
        $sql="select nombre from programas where pk_programa='".$programa."';";
        
        $r=resultQuery($sql);
        $result['programa']=$r[0]['nombre'];
        
        echo json_encode($result);
        
    }else echo "salir";
}

//llena el modal de edicion
function llenarModal(){
    //conexion a la base de datos
    include("../conect.php");
    
    //obntener datos del programa
    session_start();
    if(isset($_SESSION['fk'])){
        $programa=$_SESSION['fk'];

        $id=$_GET['id'];
        //sentencia sql
        $sql="select * from ponderaciones where pk_ponderacion='".$id."';";

        echo resultQueryJson($sql);
    }else echo "salir";
}

//Editar Ponderacion
function editarPonderacion(){
    //coneccion a la base de datos
    include("../conect.php");
    
    //obntener datos del programa
    session_start();
    if(isset($_SESSION['fk'])){
        $programa=$_SESSION['fk'];
        
        //obtencion de datos
        $id=$_POST['idPonderacion'];
        $nombre=$_POST['editarNombrePonderacion'];
        $valor=$_POST['editarValorPonderacion'];
        
        //sentencia sql
        $sql='update ponderaciones set ponderacion=?,valor=? where pk_ponderacion=? and fk_programa=?;';
        
        $datos=array($nombre,$valor,$id,$programa);
        
        if(executeQueryArray($sql,$datos)){
            echo "hecho";
        }else echo "error";

    }else echo "salir";
}

//Obtiene el valor disponible para agregar una nueva ponderacion
function rangoPonderacion(){
    //conexion a la base de datos
    include("../conect.php");
    
    //obntener datos del programa
    session_start();
    if(isset($_SESSION['fk'])){
        $programa=$_SESSION['fk'];

        //sentencia sql
        $sql="select valor from ponderaciones where fk_programa='".$programa."';";
        
        $valores=resultQuery($sql);
        $disponible=0;
        
        foreach($valores as $valor){
            $disponible+=$valor['valor'];
        }

        echo 100-$disponible;
    }else echo "salir";
}

//Elimina una ponderacion
function eliminarPonderacion(){
    //conexion a la base de datos
    include("../conect.php");
    
    //obntener datos del programa
    session_start();
    if(isset($_SESSION['fk'])){
        $programa=$_SESSION['fk'];
        $id=$_GET['id'];

        //sentencia sql
        $sql="delete from ponderaciones where pk_ponderacion='".$id."' and fk_programa='".$programa."'";
        
        if(executeQuery($sql)){
            echo "hecho";
        }else echo "error";
        

    }else echo "salir";
}

function verificarPeriodoActual(){
    //conexion a la base de datos
    include("../conect.php");
    
    //obtener datos del programa
    session_start();
    if(isset($_SESSION['fk'])){
        $programa=$_SESSION['fk'];
        //obtener fecha actual
        // Establecer la zona horaria predeterminada a usar. Disponible desde PHP 5.1
            date_default_timezone_set('UTC');
            $año=date("Y");
            $fechaActual=date("Y-m-d");
        
        //consulta SQL
        $SQL="SELECT * from periodos INNER JOIN programas on (periodos.fk_programa = programas.pk_programa) WHERE pk_programa='".$programa."' and '".$fechaActual."' BETWEEN periodos.fecha_inicio and periodos.fecha_fin;";
        
        echo $resultado=getCount($SQL);
    }else{
        echo "Salir";
    }
    
}


?>