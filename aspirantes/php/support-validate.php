<?php

//Obtener función
$funcion=$_GET['f'];

/*************************************
Se evalua la funcion que se solicita
*************************************/
switch($funcion){
    case 'redirect':
        redireccionar();
        break;
        case 'redirectUser':
        redireccionarVer();
        break;
    case 'focus':
        enfocarPregunta();
        break;
    default:
        echo "La función que intenta llamar no existe: ".$funcion;
        break;
}

/*************************************
Obtiene los datos de la pregunta
a la que se le dara soporte y crea una sesion para los desarrolladores
y una mas para indicar el numero de respuesta
*************************************/
function redireccionar(){
    //Conexion a la base de datos
    include("conect.php");

    //Obtener los datos
    $QuienPregunta=$_GET['n'];
    $Clave=$_GET['u'];
    $Pregunta=$_GET['p'];

    //Restringir uso solo a link armado
    if($Clave=="Ch3m4yW1lfr13d"){

        //Consultar numero de pregunta
        $SQL="Select pk_pregunta from soporte_preguntas where fk_usuario_correo='{$QuienPregunta}' and pregunta='{$Pregunta}'";
        $NumeroPregunta=resultQuery($SQL)[0]['pk_pregunta'];

        //Mostrar datos
        echo "Quien preguna: ".$QuienPregunta."<br>";
        echo "Pregunta: ".$Pregunta."<br>";
        echo "NumeroPregunta: ".$NumeroPregunta."<br>";
        echo "Clave: ".$Clave."<br>";
        echo "SQL: ".$SQL."<br>";

        //Creación de sesiones para desarrolladores
        session_start();
            //Buscar como cerrar todas las sesiones;
        $_SESSION['nombre'] = "Desarrollador";
        $_SESSION['tipo'] = "Developer";
        $_SESSION['numeroPregunta'] = $NumeroPregunta;

        header("location: ../soporte.html");
    }else echo "Acceso restringido...";
}

/*************************************
Devuelve el numero de pregunta a enfocar
*************************************/
function enfocarPregunta(){
    session_start();
    if(isset($_SESSION['numeroPregunta'])){
    $n=$_SESSION['numeroPregunta'];
    $_SESSION['numeroPregunta']="";
    echo $n;
    }
}

function redireccionarVer(){

    //Conexion a la base de datos
    include("conect.php");

    //Obtener los datos
    $Usuario=$_GET['u'];
    $Pregunta=$_GET['p'];
    $Clave=$_GET['cl'];
    $Tipo=$_GET['t'];
    $NumeroPregunta;
    $Matricula;
    $SQL;

    //Restrigir uso solo a link armado
    if($Clave=="Ch3m4yW1lfr13d"){

        //Creación de sesiones para desarrolladores
        session_start();
            //Buscar como serrar todas las sesiones;
        if($Tipo=="aspirante"){
            
            //Consultar numero de pregunta y datos del aspirante
            $SQL="Select soporte_preguntas.pk_pregunta,alumnos.pk_matricula from soporte_preguntas INNER JOIN alumnos ON (alumnos.correo = soporte_preguntas.email) where fk_usuario_correo='{$Usuario}' and pregunta='{$Pregunta}';";
            $NumeroPregunta=resultQuery($SQL)[0]['pk_pregunta'];
            $Matricula=resultQuery($SQL)[0]['pk_matricula'];
            
            $_SESSION['pk'] = $Matricula;
            $_SESSION['nombre'] = $Usuario;
            $_SESSION['tipo'] = $Tipo;
            $_SESSION['numeroPregunta'] = $NumeroPregunta;
        }else if ($Tipo!="aspirante" && $Tipo!=""){
            
            //Consultar numero de pregunta y datos del aspirante
            $SQL="Select soporte_preguntas.pk_pregunta,usuarios.pk_correo,usuarios.fk_programa from soporte_preguntas INNER JOIN usuarios ON (usuarios.pk_correo = soporte_preguntas.email) where fk_usuario_correo='{$Usuario}' and pregunta='{$Pregunta}';";
            $NumeroPregunta=resultQuery($SQL)[0]['pk_pregunta'];
            $Matricula=resultQuery($SQL)[0]['pk_correo'];
            $fkPrograma=resultQuery($SQL)[0]['fk_programa'];
            
            $_SESSION['pk'] = $Matricula;                                
            $_SESSION['tipo'] = $Tipo;                        
            if($Tipo=="Coordinador") $_SESSION['fk'] = $fkPrograma;
            $_SESSION['nombre'] = $Usuario;
        }
        
        //Mostrar datos s
        echo "Quien Pregunta: ".$Usuario."<br>";
        echo "Pregunta: ".$Pregunta."<br>";
        echo "NumeroPregunta: ".$NumeroPregunta."<br>";
        echo "Clave: ".$Clave."<br>";
        echo "Tipo: ".$Tipo."<br>";
        echo "Matricula: ".$Matricula."<br>";
        if($Tipo=="Coordinador")
        echo "FkPrograma: ".$fkPrograma."<br>";
        echo "SQL: ".$SQL."<br>";      
        
        //Redireccionar a la pagina
        header("location: ../soporte.html");
    }else echo "Acceso restringido...";
}

?>