<?php
  $funcion=$_GET['funcion'];
   
  switch($funcion){
      case "registrarPregunta":
          registrarPregunta();
          break;
          
      case "registrarRespuesta":
            registrarRespuesta();
          break;
          
      case "consultarPreguntas":
          consultarPreguntas();
          break;
    
      case "consultarRespuestas":
          consultarRespuestas();
          break;
          
      case "buscarPregunta":
          buscarPregunta();
          break;
        
      case "validarUsuario":
          validarUsuario();
          break;
          
      default:
          echo "No se encuentra la funcion ".$funcion;
          break;
  }

    function registrarPregunta(){
        //Incluir archivo de conexión
        include("conect.php");
        
        //Iniciar la sesión
        session_start();
        
        //Obtener los datos necesarios
        $NombreUsuario=$_SESSION['nombre'];
        $TipoUsuario=$_SESSION['tipo'];
        $Pregunta=$_POST['inputPregunta'];
        // Establecer la zona horaria predeterminada a usar. Disponible desde PHP 5.1
        date_default_timezone_set('UTC');
        $Fecha=date("Y-m-d H:i:s");
        $Correo=$_GET['e'];

        
        //Sentencia sql
        $sql="insert into soporte_preguntas (pk_pregunta,fk_usuario_correo,pregunta,fecha,tipo,email) values ('','{$NombreUsuario}','{$Pregunta}','{$Fecha}','{$TipoUsuario}','{$Correo}');";
        if(executeQuery($sql)){
            enviarCorreo($Pregunta,$NombreUsuario,$Correo,$TipoUsuario);
        }else{
        }
    }

function registrarRespuesta(){
        //Incluir archivo de conexión
        include("conect.php");
        
        //Iniciar la sesión
        session_start();
        
        //Obtener los datos necesarios
        $NombreUsuario=$_SESSION['nombre'];
        $Respuesta=$_POST['inputAnswer'];
        $fkPregunta=$_GET['fkT'];
        // Establecer la zona horaria predeterminada a usar. Disponible desde PHP 5.1
        date_default_timezone_set('UTC');
        $Fecha=date("Y-m-d H:i:s");
        $Correo=$_GET['e'];
        $TipoUsuario=$_SESSION['tipo'];
        
        //Sentencia sql
        $sql="insert into soporte_respuestas (pk_respuesta,fk_pregunta,fk_usuario_correo,respuesta,fecha,tipo,email) values ('','{$fkPregunta}','{$NombreUsuario}','{$Respuesta}','{$Fecha}','{$TipoUsuario}','{$Correo}')";
        
        if(executeQuery($sql)){
           $SQL="Select * from soporte_preguntas where pk_pregunta='{$fkPregunta}'";
           $datos=resultQuery($SQL);
           $Usuario=$datos[0][1];
           $Correo=$datos[0]['email'];
           $Pregunta=$datos[0]['pregunta'];
             enviarCorreoRespuestaDesarrollador($Usuario,$Correo,$Respuesta,$Pregunta,$NombreUsuario);
            //echo "Hecho";
        }else{
            echo "error";
        }
    }

    function consultarPreguntas(){
        //Incluir archivo de conexión
        include("conect.php");
        
        //busqueda de todas las preguntas
        $sql="select * from soporte_preguntas;";
        
        echo resultQueryJson($sql);
    }

    function consultarRespuestas(){
        //Incluir archivo de conexión
        include("conect.php");
        $fkPregunta=$_GET['fkP'];
        //busqueda de todas las preguntas
        $sql="select * from soporte_respuestas where fk_pregunta='{$fkPregunta}';";
        
        echo resultQueryJson($sql);
    }

function enviarCorreo($Pregunta,$Nombre,$Correo,$TipoUsuario){
  //conexión
//    include("../../php/conect.php");
    
    //consultar los correos de todos los participantes
    $SQL="select pk_correo from usuarios;";
    $Correos=resultQuery($SQL);
    
    $Destinatarios="Destinatarios: ";
//    foreach($Correos as $Correo){
//       $Destinatarios=$Destinatarios.$Correo['pk_correo'].","; 
//    }
    
    ////////////////////////////////Envio de Correo//////////////////////////////////////
    // Varios destinatarios
    $para  = "josemgo.9.11@gmail.com,wilfred.ironman@gmail.com";//atención a la coma ,wilfred.ironman@gmail.com

    // titulo
    $titulo = 'Soporte-FCN';
    
    //Link de soporte desde el boton
$link='https://posgradosfcn-uaq.com/aspirantes/php/support-validate.php?n='.$Nombre.'&u=Ch3m4yW1lfr13d&f=redirect&p='.$Pregunta;
        
    // mensaje
    $mensaje = '<html>
    <head>
        <title>Proceso de admisi&oacute;n</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>

    <body>
        <table style="width: 600px; padding: 10px; margin:0 auto; border-collapse: collapse;">
            <tr>
                <td style="text-align: center">
                    <img src="http://aspirantes.posgradosfcn-uaq.com/img/logos/icouaq.png" width="100px" height="100px">
                </td>
                <td >
                    <h2  style="text-align: center; color: #3498db; margin: 0 0 12px; font-size: 30px; font-family: inherit">Soporte</h2>
                </td>
                <td style="text-align: center">
                    <img src="http://aspirantes.posgradosfcn-uaq.com/img/logos/icofc.png" width="100px" height="100px">
                </td>
            </tr>
        </table>
        <br>
        <table style="width: 600px; margin:0 auto; border-collapse: collapse;" >

            <tr>
                <td style="background-color: #ecf0f1">
                    <div style="color: #34495e; margin: 4% 10% 2%; text-align: justify;font-family: sans-serif">
                        <p style="margin: 2px; font-size: 15px">
                            ¡Hola!<br><br>
                            El usuario <strong>'.$Nombre.'</strong> <small>('.$TipoUsuario.' <'.$Correo.'>)</small> ha realizado la siguiente pregunta:<br><br>
                        </p>
                        <h3 align="center" style="color: #e67e22; margin: 0 0 7px;">'.$Pregunta.'</h3>

                      <br>
                        <div style="text-align: center">
                         <img src="https://posgradosfcn-uaq.com/aspirantes/img/support/soporte-tecnico.jpg" width="200px" height="200px">
                        </div> 
                        <br clear="all">
                        <div style="width: 100%; text-align: center">
                            <a style="text-decoration: none; border-radius: 5px; padding: 11px 23px; color: white; background-color: #3498db" href="'.$link.'">Dar soporte</a>
                        </div>
                        <p style="color: #b3b3b3; font-size: 12px; text-align: center;margin: 30px 0 0">UAQ, Facultad de Ciencias Naturales.</p>
                    </div>
                </td>
            </tr>
        </table>
    </body>
    </html>';

    // Para enviar un correo HTML, debe establecerse la cabecera Content-type
    $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

    // Cabeceras adicionales
    $cabeceras .= 'To: Desarrolladores <'.$Correo.'>'."\r\n";
    $cabeceras .= 'From: Soporte-Plataforma de aspirantes-FCN <uaq_fcn@posgradosfcn-uaq.com>' . "\r\n";


    // Enviarlo
    $enviado = mail($para, $titulo, $mensaje, $cabeceras);
    if($enviado){
       echo "Hecho";
    }else{
       echo "error";
    }
}


function enviarCorreoRespuestaDesarrollador($Usuario,$Correo,$Respuesta,$Pregunta,$Responde){  
    ////////////////////////////////Envio de Correo//////////////////////////////////////
    // Varios destinatarios
    $para  = $Correo;//."josemgo.9.11@gmail.com,wilfred.ironman@gmail.com";

    // titulo
    $titulo = 'Soporte-FCN';
    
    $TipoUsuario=$_SESSION['tipo'];
    $m=$_SESSION['pk'];
    
    //Link de soporte desde el boton
$link="https://posgradosfcn-uaq.com/aspirantes/php/support-validate.php?f=redirectUser&t={$TipoUsuario}&u={$Usuario}&cl=Ch3m4yW1lfr13d&p={$Pregunta}";
        
    $mensajee="Hola {$Usuario}!: {$Responde} ha contestado a tu pregunta: {$Pregunta}, respuesta: {$Respuesta}, link: {$link}, se evio el correo a : {$para}";
    
    // mensaje
    $mensaje = '<html>
    <head>
        <title>Proceso de admisi&oacute;n</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>

    <body>
        <table style="width: 600px; padding: 10px; margin:0 auto; border-collapse: collapse;">
            <tr>
                <td style="text-align: center">
                    <img src="http://aspirantes.posgradosfcn-uaq.com/img/logos/icouaq.png" width="100px" height="100px">
                </td>
                <td >
                    <h2  style="text-align: center; color: #3498db; margin: 0 0 12px; font-size: 30px; font-family: inherit">Soporte</h2>
                </td>
                <td style="text-align: center">
                    <img src="http://aspirantes.posgradosfcn-uaq.com/img/logos/icofc.png" width="100px" height="100px">
                </td>
            </tr>
        </table>
        <br>
        <table style="width: 600px; margin:0 auto; border-collapse: collapse;" >

            <tr>
                <td style="background-color: #ecf0f1">
                    <div style="color: #34495e; margin: 4% 10% 2%; text-align: justify;font-family: sans-serif">
                        <p style="margin: 2px; font-size: 15px">
                            ¡Hola '.$Usuario.'!<br><br>
                            <strong>'.$Responde.'</strong> ha contestado a tu pregunta:<br><br> 
                        <h3 align="center" style="color: #e67e22; margin: 0 0 7px;">'.$Pregunta.'</h3> <br><br>
                            <strong>'.$Responde.': </strong> '.$Respuesta.'
                    </div>
                        </p>

                      <br>
                        <div style="text-align: center">
                         <img src="/fcn/aspirantes/img/support/soporte-tecnico.png" width="200px" height="200px">
                        </div> 
                        <br clear="all">
                        <div style="width: 100%; text-align: center">
                            <a style="text-decoration: none; border-radius: 5px; padding: 11px 23px; color: white; background-color: #3498db" href="'.$link.'">Ver en la plataforma</a>
                        </div>
                        <p style="color: #b3b3b3; font-size: 12px; text-align: center;margin: 30px 0 0">UAQ, Facultad de Ciencias Naturales.</p>
                
                </td>
            </tr>
        </table>
    </body>
    </html>';

    // Para enviar un correo HTML, debe establecerse la cabecera Content-type
    $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

    // Cabeceras adicionales
    $cabeceras .= 'To: '.$Usuario.' <'.$Correo.'>'."\r\n";
    $cabeceras .= 'From: Soporte-Plataforma de aspirantes-FCN <uaq_fcn@posgradosfcn-uaq.com>' . "\r\n";


    // Enviarlo
    $enviado = mail($para, $titulo, $mensaje, $cabeceras);
    if($enviado){
       echo "Hecho";
    }else{
       echo "error";
    }
    
    //echo $mensajee;
}

/**********************************************
Valida que se haya iniciado sesion
devuelve el tipo de usuario y correo de quien ingresa
al modulo de soporte
***********************************************/
function validarUsuario(){
    //Incluir archivo de conexión
        include("conect.php");
        
        //Iniciar la sesión
        session_start();
        
    if(isset($_SESSION['tipo']) && isset($_SESSION['nombre'])){
        $datos= array();
        
        
        if($_SESSION['tipo']=="aspirante"){
            //Para los aspirantes
            $SQL="Select correo from alumnos where pk_matricula='".$_SESSION['pk']."';";
            $Correo=resultQuery($SQL)[0][0];
            $datos[0]=$_SESSION['tipo'];
            $datos[1]=$Correo;
        }else if($_SESSION['tipo']=="Developer"){
            //Para los desarrolladores
            $datos[0]=$_SESSION['tipo'];
            $datos[1]="josemgo.9.11@gmail.com,wilfred.ironman@gmail.com";
        }else{
            //Para los administrativos
            $datos[0]=$_SESSION['tipo'];
            $datos[1]=$_SESSION['pk'];
        }
        
        echo json_encode($datos);
    }else echo "salir";
}



?>
