<?php
    $funcion=$_GET['funcion'];

switch($funcion){
    case 'aspirantes':
        consultarAspirantes();
        break;
    case 'sexo':
        consultarSexo();
        break;
    case 'universidades':
        consultarUniversidades();
        break;
    case 'nacionalidad':
        consultarNacionalidad();
        break;
    case 'resultados':
        consultarResultados();
        break;
    case 'modalidad':
        consultarModalidad();
        break;
}


function consultarAspirantes(){
    header( 'Content-Type: application/json' );
    include("conect.php");
    
    $SQL="Select estado,fk_programa from alumnos";
    $Datos=executeQuery($SQL);
    
    $DatosArray = array(); //creamos un array

        foreach($Datos as $row)
        {
            $DatosArray[] = $row;
        }
            echo json_encode($DatosArray);
}

function consultarSexo(){
    header( 'Content-Type: application/json' );
    include("conect.php");
    
    session_start();
    $periodo=$_GET['periodo'];
    $programa=$_SESSION['fk'];
    
    $SQL="(select sexo,count(*) as cantidad from alumnos where alumnos.sexo = 'Masculino' and fk_programa='".$programa."' and fk_periodo='".$periodo."') UNION ALL (select sexo,count(*) as cantidad from alumnos where alumnos.sexo = 'Femenino' and fk_programa='".$programa."' and fk_periodo='".$periodo."');";
    $Datos=executeQuery($SQL);
    
    $DatosArray = array(); //creamos un array

        foreach($Datos as $row)
        {
            $DatosArray[] = $row;
        }
    
        echo json_encode($DatosArray);
}

function consultarUniversidades(){
    header( 'Content-Type: application/json' );
    include("conect.php");
    
    session_start();
    $periodo=$_GET['periodo'];
    $programa=$_SESSION['fk'];
    
    if($programa == 8){
        $SQL="SELECT DISTINCT alumnos.institucion_carrera_maestria as Universidad,count(*) as Aspirantes FROM alumnos where fk_programa='".$programa."' and fk_periodo='".$periodo."' GROUP BY alumnos.institucion_carrera_maestria";
    }else{
        $SQL="SELECT DISTINCT alumnos.institucion_carrera_licenciatura as Universidad,count(*) as Aspirantes FROM alumnos where fk_programa='".$programa."' and fk_periodo='".$periodo."' GROUP BY alumnos.institucion_carrera_licenciatura";
    }
    
    $Datos=executeQuery($SQL);
    
    $DatosArray = array(); //creamos un array

        foreach($Datos as $row)
        {
            $DatosArray[] = $row;
        }
    
        echo json_encode($DatosArray);
}

function consultarNacionalidad(){
    header( 'Content-Type: application/json' );
    include("conect.php");
    
    session_start();
    $periodo=$_GET['periodo'];
    $programa=$_SESSION['fk'];
    
    $SQL="(select pais as Pais,count(*) as Aspirantes from alumnos WHERE alumnos.pais='México' and fk_programa='".$programa."' and fk_periodo='".$periodo."') UNION all (select pais,count(*) as Extranjeros from alumnos WHERE alumnos.pais !='México' and fk_programa='".$programa."' and fk_periodo='".$periodo."')";
    $Datos=executeQuery($SQL);
    
    $DatosArray = array(); //creamos un array

        foreach($Datos as $row)
        {
            $DatosArray[] = $row;
        }
    
        echo json_encode($DatosArray);
}

function consultarResultados(){
    header( 'Content-Type: application/json' );
    include("conect.php");
    
    session_start();
    $periodo=$_GET['periodo'];
    $programa=$_SESSION['fk'];
    
    $SQL="(select estado as Estado,count(*) as Aspirantes from alumnos WHERE alumnos.estado='Aceptado' and fk_programa='".$programa."' and fk_periodo='".$periodo."') UNION all (select estado as Estado,count(*) as Aspirantes from alumnos WHERE alumnos.estado!='Aceptado' and fk_programa='".$programa."' and fk_periodo='".$periodo."')";
    $Datos=resultQuery($SQL);
    
    $Aceptados["Estado"]="Aceptados";
    $Aceptados["Aspirantes"]=$Datos[0]["Aspirantes"];
    
    $NoAceptados["Estado"]="No aceptados";
    $NoAceptados["Aspirantes"]=$Datos[1]["Aspirantes"];
    
    $Resultados= array();
    $Resultados[]=$Aceptados;
    $Resultados[]=$NoAceptados; 

    
        echo json_encode($Resultados);
}

function consultarModalidad(){
    header( 'Content-Type: application/json' );
    include("conect.php");
    
    session_start();
    $periodo=$_GET['periodo'];
    $programa=$_SESSION['fk'];
    
    $SQL="select DISTINCT modalidad as Modalidad,COUNT(*) as Aspirantes from alumnos where fk_programa='".$programa."' and fk_periodo='".$periodo."'";
    $Datos=executeQuery($SQL);
    
    $DatosArray = array(); //creamos un array

        foreach($Datos as $row)
        {
            $DatosArray[] = $row;
        }
    
        echo json_encode($DatosArray);
}
?>
