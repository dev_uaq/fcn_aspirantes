<?php
//Conexión
include("conect.php");

//Variables POST
$Correo = addslashes(htmlspecialchars($_POST["correoRecuperacion"]));

//Verificar si existe el usuario
$sql="(select count(pk_correo) from usuarios where pk_correo='".$Correo."')
union all
(select count(correo) from alumnos where correo='".$Correo."')";

//Ejecutar consulta
$resultado=resultQuery($sql);
$usuario=$resultado[0][0];
$alumno=$resultado[1][0];

if($usuario>=1){
    recuperarContraseña("usuarios",$Correo);
}else if($alumno>=1){
    recuperarContraseña("alumnos",$Correo);
}else{
    echo "No registrado";
}

function recuperarContraseña($tipo,$Correo){
    //consulta de la contraseña
    switch($tipo){
        case 'usuarios':
            $sql="select password,nombre,apellido_p,apellido_m from usuarios where pk_correo='".$Correo."';";
            break;
        case 'alumnos':
            $sql="select password,nombre,apellido_p,apellido_m from alumnos where correo='".$Correo."';";
            break;
    }
    
    //obtencion de datos del usuario
    $resultado=resultQuery($sql);
    $contraseña=$resultado[0][0];
    $nombre=$resultado[0][1];
    $apellidoP=$resultado[0][2];
    $apellidoM=$resultado[0][3];
    
//enviar correo con la contraseña
    // Varios destinatarios
    $para  = $Correo;// . ', '; // atenci&oacute;n a la coma
    //$para .= 'wilfred.ironman@gmail.com';

    // titulo
    $titulo = 'Recuperación de contraseña FCN';

    // mensaje
    $mensaje = '<html>

<head>
    <title>Proceso de admisión</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
    <table style="width: 600px; padding: 10px; margin:0 auto; border-collapse: collapse;">
        <tr>
            <td style="text-align: center">
                <img src="http://aspirantes.posgradosfcn-uaq.com/img/logos/icouaq.png" width="100px" height="100px">
            </td>
            <td>
                <h2 style="text-align: center; color: #3498db; margin: 0 0 12px; font-size: 30px; font-family: inherit">Proceso de admisión.</h2>
                <h4 style="text-align: center; color: #3498db; margin: 0 0 12px; font-size: 20px; font-family: inherit">Recuperación de contraseña</h4>
            </td>
            <td style="text-align: center">
                <img src="http://aspirantes.posgradosfcn-uaq.com/img/logos/icofc.png" width="100px" height="100px">
            </td>
        </tr>
    </table>
    <br>
    <table style="width: 600px; margin:0 auto; border-collapse: collapse;">

        <tr>
            <td style="background-color: #ecf0f1">
                <div style="color: #34495e; margin: 4% 10% 2%; text-align: justify;font-family: sans-serif">
                    <h3 style="color: #e67e22; margin: 0 0 7px">Hola '.$nombre.' '.$apellidoP.' '.$apellidoM.'</h3>
                    <p style="margin: 2px; font-size: 15px">
                        Recientemente ha solicitado la recuperación de su contraseña.<br>
                    </p>
                    <hr>
                    <div style="font-size: 12px;  margin: 5px 0">
                        Contraseña: '.$contraseña.'<br>
                    </div>
                    <hr>
                    <p style="margin: 2px; font-size: 15px">
                        Le sugerimos que cambie su contraseña por otra que le sea fácil de recordar evidentemente sin ignorar la seguridad de la misma.<br>
                    </p>
                    <br clear="all">
                    <div style="width: 100%; text-align: center">
                        <a style="text-decoration: none; border-radius: 5px; padding: 11px 23px; color: white; background-color: #3498db" href="http://aspirantes.posgradosfcn-uaq.com/login.php">Ingresar</a>
                    </div>
                    <p style="color: #b3b3b3; font-size: 12px; text-align: center;margin: 30px 0 0">UAQ, Facultad de Ciencias Naturales.</p>
                </div>
            </td>
        </tr>
    </table>
</body>

</html>';

    // Para enviar un correo HTML, debe establecerse la cabecera Content-type
    $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

    // Cabeceras adicionales
    $cabeceras .= 'To:'.$Nombre.'<'.$Correo.'>'."\r\n";
    $cabeceras .= 'From: Facultad de Ciencias Naturales <fcn@posgradosfcn-uaq.com>' . "\r\n";
    
    //verificar que se envio el correo
    $enviado = mail($para, $titulo, $mensaje, $cabeceras);
    if($enviado){
        echo "Hecho";
    }else{
        echo "Error";
    }
}

?>