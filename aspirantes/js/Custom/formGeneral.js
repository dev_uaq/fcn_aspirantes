        $(document).ready(function() {
            var $validator = $("#commentForm").validate();

            $('#rootwizard').bootstrapWizard({

                'tabClass': 'nav nav-tabs',
                'onNext': function(tab, navigation, index) {
                    var $valid = $("#commentForm").valid();

                    if (!$valid) {
                        $validator.focusInvalid();
                        var $valid = $("#barProgreso").removeClass("bg-success").addClass("bg-danger");
                        return false;
                    } else {
                        var $valid = $("#barProgreso").removeClass("bg-danger").addClass("bg-success");
                    }
                },
                'onTabClick': function(tab, navigation, index) {
                    var $valid = $("#commentForm").valid();

                    if (!$valid) {
                        $validator.focusInvalid();
                        var $valid = $("#barProgreso").removeClass("bg-success").addClass("bg-danger");
                        return false;
                    } else {
                        var $valid = $("#barProgreso").removeClass("bg-danger").addClass("bg-success");
                    }
                },
                'onTabShow': function(tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index + 1;
                    var $percent = ($current / $total) * 100;
                    $('#rootwizard .progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
            });
        });