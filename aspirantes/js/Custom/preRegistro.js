        $(document).ready(function () {
            document.getElementById('cargando').style. display = 'none';
            
            var $validator = $("#formularioPreRegistro").validate();

            $('#rootwizard').bootstrapWizard({

                'tabClass': 'nav nav-tabs',
                'onNext': function (tab, navigation, index) {
                    var $valid = $("#formularioPreRegistro").valid();

                    if (!$valid) {
                        $validator.focusInvalid();
                        var $valid = $("#barProgreso").removeClass("bg-success").addClass("bg-danger");
                        return false;
                    } else {
                        var $valid = $("#barProgreso").removeClass("bg-danger").addClass("bg-success");
                    }
                },
                'onTabClick': function (tab, navigation, index) {
                    var $valid = $("#formularioPreRegistro").valid();

                    if (!$valid) {
                        $validator.focusInvalid();
                        var $valid = $("#barProgreso").removeClass("bg-success").addClass("bg-danger");
                        return false;
                    } else {
                        var $valid = $("#barProgreso").removeClass("bg-danger").addClass("bg-success");
                    }
                },
                'onTabShow': function (tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index + 1;
                    var $percent = ($current / $total) * 100;
                    $('#rootwizard .progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#rootwizard .finish').click(function () {
                var $valid = $("#formularioPreRegistro").valid();
                    if ($valid) {
                        $validator.focusInvalid();
                        var url = "php/preRegistro.php"; // El script a dónde se realizará la petición.
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: $("#formularioPreRegistro").serialize(), // Adjuntar los campos del formulario enviado.
                        success: function (data) {
                            $("#formularioPreRegistro").html(data);
                        }
                    });

                    
                    } else {
                        var $valid = $("#barProgreso").removeClass("bg-success").addClass("bg-danger");
                        return false; // Evitar ejecutar el submit del formulario.
                    }       
            });
        });
