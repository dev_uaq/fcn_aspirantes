/*---------------------------------------
       DEEP MINT WAS HERE  <(°_°)> 
---------------------------------------*/
//VARIABLES GLOBALES

var userLevel = "null";
var userData = [];
var periodoActivo = false;
var userLevel = "null";
var userData = [];

/*---------------------------------------
            VALIDAR SESIÓN 
---------------------------------------*/
function Kakunin() { /* Kakunin == Validar en japonés --- función que valida la sesión y hace que el resto de la pagina continue*/
    var url = "php/Consultas/sesion-validate.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: null,
        success: function (data) { /* Data contendra el valor true encaso de que se valide la sesión o nanay en caso contrario a travéz del archivo php validar-sesion */
            if (data.usuario_success) {//Sí hay sesión ejecuta las demás funciones
                
                if (data.level == 'Jefe') {
                    location.href = "usuarios.html";
                }else{
                    userData = data.user_data;
                    $("#nombre").val(data.user_data[0][0]);
                    $("#apellido-p").val(data.user_data[0][1]);
                    $("#apellido-m").val(data.user_data[0][2]);
                    $("#mail").val(data.user_data[0][3]);
                    $("#pass").val(data.user_data[0][4]);
                    $(".save-user").attr("name",data.user_data[0][3]);
                  Materialize.updateTextFields();
                    /*----------------------------------
                      Consulta las ponderaciones para
                      mostrarlas en la interfaz
                    ----------------------------------*/
                    consultarPrograma();
                    consultarPonderaciones();
                    inizializzare();
                }                  
    
            } else { //Si no hay sesión nos vamos al login
                //De regreso al login
                location.href = "login.php";
            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error nos vamos al login
            //De regreso al login
            location.href = "login.php";
        }

    });
}
Kakunin(); //Ejecutamos la función Kakunin


/*----------------------------------
   FUNCION PARA MENU DE NAVEGACIÓN
----------------------------------*/
function goToPage(link){
    location.href = link;
}

/*---------------------------------------
            CERRAR SESIÓN 
---------------------------------------*/
function sesionDie() { /*  --- función que aniquila la sesión activa*/
    kuonyesha(2);
    var url = "php/Consultas/sesion-die.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: null,
        success: function (data) {
            if (data.success) {
                //De regreso al login
                location.href = "login.php";
            } else {
                Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
                kuonyesha(3);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error nos vamos al login
            //De regreso al login
            Materialize.toast("¡Error!, vuelva a intentarlo", 3000);
            kuonyesha(3);
        }

    });
}


/*------------------------------------
   FUNCIÓN PARA EDITAR USUARIO
-------------------------------------*/
function editUserStatic() {
    $(document.getElementById('edit')).addClass('hide');
    $(document.getElementById('cancel')).removeClass('hide');
    $(document.getElementById('save')).removeClass("scale-out");
    $(document.getElementById('save')).addClass("scale-in");

    $(document.getElementById('nombre')).removeAttr("disabled");
    $(document.getElementById('apellido-p')).removeAttr("disabled");
    $(document.getElementById('apellido-m')).removeAttr("disabled");
    $(document.getElementById('mail')).removeAttr("disabled");
    $(document.getElementById('pass')).removeAttr("disabled");
}
/*-----------------------------------------------
   FUNCIÓN PARA CANCELAR ACCIÓN EDITAR USUARIO
-----------------------------------------------*/
function cancelUserStatic() {
    $(document.getElementById('cancel')).addClass('hide');
    $(document.getElementById('edit')).removeClass('hide');
    $(document.getElementById('save')).removeClass("scale-in");
    $(document.getElementById('save')).addClass("scale-out");
    $(document.getElementById('save')).addClass("scale-out");
    var index = parseInt($(document.getElementById('cancel')).attr("value"));
    $(document.getElementById('nombre')).val(userData[0][0]);
    $(document.getElementById('apellido-p')).val(userData[0][1]);
    $(document.getElementById('apellido-m')).val(userData[0][2]);
    $(document.getElementById('mail')).val(userData[0][3]);
    $(document.getElementById('pass')).val(userData[0][4]);
    $(document.getElementById('nombre')).attr("disabled", "disabled");
    $(document.getElementById('apellido-p')).attr("disabled", "disabled");
    $(document.getElementById('apellido-m')).attr("disabled", "disabled");
    $(document.getElementById('mail')).attr("disabled", "disabled");
    $(document.getElementById('pass')).attr("disabled", "disabled");
    Materialize.updateTextFields();

}
/*-------------------------------------------
   FUNCIÓN PARA GUARDAR CAMBIOS DE USUARIO
--------------------------------------------*/
function saveUserStatic(pkUsuario) {
    var check = [];
    check = checkValidateStatic();
    if (check[0] == 1) {
        Materialize.toast(check[1], 3000);
    } else {
        kuonyesha(2);
        var userinfo = [];
        userinfo[0] = $(document.getElementById('nombre')).val();
        userinfo[1] = $(document.getElementById('apellido-p')).val();
        userinfo[2] = $(document.getElementById('apellido-m')).val();
        userinfo[3] = $(document.getElementById('mail')).val();
        userinfo[4] = $(document.getElementById('pass')).val();
        var url = "php/Consultas/usuarios.php"; // El script a dónde se realizará la petición.
        var parametro = {
            "accion": "update_usuario",
            "pk_usuario": pkUsuario,
            "user-data": userinfo
        };
        $.ajax({
            type: "POST",
            url: url,
            data: parametro,
            dataType: "JSON",
            success: function (data) {
                if (data.success) {
                    Materialize.toast("¡Exito!, los cambios han sido efectuados", 3000,'',function(){ location.reload();});
                   
                } else {
                    Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
                    pkDeleteUser = "null";
                    kuonyesha(3);
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                Materialize.toast("¡Error!, vuelva a intentarlo", 3000);
                pkDeleteUser = "null";
                kuonyesha(3);
            }

        });
    }
}

/*--------------------------------------
   VALIDAR CAMPOS DE USUARIO
--------------------------------------*/
function checkValidateStatic() {
    var regex = "/[w-.]{2,}@([w-]{2,}.)*([w-]{2,}.)[w-]{2,4}/";
    var meta = [];
    if ($(document.getElementById('nombre')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo nombre debe ser llenado";
    } else if ($(document.getElementById('apellido-p')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo apellido paterno debe ser llenado";
    } else if ($(document.getElementById('apellido-m')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo apellido materno debe ser llenado";
    } else if ($(document.getElementById('mail')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo correo electrónico debe ser llenado";
    } else if ($(document.getElementById('mail')).val().indexOf('@', 0) == -1 || $(document.getElementById('mail')).val().indexOf('.', 0) == -1) {
        meta[0] = 1;
        meta[1] = "!Alerta¡, introduzca un correo válido";

    } else if ($(document.getElementById('pass')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo contraseña debe ser llenado";
    } else {
        meta[0] = 0;
        meta[1] = "All right!";

    }
    return meta;
}
/*-----------------------------------
   INICIALIZAR COMPONENTES
-----------------------------------*/
function inizializzare() {
    showDialog();

    /* inizializzare == inicializar en Italiano, función que  inicializa ciertos  componentes de algunos frameworks como
       MATERIALIZE Y WOW ANIMATION*/
    $(".añadir-ponderacion").hide(50);


    /*---------------------------------
      INIT DATEPICKER MATERIALIZE
  ----------------------------------*/

    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 10, // Creates a dropdown of 15 years to control year,
        today: 'Hoy',
        clear: 'Limpiar',
        close: 'Ok',
        closeOnSelect: false // Close upon selecting a date,
    });

    

    /* ---------------------------
      INIT SELECT MATERIALIZE
    ----------------------------*/
    initializzare_select();

    /* ---------------------------
      INIT DROPDOWN MATERIALIZE
    ----------------------------*/
    initializzare_dropdown();

    


    /*----------------------------------
      INIT PUSH SPIN MATERIALIZE
    ----------------------------------*/
    initializzare_pushpin();

    //Inicializa Modal
    $('#modal1').modal();
    
        $(document).ready(function () {
        $('#modal-user-data').modal();
    });
// Initialize collapse button
  $(".button-collapse").sideNav();


//    verificarPeriodo();
}

/*----------------------------------
      INICIAR SELECT MATERIALIZE
----------------------------------*/
function initializzare_select() {
    $(document).ready(function () {
        $('select').material_select();
    });
}

/*----------------------------------
      INICIAR DROPDOWN MATERIALIZE
----------------------------------*/
function initializzare_dropdown() {
    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false, // Does not change width of dropdown to that of the activator
        hover: true, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: false, // Displays dropdown below the button
        alignment: 'left', // Displays dropdown with edge aligned to the left of button
        stopPropagation: false // Stops event propagation
    });
}


/*----------------------------------
      INICIALIZAR PUSHPIN
----------------------------------*/
function initializzare_pushpin() {
    $(document).ready(function () {
        if (screen.width > 992) {
            $('.pin-top').pushpin({
                top: 527,
                offset: 0
            });
        } else if (screen.width > 600) {
            $('.pin-top').pushpin({
                top: 510,
                offset: 0
            });
        } else {
            $('.pin-top').pushpin({
                top: 920,
                offset: 0
            });
        }
    });
}

/*--------------------------------------
    EVITAR ONCLICK POR DEFAULT EN LINK
 -------------------------------------*/
$(document).on("click", "a", function (e) {
        e.preventDefault();
});

/*--------------------------------------
TAB HECHO EN CHINA
---------------------------------------*/
$(document).ready(function () {
    $(".li-ponderar").click(function () {
        $(".añadir-ponderacion").hide(10);
        $(".ponderar").show("slow");
        $(".li-añadir-ponderacion").removeClass("k", 1000, "easeInBack");
        $(".li-ponderar").addClass("k", 1000, "easeInBack");
    });
    $(".li-añadir-ponderacion").click(function () {
        if (periodoActivo) {
            Materialize.toast('Hay un periodo activo', 4000, 'rounded');
        } else {
            $(".ponderar").hide(10);
            $(".añadir-ponderacion").show("slow");
            $(".li-añadir-ponderacion").addClass("k", 1000, "easeInBack");
            $(".li-ponderar").removeClass("k", 1000, "easeInBack");
        }
    });
});

/*----------------------------------
      MOSTRAR U OCULTAR CONTENIDO
----------------------------------*/
function kuonyesha(almostflag) { /* kuonyesha == mostrar en Suajili, función que oculta o muestra el div de carga que tiene un gif súper cool,también activa o des activa el scroll*/
    if (almostflag == 1) {
        $(document).ready(function () {
            $('.preloader').fadeOut('fast');
            $('body').css({
                'overflow': 'visible'
            });
        });
    } else if (almostflag == 2) {
        $(document).ready(function () {
            $("#wait").addClass("wait");
            $('.wait').removeClass("hide");
            $('.wait').fadeIn('fast');
            $('body').css({
                'overflow': 'hidden'
            });
        });
    } else if (almostflag == 3) {
        $(document).ready(function () {
            $("#wait").removeClass("wait");
            $('.wait').fadeOut('fast');
            $('body').css({
                'overflow': 'visible'
            });
        });
    } else if (almostflag == 4) {
        $(document).ready(function () {
            $('.overlay').fadeIn('fast');
            $('body').css({
                'overflow': 'hidden'
            });
        });
    } else if (almostflag == 5) {
        $(document).ready(function () {
            $('.overlay').fadeOut('fast');
            $('body').css({
                'overflow': 'visible'
            });
        });
    }
}
/*----------------------------------
      AÑADIR NUEVA PONDERACION
----------------------------------*/
$("#añadir-ponderacion").submit(function () {
    showDialog();
    var url = "./php/Consultas/ponderaciones.php?funcion=añadirPonderacion"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: $("#añadir-ponderacion").serialize(), // Adjuntar los campos del formulario enviado.
        success: function (data) {
            document.getElementById("añadir-ponderacion").reset();
            consultarPonderaciones();
            Materialize.toast('Ponderación agregada correctamente', 4000, 'rounded');
            hideDialog();
        }
    });

    return false;
});

/*----------------------------------
      Consultar Ponderaciones
----------------------------------*/
function consultarPonderaciones() {
    showDialog();
    console.log("consultando ponderaciones");
    var url = "./php/Consultas/ponderaciones.php?funcion=consultarPonderaciones"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        //            data: $("#añadir-ponderacion").serialize(), // Adjuntar los campos del formulario enviado.
        success: function (data) {
            if (data != "salir") {
                //                console.log(data);
                showDialog();
                var ponderaciones = JSON.parse(data);
                $("#ponderaciones").html("");
                $.each(ponderaciones, function (index) {
                    var nombre = ponderaciones[index].ponderacion;
                    var valor = ponderaciones[index].valor;
                    var pk = ponderaciones[index].pk_ponderacion;
                    if (nombre != undefined) {
                        $("#ponderaciones").append("<a href='#' class='collection-item' onClick=editar(" + pk + ");><span class='new badge' data-badge-caption='Presione para editar'></span><div class='chip'>" + valor + " %</div> " + nombre + "</a>");
                    }
                });
                valorDisponible();
            } else {
                location.href = "./php/Consultas/sesion-die.php";
            }
        }
    });
}

/*----------------------------------
      Consultar Datos del programa
----------------------------------*/
function consultarPrograma() {
    showDialog();
    var url = "./php/Consultas/ponderaciones.php?funcion=consultarPrograma"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {
            if (data != "salir") {
                var datos = JSON.parse(data);
                $("#nombreCarrera").html(datos['programa']);
                $("#usuario_name").append(datos['nombre']);
                valorDisponible(); 
            } else {
                location.href = "./php/Consultas/sesion-die.php";
            }
        }
    });
}

/*----------------------------------
      Llena los campos del modal
----------------------------------*/
function editar(pk) {
    if (periodoActivo) {
        Materialize.toast('Hay un periodo activo', 4000, 'rounded');
    } else {
        showDialog();
        var url = "./php/Consultas/ponderaciones.php?funcion=llenarModal&id=" + pk; // El script a dónde se realizará la petición.
        $.ajax({
            type: "POST",
            url: url,
            success: function (data) {
                if (data != "salir") {
                    var datos = JSON.parse(data);
                    $("#editarValorPonderacion").attr("value", datos[0].valor);
                    $("#editarNombrePonderacion").attr("value", datos[0].ponderacion);
                    var disponibleEditar = parseInt(datos[0].valor) + parseInt(auxiliarDisponible);
                    $("#editarValorPonderacion").attr("max", disponibleEditar);
                    $("#idPonderacion").attr("value", pk);
                    $("#NombrePonderacion").html(datos[0].ponderacion);
                    $('#modal1').modal('open');
                    hideDialog();
                } else {
                    location.href = "./php/Consultas/sesion-die.php";
                }
            }
        });
    }
};

/*----------------------------------
      Editar Ponderacion
----------------------------------*/
$("#editarPonderacion").submit(function () {
    showDialog();
    var url = "./php/Consultas/ponderaciones.php?funcion=editarPonderacion"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: $("#editarPonderacion").serialize(), // Adjuntar los campos del formulario enviado.
        success: function (data) {
            console.log(data);
            if (data != "salir" && data == "hecho") {
                Materialize.toast('Ponderación editada correctamente', 4000, 'rounded');
                document.getElementById("editarPonderacion").reset();
                consultarPonderaciones();
                $('#modal1').modal('close');
            } else if (data == "salir") {
                location.href = "./php/Consultas/sesion-die.php";
            }
        }

    });
    return false;
});

/*----------------------------------
    Consultar porcentaje disponible
----------------------------------*/
var auxiliarDisponible = 0;

function valorDisponible() {
    var url = "./php/Consultas/ponderaciones.php?funcion=rangoPonderacion"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {
            if (data != "salir") {
                auxiliarDisponible = data;
                $("#valorPonderacion").attr("max", data);
                hideDialog();
            } else {
                location.href = "./php/Consultas/sesion-die.php";
            }
        }
    });
}

/*----------------------------------
    Eliminar ponderacion
----------------------------------*/
$("#btnEliminar").click(function () {
    showDialog();
    var id = document.getElementById("idPonderacion").value;

    var url = "./php/Consultas/ponderaciones.php?funcion=eliminarPonderacion&id=" + id; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {
            if (data != "salir" && data == "hecho") {
                Materialize.toast('Ponderación eliminada correctamente', 4000, 'rounded');
                $('#modal1').modal('close');
                consultarPonderaciones();
            } else if (data == "salir") {
                location.href = "./php/Consultas/sesion-die.php";
            }
        }
    });
});

/*----------------------------------
      Especificaciones del modal
----------------------------------*/
$('.modal').modal({
    dismissible: true, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '4%', // Starting top style attribute
    endingTop: '10%', // Ending top style attribute
    ready: function (modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
        //        alert("Ready");
        //        console.log(modal, trigger);
    },
    complete: function () {
        console.log("formulario limpiado");
        document.getElementById("editarPonderacion").reset();

    } // Callback for Modal close
});

/*----------------------------------
      MOSTRAR DIALOGO
----------------------------------*/
function showDialog() {
    $("#wait").addClass("wait");
    //    console.log("show");
}
/*----------------------------------
      OCULTAR DIALOGO
----------------------------------*/
function hideDialog() {
    $("#wait").removeClass("wait");
    //    console.log("hide");
}

/*----------------------------------
 VERIFICAR SI HAY PERIODOS ACTIVOS
----------------------------------*/
function verificarPeriodo() {
    showDialog();

    var url = "./php/Consultas/ponderaciones.php?funcion=verificarPeriodoActual"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function (data) {
            hideDialog();
            if (data != "salir") {
                if (data == 1) {
                    periodoActivo = true;
                    $("#infoActivo").removeClass("hide");
                } else if (periodoActivo == 0) {
                    periodoActivo = false;
                }
            } else if (data == "salir") {
                location.href = "./php/Consultas/sesion-die.php";
            }
        }
    });
}
