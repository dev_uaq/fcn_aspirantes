/*---------------------------------------
       DEEP MINT WAS HERE  <(°_°)> 
---------------------------------------*/
//VARIABLES GLOBALES
var filtro = 1;
var acdcFlag = 0;
var acdc = "ac";
var limit = 10;
var id_asp = "";
var userLevel = "null";
var userData = [];
/*---------------------------------------
            VALIDAR SESIÓN 
---------------------------------------*/
function Kakunin() { /* Kakunin == Validar en japonés --- función que valida la sesión y hace que el resto de la pagina continue*/
    var url = "php/Consultas/sesion-validate.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: null,
        success: function (data) { /* Data contendra el valor true encaso de que se valide la sesión o nanay en caso contrario a travéz del archivo php validar-sesion */
            if (data.usuario_success) { //Sí hay sesión ejecuta las demás funciones

                if (data.level == 'Jefe') {
                    $("#ponderacion-page-mobile").addClass("hide");
                    $("#ponderacion-page").addClass("hide");
                    $("#periodo-page-mobile").addClass("hide");
                    $("#periodo-page").addClass("hide");
                }
                userData = data.user_data;
                $("#nombre").val(data.user_data[0][0]);
                $("#apellido-p").val(data.user_data[0][1]);
                $("#apellido-m").val(data.user_data[0][2]);
                $("#mail").val(data.user_data[0][3]);
                $("#pass").val(data.user_data[0][4]);
                $(".save-user").attr("name", data.user_data[0][3]);
                Materialize.updateTextFields();
                fatarat();
                inizializzare();
            } else { //Si no hay sesión nos vamos al login
                //De regreso al login
                location.href = "login.php";
            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error nos vamos al login
            //De regreso al login
            location.href = "login.php";
        }

    });
}
Kakunin(); //Ejecutamos la función Kakunin


/*----------------------------------
   FUNCION PARA MENU DE NAVEGACIÓN
----------------------------------*/
function goToPage(link) {
    location.href = link;
}

/*---------------------------------------
            CERRAR SESIÓN 
---------------------------------------*/
function sesionDie() { /*  --- función que aniquila la sesión activa*/
    kuonyesha(2);
    var url = "php/Consultas/sesion-die.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: null,
        success: function (data) {
            if (data.success) {
                //De regreso al login
                location.href = "login.php";
            } else {
                Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
                kuonyesha(3);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error nos vamos al login
            //De regreso al login
            Materialize.toast("¡Error!, vuelva a intentarlo", 3000);
            kuonyesha(3);
        }

    });
}


/*------------------------------------
   FUNCIÓN PARA EDITAR USUARIO
-------------------------------------*/
function editUserStatic() {
    $(document.getElementById('edit')).addClass('hide');
    $(document.getElementById('cancel')).removeClass('hide');
    $(document.getElementById('save')).removeClass("scale-out");
    $(document.getElementById('save')).addClass("scale-in");

    $(document.getElementById('nombre')).removeAttr("disabled");
    $(document.getElementById('apellido-p')).removeAttr("disabled");
    $(document.getElementById('apellido-m')).removeAttr("disabled");
    $(document.getElementById('mail')).removeAttr("disabled");
    $(document.getElementById('pass')).removeAttr("disabled");
}
/*-----------------------------------------------
   FUNCIÓN PARA CANCELAR ACCIÓN EDITAR USUARIO
-----------------------------------------------*/
function cancelUserStatic() {
    $(document.getElementById('cancel')).addClass('hide');
    $(document.getElementById('edit')).removeClass('hide');
    $(document.getElementById('save')).removeClass("scale-in");
    $(document.getElementById('save')).addClass("scale-out");
    $(document.getElementById('save')).addClass("scale-out");
    var index = parseInt($(document.getElementById('cancel')).attr("value"));
    $(document.getElementById('nombre')).val(userData[0][0]);
    $(document.getElementById('apellido-p')).val(userData[0][1]);
    $(document.getElementById('apellido-m')).val(userData[0][2]);
    $(document.getElementById('mail')).val(userData[0][3]);
    $(document.getElementById('pass')).val(userData[0][4]);
    $(document.getElementById('nombre')).attr("disabled", "disabled");
    $(document.getElementById('apellido-p')).attr("disabled", "disabled");
    $(document.getElementById('apellido-m')).attr("disabled", "disabled");
    $(document.getElementById('mail')).attr("disabled", "disabled");
    $(document.getElementById('pass')).attr("disabled", "disabled");
    Materialize.updateTextFields();

}
/*-------------------------------------------
   FUNCIÓN PARA GUARDAR CAMBIOS DE USUARIO
--------------------------------------------*/
function saveUserStatic(pkUsuario) {
    var check = [];
    check = checkValidateStatic();
    if (check[0] == 1) {
        Materialize.toast(check[1], 3000);
    } else {
        kuonyesha(2);
        var userinfo = [];
        userinfo[0] = $(document.getElementById('nombre')).val();
        userinfo[1] = $(document.getElementById('apellido-p')).val();
        userinfo[2] = $(document.getElementById('apellido-m')).val();
        userinfo[3] = $(document.getElementById('mail')).val();
        userinfo[4] = $(document.getElementById('pass')).val();
        var url = "php/Consultas/usuarios.php"; // El script a dónde se realizará la petición.
        var parametro = {
            "accion": "update_usuario",
            "pk_usuario": pkUsuario,
            "user-data": userinfo
        };
        $.ajax({
            type: "POST",
            url: url,
            data: parametro,
            dataType: "JSON",
            success: function (data) {
                if (data.success) {
                    Materialize.toast("¡Exito!, los cambios han sido efectuados", 3000, '', function () {
                        location.reload();
                    });

                } else {
                    Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
                    pkDeleteUser = "null";
                    kuonyesha(3);
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                Materialize.toast("¡Error!, vuelva a intentarlo", 3000);
                pkDeleteUser = "null";
                kuonyesha(3);
            }

        });
    }
}

/*--------------------------------------
   VALIDAR CAMPOS DE USUARIO
--------------------------------------*/
function checkValidateStatic() {
    var regex = "/[w-.]{2,}@([w-]{2,}.)*([w-]{2,}.)[w-]{2,4}/";
    var meta = [];
    if ($(document.getElementById('nombre')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo nombre debe ser llenado";
    } else if ($(document.getElementById('apellido-p')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo apellido paterno debe ser llenado";
    } else if ($(document.getElementById('apellido-m')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo apellido materno debe ser llenado";
    } else if ($(document.getElementById('mail')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo correo electrónico debe ser llenado";
    } else if ($(document.getElementById('mail')).val().indexOf('@', 0) == -1 || $(document.getElementById('mail')).val().indexOf('.', 0) == -1) {
        meta[0] = 1;
        meta[1] = "!Alerta¡, introduzca un correo válido";

    } else if ($(document.getElementById('pass')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo contraseña debe ser llenado";
    } else {
        meta[0] = 0;
        meta[1] = "All right!";

    }
    return meta;
}

/*---------------------------------------
            CARGAR PERIODOS y NOMBRE
---------------------------------------*/
function fatarat() { /* fatarat == periodos en Árabe, función que recibe los periodos correspondientes al programa academico y los muestra en un Select, también muestra  el nombre del programa académico y el nombre del  usuario.*/
    var url = "php/Consultas/consultas.php"; // El script a dónde se realizará la petición.
    var parametro = {
        "accion": "consulta-inicial"
    };
    $.ajax({
        type: "POST",
        url: url,
        data: parametro,
        dataType: "JSON",
        success: function (data) {
            if (data.success) {
                $('#usuario_name').append(data.nombre);
                $('#usuario_name_mobile').append(data.nombre);
                $('#programa_name').text(
                    data.programa
                );

                /*/Rellena el select
                $.each(data.periodos, function (index) {
                    $('#period_ajax').append($('<option>', {
                        value: data.periodos[index][0],
                        text: data.periodos[index][1]
                    }));
                }); */
                /*///////////////////////////////////////////////////////////////////////*/

                var flagSeparador = 0; //Bandera para poner separador de año en lista de periodos
                $.each(data.periodos, function (index) {
                    flagSeparador = 0;
                    var $optgroup = $("<optgroup>", {});
                    $optgroup.empty();
                    $.each(data.periodos[index], function (index2) {
                        if (flagSeparador == 0) {
                            $optgroup.attr("label", data.periodos[index][index2][3]);
                            flagSeparador = 1;
                        }

                        var $a = $("<option>", {
                            class: "waves-effect waves-light left circle",
                            value: data.periodos[index][index2][0]
                        });
                        if (data.periodos[index][index2][4] == 'just_now') {

                        }
                        if (data.periodos[index][index2][2] == '1') {
                            $a.attr("data-icon", "img/numbers/one.svg");
                        } else if (data.periodos[index][index2][2] == '2') {
                            $a.attr("data-icon", "img/numbers/two.svg");
                        } else if (data.periodos[index][index2][2] == '3') {
                            $a.attr("data-icon", "img/numbers/three.svg");
                        } else if (data.periodos[index][index2][2] == '4') {
                            $a.attr("data-icon", "img/numbers/four.svg");
                        } else if (data.periodos[index][index2][2] == '5') {
                            $a.attr("data-icon", "img/numbers/five.svg");
                        } else if (data.periodos[index][index2][2] == '6') {
                            $a.attr("data-icon", "img/numbers/six.svg");
                        }
                        $a.append(data.periodos[index][index2][1]);
                        $optgroup.append($a);

                    });
                    $('#period_ajax').append($optgroup);
                });

                /*////////////////////////////////////////////////////////////////////////*/

                initializzare_select();
                tableau_de_bord($("#period_ajax option:selected").val());

            } else { //Si no se hayarón periodos asociados a este programa académico
                $('#usuario_name').append(data.nombre);
                $('#programa_name').text(
                    data.programa
                );
                $('.select_period').addClass("hide");
                $('.cards').addClass("hide");
                $('.table-div').addClass("hide");
                $('.categories-wrapper').addClass("hide");
                $('.nav-extended').addClass("fill-screen");
                $('.card-info').removeClass("hide");
                $('#info-icon').text("info");
                $('#info-text').text("Nada que mostrar aún..");

                kuonyesha(1);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {

        }

    });
}

/*-----------------------------------
   LLENAR DASHBOARD
-----------------------------------*/
function tableau_de_bord(periodo) { /* tableau de bord == DASHBOARD en frances, función que llena el tablero o dashboard con los datos correspondientes a el periodo especificado*/

    var url = "php/Consultas/consultas.php"; // El script a dónde se realizará la petición.
    var parametros = {
        "accion": "llenar-DashBoard",
        "pk_periodo": periodo,
        "opcion": "1"
    };
    $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        dataType: "JSON",
        success: function (data) {
            if (data.success) {
                //Llenar dashboard
                if (data.estado_periodo == 'just_now') { //Si el periodo esta activo justo ahora y hay registros


                    $('.empty').addClass("hide");
                    $('.cards').removeClass("hide");
                    $('.categories-wrapper').removeClass("hide");
                    $('.nav-extended').removeClass("fill-screen");
                    $('.card-info').addClass("hide");

                    $('#registrados_dash').text(data.registrados);
                    $('#espera_dash').text(data.en_espera);
                    $('#aceptados_dash').text(data.aceptados);
                    $('#espera_text').text("Expediente completo");
                    $('.table-div').removeClass("hide");
                    $('.tab-process').removeClass("hide");
                    $('.tab-waiting').removeClass("hide");
                    fylde_bord(periodo, 1, 0, 0);



                } else if (data.estado_periodo == 'after') { //Si el periodo estuvo activo y hay registros

                    $('.empty').addClass("hide");
                    $('.cards').removeClass("hide");
                    $('.categories-wrapper').removeClass("hide");
                    $('.nav-extended').removeClass("fill-screen");
                    $('.card-info').addClass("hide");
                    $('#registrados_dash').text(data.registrados);
                    $('#espera_dash').text(data.no_aceptados);
                    $('#aceptados_dash').text(data.aceptados);
                    $('.table-div').removeClass("hide");
                    $('#espera_text').text("No aceptados");
                    $('.tab-process').addClass("hide");
                    $('.tab-waiting').addClass("hide");

                    fylde_bord(periodo, 1, 0, 0);


                } else { //Si el periodo estara activo
                    $('.empty').addClass("hide");
                    $('.cards').addClass("hide");
                    $('.table-div').addClass("hide");
                    $('.categories-wrapper').addClass("hide");
                    $('.nav-extended').addClass("fill-screen");
                    $('.card-info').removeClass("hide");
                    $('#info-icon').text("access_time");
                    $('#info-text').text("Periodo aún no activo");
                    kuonyesha(1);
                    kuonyesha(3);
                }


            } else { //Si no ha habido aspirantes registrados u ocurrio un error con el periodo solicitado
                if (data.estado_periodo == 'just_now') {
                    $('.empty').addClass("hide");
                    $('.cards').addClass("hide");
                    $('.table-div').addClass("hide");
                    $('.categories-wrapper').addClass("hide");
                    $('.nav-extended').addClass("fill-screen");
                    $('.card-info').removeClass("hide");
                    $('#info-icon').text("layers_clear");
                    $('#info-text').text("Ningún aspirante registrado aún");

                } else if (data.estado_periodo == 'after') {
                    $('.empty').addClass("hide");
                    $('.cards').addClass("hide");
                    $('.table-div').addClass("hide");
                    $('.categories-wrapper').addClass("hide");
                    $('.nav-extended').addClass("fill-screen");
                    $('.card-info').removeClass("hide");
                    $('#info-icon').text("sentiment_very_dissatisfied");
                    $('#info-text').text("Ningún aspirante registrado!");

                } else if (data.estado_periodo == 'mistake') {
                    $('.empty').addClass("hide");
                    $('.cards').addClass("hide");
                    $('.table-div').addClass("hide");
                    $('.categories-wrapper').addClass("hide");
                    $('.nav-extended').addClass("fill-screen");
                    $('.card-info').removeClass("hide");
                    $('#info-icon').text("adb");
                    $('#info-text').text("Ups! parece que a ocurrido un error");

                }
                kuonyesha(1);
                kuonyesha(3);

            }
        },
        error: function (jqXhr, textStatus, errorThrown) {

        }

    });
}

/*---------------------------------------
            LLENAR TABLA
---------------------------------------*/
function fylde_bord(key_periodo, option, offset, first) { /* fylde bord == Llenar tabla en Danes --- función que llena la tabla de aspirantes mediante la fk de un periodo*/
    if (first == 1) {

        $('.wait').removeClass("hide");
        kuonyesha(2);
    }

    var url = "php/Consultas/consultas.php"; // El script a dónde se realizará la petición.
    var parametros = {
        "accion": "llenar-tabla",
        "pk_periodo": key_periodo,
        "opcion": option, //Filtro de la consulta
        "ac-dc": acdc, //Ascendente o descendente     
        "offset": offset,
        "limit": limit
    };
    $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        dataType: "JSON",
        success: function (datos) { /* Data contendra los registros de los aspirantes pertenecientes a cierto periodo "key_periodo", y filtro "option" indicado */
            if (datos.success) {
                $('#table-body').empty();
                //Por cada aspirante hallado se añade una fila
                $.each(datos.registros, function (index) {
                    var $tr = $("<tr>", {

                        }),
                        $td1 = $("<td>", {
                            text: datos.registros[index][2]
                        }),
                        $td2 = $("<td>", {
                            text: datos.registros[index][3]
                        }),
                        $td3 = $("<td>", {
                            text: datos.registros[index][1]
                        }),
                        $td4 = $("<td>", {

                        }),
                        $a1 = $("<a>", {
                            class: "dropdown-button btn btn-ver-datos red accent-3",
                            href: "#",
                            text: "Ver..."
                        }),
                        $ul = $('<ul>', {
                            class: "dropdown-content",
                            id: datos.registros[index][0] + "drop"
                        }),
                        $li1 = $('<li>', {}),
                        $a2 = $('<a>', {
                            text: "Datos",
                            id: datos.registros[index][0],
                            onclick: "loadAspPage(this.id)"
                        }),
                        $i1 = $("<i>", {
                            class: "material-icons",
                            text: "format_list_bulleted"
                        }),
                        $li2 = $('<li>', {
                            class: "divider"
                        }),
                        $li3 = $('<li>', {}),
                        $a3 = $('<a>', {
                            href: "#",
                            text: "Pago",
                            id: datos.registros[index][0],
                            name: "_file_comprobante_pago",
                            onclick: "popUpModal(this.id,this.name)"

                        }),
                        $i2 = $("<i>", {
                            class: "material-icons",
                            text: "attach_money"
                        });
                    $a1.attr('data-activates', datos.registros[index][0] + "drop");


                    $tr.append($td1);
                    $tr.append($td2);
                    $tr.append($td3);
                    $td4.append($a1);
                    $ul.append($li1.append($a2.append($i1)));
                    $ul.append($li2);
                    $ul.append($li3.append($a3.append($i2)));
                    $td4.append($ul);
                    $tr.append($td4);
                    $('#table-body').append($tr);
                    $('.table-div').removeClass("hide");
                    $('.empty').addClass("hide");

                });
                paginador(key_periodo, option, offset);
                initializzare_dropdown();
            } else {
                $('#table-body').empty();
                $('.table-div').addClass("hide");
                $('.empty').removeClass("hide");

                kuonyesha(3);
                var text;
                if (option == 2) {
                    text = "Ningún aspirante en proceso";
                } else if (option == 3) {
                    text = "Ningún aspirante en espera";
                } else {
                    text = "Ningún aspirante aceptado";
                }
                Materialize.toast(text, 3000, 'rounded');
            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error nos vamos al login

        }

    });
}

/*-----------------------------------
      CREAR PAGINADOR
-----------------------------------*/
function paginador(key_periodo, option, offset) {
    var url = "php/Consultas/consultas.php"; // El script a dónde se realizará la petición.
    var parametros = {
        "accion": "contar-rows",
        "pk_periodo": key_periodo,
        "opcion": option,
        "offset": offset, //Identifica que link estara activo
    };
    $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        dataType: "JSON",
        success: function (data) { /* Data contendra la cantidad de links que serán requeridos para el tab*/
            if (data.success) {
                if (data.rows <= limit) {
                    $('.pagination').addClass("hide");
                    kuonyesha(1);
                    kuonyesha(3);
                } else {
                    $('.pagination').removeClass("hide");
                    $('#paginator').empty();;
                    var $li = $('<li>', {
                            class: "disabled"
                        }),
                        $a = $('<a>', {
                            href: "#a"
                        }),
                        $i = $("<i>", {
                            class: "material-icons",
                            text: "chevron_left"
                        }),
                        $li1 = $('<li>', {
                            class: "disabled"
                        }),
                        $a1 = $('<a>', {
                            href: "#a"
                        }),
                        $i1 = $("<i>", {
                            class: "material-icons",
                            text: "chevron_right"
                        });

                    $('#paginator').append($li.append($a.append($i)));

                    //For que añadira los links y asignara el evento correspondiente a cada link del paginador
                    for (i = 0; i < Math.ceil(data.rows / limit); i++) {
                        var off = i * limit;
                        var $li2 = $('<li>', {}),
                            $a2 = $('<a>', {
                                href: "#",
                                text: i + 1,
                                onclick: "fylde_bord(" + key_periodo + "," + option + "," + off + "," + 1 + ")"
                            });
                        $a2.attr('data-val', i + 1);
                        if ((offset / limit) == i) {
                            $li2.attr('class', "active");

                        } else {

                            $li2.attr('class', "waves-effect");

                        }
                        $('#paginator').append($li2.append($a2));
                    }
                    $('#paginator').append($li1.append($a1.append($i1)));


                    // initializzare_dropdown();
                    kuonyesha(1);
                    kuonyesha(3);
                }

            } else {

            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error nos vamos al login

        }

    });
}




/*----------------------------------
    CONSULTAR ASPIRANTE
----------------------------------*/
function loadAspPage(pk_aspirante) {
    //Carga la página de aspirante
    kuonyesha(2);
    var url = "php/Consultas/consultas.php"; // El script a dónde se realizará la petición.
    var parametros = {
        "accion": "session-aspirante",
        "pk_aspirante": pk_aspirante //Pk del alumno
    };
    $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        dataType: "JSON",
        success: function (data) {
            if (data.created) {
                // location.href = "consulta-aspirante.html";
                kuonyesha(3);
                window.open("consulta-aspirante.html");
                // initializzare_dropdown();
                //$(location).href('consulta-aspirante.html')
                // kuonyesha(3);
            } else {
                kuonyesha(3);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error 

        }

    });
}



/*-----------------------------------
   EVENTO CLICK DESCARGAR PDF
-----------------------------------*/
$("#pdf-link-pago").click(function () {
    var url = "php/Consultas/download-file.php"; // El script a dónde se realizará la petición.
    var parametros = {
        "file": $(this).attr("href")
    };
    $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        dataType: "JSON",
        success: function (data) {
            Materialize.toast('¡Descargado con exito!', 5000);
        },
        error: function (jqXhr, textStatus, errorThrown) {
            Materialize.toast('¡Falló la descarga!', 5000);
        }
    });
});

/*-----------------------------------
   PREVENT DEFAULT a
-----------------------------------*/
$(document).on("click", "a", function (e) {
    e.preventDefault();
})

/*-----------------------------------
   EVENTO MODAL PDF
-----------------------------------*/
/*Evento que llenara el modal con el archivo pdf correspondiente del aspirante*/
function popUpModal(pk_aspirante, documentName) {
    $('.overlay').removeClass("hide");
    kuonyesha(4);
    var url = "php/Consultas/consultas.php"; // El script a dónde se realizará la petición.
    var parametros = {
        "accion": "documento_Exist",
        "documento": documentName,
        "pk_aspirante": pk_aspirante //Pk del alumno
    };
    $.ajax({
        type: "POST",
        url: url,
        data: parametros,
        dataType: "JSON",
        success: function (data) {
            if (data.exist) {

                $("#pdf-object-pago").attr("data", "cuestionarios/PDF/" + pk_aspirante + documentName);
                $("#pdf-link-pago").attr("href", "cuestionarios/PDF/" + pk_aspirante + documentName);
                $("#input-banco").attr("value", data.banco[0]['pago_banco']);
                $("#input-folio").attr("value", data.banco[0]['pago_folio']);
                $("#input-fecha").attr("value", data.banco[0]['pago_fecha']);
                $("#input-referencia").attr("value", data.banco[0]['pago_referencia1']);
                $("#input-expediente").attr("value", data.banco[0]['pago_expediente']);

                kuonyesha(5);
                $('#modal-pdf-pago').modal('open');

            } else {
                kuonyesha(5);
                Materialize.toast('¡El aspirante aún no a proporcionado este documento!', 5000);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error 
            kuonyesha(5);
            Materialize.toast('¡El servidor no responde!', 5000);
        }
    });
}

/*-----------------------------------
   EVENTO PERIODO
-----------------------------------*/
/* Evento encargado de actualizar la información del DASHBOARD y la tabla de aspirantes al cambiar de perido */
$(document).ready(function () {
    $("#period_ajax").change(function () {

        $('.wait').removeClass("hide");
        kuonyesha(2);
        $('.tab-all').addClass("k");
        $('.tab-process').removeClass("k");
        $('.tab-waiting').removeClass("k");
        $('.tab-acepted').removeClass("k");
        tableau_de_bord($("#period_ajax option:selected").val());

    });
});

/*-----------------------------------
   EVENTO ASCENDENTE DESCENDENTE
-----------------------------------*/
$(".ac_dc").change(function () {
    /*Cuando el Botón Switch cambia se efectuan cambios en la variable acdcFlag la cual es utilizada por la función
       tableau_de_bord para llenar la tabla de forma ascendente o descendente*/
    if (acdcFlag == 0) {
        acdcFlag = 1;
    } else {
        acdcFlag = 0;
    }
    if (acdcFlag == 0) {
        acdc = "ac";
    } else {
        acdc = "dc";

    }
    /*Verifica la variable global filtro para determinar en que pestaña de la tabla de consultas se encontaba el usuario despues de cambiar el filtro de ascendente a descendente*/
    if (filtro == 1) {
        $(".tab-all").click();
    } else if (filtro == 2) {
        $(".tab-process").click();
    } else if (filtro == 3) {
        $(".tab-waiting").click();
    } else {
        $(".tab-acepted").click();
    }
});

/*-----------------------------------
     EVENTOS TABS
-----------------------------------*/
/* Eventos encargados de actualizar los datos de tabla de aspirantes de acuerdo  al filtro selecionado */
$(document).ready(function () {
    $(".tab-all").click(function () {
        $('.wait').removeClass("hide");
        kuonyesha(2);
        //Función que llena la tabla
        fylde_bord($("#period_ajax option:selected").val(), 1, 0, 0);
        $('.tab-all').addClass("k");
        $('.tab-process').removeClass("k");
        $('.tab-waiting').removeClass("k");
        $('.tab-acepted').removeClass("k");
        filtro = 1;
    });
    $(".tab-process").click(function () {
        $('.wait').removeClass("hide");
        kuonyesha(2);
        //Función que llena la tabla
        fylde_bord($("#period_ajax option:selected").val(), 2, 0, 0);
        $('.tab-all').removeClass("k");
        $('.tab-process').addClass("k");
        $('.tab-waiting').removeClass("k");
        $('.tab-acepted').removeClass("k");
        filtro = 2;
    });
    $(".tab-waiting").click(function () {
        $('.wait').removeClass("hide");
        kuonyesha(2);
        //Función que llena la tabla
        fylde_bord($("#period_ajax option:selected").val(), 3, 0, 0);
        $('.tab-all').removeClass("k");
        $('.tab-process').removeClass("k");
        $('.tab-waiting').addClass("k");
        $('.tab-acepted').removeClass("k");
        filtro = 3;
    });
    $(".tab-acepted").click(function () {
        $('.wait').removeClass("hide");
        kuonyesha(2);
        //Función que llena la tabla
        fylde_bord($("#period_ajax option:selected").val(), 4, 0, 0);
        $('.tab-all').removeClass("k");
        $('.tab-process').removeClass("k");
        $('.tab-waiting').removeClass("k");
        $('.tab-acepted').addClass("k");
        filtro = 4;
    });
});


/*-----------------------------------
   INICIALIZAR COMPONENTES
-----------------------------------*/
function inizializzare() {
    /* inizializzare == inicializar en Italiano, función que  inicializa ciertos  componentes de algunos frameworks como
       MATERIALIZE Y WOW ANIMATION*/

    /*-----------------------------------
       INIT MODAL
    -----------------------------------*/
    $('#modal-pdf-pago').modal();

    /*-----------------------------------
       INIT WOW ANIMATION
    -----------------------------------*/
    wow = new WOW({
        boxClass: 'wow', // default
        animateClass: 'animated', // default
        offset: 0, // default
        mobile: true, // default
        live: true // default
    })
    new WOW().init();


    /*-------------------------------------
    Iniciamos smoothScroll (Scroll Suave)
    -------------------------------------*/
    smoothScroll.init({
        speed: 1000, // Integer. How fast to complete the scroll in milliseconds
        offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
    });

    /* ---------------------------
      INIT SELECT MATERIALIZE
    ----------------------------*/
    initializzare_select();

    /* ---------------------------
      INIT DROPDOWN MATERIALIZE
    ----------------------------*/
    initializzare_dropdown();

    /* ---------------------------
      INIT MODALES MATERIALIZE
    ----------------------------*/
    $(document).ready(function () {
        $('#modal-pdf').modal();
    });

    $(document).ready(function () {
        $('#modal-evaluar').modal();
    });

    $(document).ready(function () {
        $('#modal-user-data').modal();
    });
    // Initialize collapse button
    $(".button-collapse").sideNav();
    /*----------------------------------
      INIT PUSH SPIN MATERIALIZE
    ----------------------------------*/
    initializzare_pushpin();
}

/*----------------------------------
      INICIAR SELECT MATERIALIZE
----------------------------------*/
function initializzare_select() {
    $(document).ready(function () {
        $('select').material_select();
    });
}

/*----------------------------------
      INICIAR DROPDOWN MATERIALIZE
----------------------------------*/
function initializzare_dropdown() {
    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false, // Does not change width of dropdown to that of the activator
        hover: true, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: false, // Displays dropdown below the button
        alignment: 'left', // Displays dropdown with edge aligned to the left of button
        stopPropagation: false // Stops event propagation
    });
}


/*----------------------------------
      INICIALIZAR PUSHPIN
----------------------------------*/
function initializzare_pushpin() {
    $(document).ready(function () {
        if (screen.width > 992) {
            $('.pin-top').pushpin({
                top: 527,
                offset: 0
            });
        } else if (screen.width > 600) {
            $('.pin-top').pushpin({
                top: 510,
                offset: 0
            });
        } else {
            $('.pin-top').pushpin({
                top: 920,
                offset: 0
            });
        }
    });
}

/*----------------------------------
      MOSTRAR U OCULTAR CONTENIDO
----------------------------------*/
function kuonyesha(almostflag) { /* kuonyesha == mostrar en Suajili, función que oculta o muestra el div de carga que tiene un gif súper cool,también activa o des activa el scroll*/
    if (almostflag == 1) {
        $(document).ready(function () {
            $('.preloader').fadeOut('fast');
            $('body').css({
                'overflow': 'visible'
            });
        });
    } else if (almostflag == 2) {
        $(document).ready(function () {
            $('.wait').removeClass("hide");
            $('.wait').fadeIn('fast');
            $('body').css({
                'overflow': 'hidden'
            });
        });
    } else if (almostflag == 3) {
        $(document).ready(function () {
            $('.wait').fadeOut('fast');
            $('body').css({
                'overflow': 'visible'
            });
        });
    } else if (almostflag == 4) {
        $(document).ready(function () {
            $('.overlay').fadeIn('fast');
            $('body').css({
                'overflow': 'hidden'
            });
        });
    } else if (almostflag == 5) {
        $(document).ready(function () {
            $('.overlay').fadeOut('fast');
            $('body').css({
                'overflow': 'visible'
            });
        });
    }
}
