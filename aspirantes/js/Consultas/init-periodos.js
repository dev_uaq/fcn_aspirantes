/*---------------------------------------
       DEEP MINT WAS HERE  <(°_°)> 
---------------------------------------*/
//VARIABLES GLOBALES
var itemSaved = [];
var periodo;
var refresh = false;
var userLevel = "null";
var userData = [];

/*---------------------------------------
            VALIDAR SESIÓN 
---------------------------------------*/
function Kakunin() { /* Kakunin == Validar en japonés --- función que valida la sesión y hace que el resto de la pagina continue*/
    var url = "php/Consultas/sesion-validate.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: null,
        success: function (data) { /* Data contendra el valor true encaso de que se valide la sesión o nanay en caso contrario a travéz del archivo php validar-sesion */
            if (data.usuario_success) {//Sí hay sesión ejecuta las demás funciones
                
                if (data.level == 'Jefe') {
                    location.href = "usuarios.html";
                }else{
                    userData = data.user_data;
                    $("#nombre").val(data.user_data[0][0]);
                    $("#apellido-p").val(data.user_data[0][1]);
                    $("#apellido-m").val(data.user_data[0][2]);
                    $("#mail").val(data.user_data[0][3]);
                    $("#pass").val(data.user_data[0][4]);
                    $(".save-user").attr("name",data.user_data[0][3]);
                  Materialize.updateTextFields();
                    /*----------------------------------
                      Consulta las ponderaciones para
                      mostrarlas en la interfaz
                    ----------------------------------*/
                    fatarat();
                    inizializzare();
                }                  
    
            } else { //Si no hay sesión nos vamos al login
                //De regreso al login
                location.href = "login.php";
            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error nos vamos al login
            //De regreso al login
            location.href = "login.php";
        }

    });
}
Kakunin(); //Ejecutamos la función Kakunin


/*----------------------------------
   FUNCION PARA MENU DE NAVEGACIÓN
----------------------------------*/
function goToPage(link){
    location.href = link;
}

/*---------------------------------------
            CERRAR SESIÓN 
---------------------------------------*/
function sesionDie() { /*  --- función que aniquila la sesión activa*/
    kuonyesha(2);
    var url = "php/Consultas/sesion-die.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: null,
        success: function (data) {
            if (data.success) {
                //De regreso al login
                location.href = "login.php";
            } else {
                Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
                kuonyesha(3);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error nos vamos al login
            //De regreso al login
            Materialize.toast("¡Error!, vuelva a intentarlo", 3000);
            kuonyesha(3);
        }

    });
}


/*------------------------------------
   FUNCIÓN PARA EDITAR USUARIO
-------------------------------------*/
function editUserStatic() {
    $(document.getElementById('edit')).addClass('hide');
    $(document.getElementById('cancel')).removeClass('hide');
    $(document.getElementById('save')).removeClass("scale-out");
    $(document.getElementById('save')).addClass("scale-in");

    $(document.getElementById('nombre')).removeAttr("disabled");
    $(document.getElementById('apellido-p')).removeAttr("disabled");
    $(document.getElementById('apellido-m')).removeAttr("disabled");
    $(document.getElementById('mail')).removeAttr("disabled");
    $(document.getElementById('pass')).removeAttr("disabled");
}
/*-----------------------------------------------
   FUNCIÓN PARA CANCELAR ACCIÓN EDITAR USUARIO
-----------------------------------------------*/
function cancelUserStatic() {
    $(document.getElementById('cancel')).addClass('hide');
    $(document.getElementById('edit')).removeClass('hide');
    $(document.getElementById('save')).removeClass("scale-in");
    $(document.getElementById('save')).addClass("scale-out");
    $(document.getElementById('save')).addClass("scale-out");
    var index = parseInt($(document.getElementById('cancel')).attr("value"));
    $(document.getElementById('nombre')).val(userData[0][0]);
    $(document.getElementById('apellido-p')).val(userData[0][1]);
    $(document.getElementById('apellido-m')).val(userData[0][2]);
    $(document.getElementById('mail')).val(userData[0][3]);
    $(document.getElementById('pass')).val(userData[0][4]);
    $(document.getElementById('nombre')).attr("disabled", "disabled");
    $(document.getElementById('apellido-p')).attr("disabled", "disabled");
    $(document.getElementById('apellido-m')).attr("disabled", "disabled");
    $(document.getElementById('mail')).attr("disabled", "disabled");
    $(document.getElementById('pass')).attr("disabled", "disabled");
    Materialize.updateTextFields();

}
/*-------------------------------------------
   FUNCIÓN PARA GUARDAR CAMBIOS DE USUARIO
--------------------------------------------*/
function saveUserStatic(pkUsuario) {
    var check = [];
    check = checkValidateStatic();
    if (check[0] == 1) {
        Materialize.toast(check[1], 3000);
    } else {
        kuonyesha(2);
        var userinfo = [];
        userinfo[0] = $(document.getElementById('nombre')).val();
        userinfo[1] = $(document.getElementById('apellido-p')).val();
        userinfo[2] = $(document.getElementById('apellido-m')).val();
        userinfo[3] = $(document.getElementById('mail')).val();
        userinfo[4] = $(document.getElementById('pass')).val();
        var url = "php/Consultas/usuarios.php"; // El script a dónde se realizará la petición.
        var parametro = {
            "accion": "update_usuario",
            "pk_usuario": pkUsuario,
            "user-data": userinfo
        };
        $.ajax({
            type: "POST",
            url: url,
            data: parametro,
            dataType: "JSON",
            success: function (data) {
                if (data.success) {
                    Materialize.toast("¡Exito!, los cambios han sido efectuados", 3000,'',function(){ location.reload();});
                   
                } else {
                    Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
                    pkDeleteUser = "null";
                    kuonyesha(3);
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                Materialize.toast("¡Error!, vuelva a intentarlo", 3000);
                pkDeleteUser = "null";
                kuonyesha(3);
            }

        });
    }
}

/*--------------------------------------
   VALIDAR CAMPOS DE USUARIO
--------------------------------------*/
function checkValidateStatic() {
    var regex = "/[w-.]{2,}@([w-]{2,}.)*([w-]{2,}.)[w-]{2,4}/";
    var meta = [];
    if ($(document.getElementById('nombre')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo nombre debe ser llenado";
    } else if ($(document.getElementById('apellido-p')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo apellido paterno debe ser llenado";
    } else if ($(document.getElementById('apellido-m')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo apellido materno debe ser llenado";
    } else if ($(document.getElementById('mail')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo correo electrónico debe ser llenado";
    } else if ($(document.getElementById('mail')).val().indexOf('@', 0) == -1 || $(document.getElementById('mail')).val().indexOf('.', 0) == -1) {
        meta[0] = 1;
        meta[1] = "!Alerta¡, introduzca un correo válido";

    } else if ($(document.getElementById('pass')).val() == '') {
        meta[0] = 1;
        meta[1] = "!Alerta¡, el campo contraseña debe ser llenado";
    } else {
        meta[0] = 0;
        meta[1] = "All right!";

    }
    return meta;
}



/*---------------------------------------
            CARGAR PERIODOS y NOMBRE
---------------------------------------*/
function fatarat() { /* fatarat == periodos en Árabe, Función que consulta todos los periodos*/
    var url = "php/Consultas/periodos.php"; // El script a dónde se realizará la petición.
    var parametro = {
        "accion": "consulta-periodos"
    };
    $.ajax({
        type: "POST",
        url: url,
        data: parametro,
        dataType: "JSON",
        success: function (data) {
            if (data.success) {
                if (!refresh) {
                    $('#usuario_name').append(data.nombre);
                    $('#programa_name').text(
                        data.programa
                    );

                } else {
                    $('#collection-periodos').empty();
                    itemSaved = null;
                }
                itemSaved = data.periodos;
                var flagSeparador = 0; //Bandera para poner separador de año en lista de periodos
                $.each(data.periodos, function (index) {
                    flagSeparador = 0;
                    $.each(data.periodos[index], function (index2) {

                        if (flagSeparador == 0) {
                            var $li_year = $("<li>", {
                                    class: "collection-header"
                                }),
                                $h5_year = $("<h5>", {
                                    text: data.periodos[index][index2][3]
                                });
                            $('#collection-periodos').append($li_year.append($h5_year));
                            flagSeparador = 1;
                        }

                        var $a = $("<a>", {
                                class: "collection-item waves-effect waves-light",
                                value: index,
                                name: index2,
                                id: data.periodos[index][index2][0],
                                onclick: "popUpModal(this.id)"
                            }),
                            $div_chip = $("<div>", {
                                class: "chip"
                            }),
                            $icon = $("<i>", {
                                class: "material-icons"
                            });
                        if (data.periodos[index][index2][4] == 'just_now') {
                            var $span = $("<span>", {
                                class: "new badge red"
                            });
                            $span.attr("data-badge-caption", "Activo");
                            $a.append($span);
                        }
                        if (data.periodos[index][index2][2] == '1') {
                            $icon.append("looks_one");
                            $div_chip.append($icon);
                            $div_chip.append("Periodo");

                        } else if (data.periodos[index][index2][2] == '2') {
                            $icon.append("looks_two");
                            $div_chip.append($icon);
                            $div_chip.append("Periodo");

                        } else if (data.periodos[index][index2][2] == '3') {
                            $icon.append("looks_3");
                            $div_chip.append($icon);
                            $div_chip.append("Periodo");

                        } else if (data.periodos[index][index2][2] == '4') {
                            $icon.append("looks_4");
                            $div_chip.append($icon);
                            $div_chip.append("Periodo");

                        } else if (data.periodos[index][index2][2] == '5') {
                            $icon.append("looks_5");
                            $div_chip.append($icon);
                            $div_chip.append("Periodo");

                        } else if (data.periodos[index][index2][2] == '6') {
                            $icon.append("looks_6");
                            $div_chip.append($icon);
                            $div_chip.append("Periodo");

                        } else {
                            $div_chip.append(data.periodos[index][index2][2] + " Periodo");
                        }
                        $a.append($div_chip);
                        $a.append(data.periodos[index][index2][1]);
                        $('#collection-periodos').append($a);

                    });

                });
                kuonyesha(3);
            } else { //Si no se hayarón periodos asociados a este programa académicp
                $('#usuario_name').append(data.nombre);
                $('#programa_name').text(
                    data.programa
                );
                 $('.card-info').removeClass("hide");
                $('#info-icon').text("info");
                $('#info-text').text("Nada que mostrar aún..");
                kuonyesha(3);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {

        }

    });
}


/*------------------------------------
   FUNCIÓN PARA ACTUALIZAR PAGÍNA
-------------------------------------*/
/*Función que actualizara el contenido de la página despues de haber realizado algún cambio o haber insertado un nuevo registro*/
function updatePage() {
    kuonyesha(2);
    refresh = true;
    $(".li-consultar-periodos").click();
    fatarat();
}
/*-----------------------------------
   EVENTO MODAL PDF
-----------------------------------*/
/*Evento que llenara el modal con el archivo pdf correspondiente del aspirante*/
function popUpModal(pk_periodo) {
    periodo = pk_periodo;
    index1 = $("#" + pk_periodo).attr("value");
    index2 = $("#" + pk_periodo).attr("name");
    $('#collection-modal').empty();
    var $li_year = $("<li>", {
            class: "collection-header"
        }),
        $h5_year = $("<h5>", {
            text: itemSaved[index1][index2][3]
        });
    $('#collection-modal').append($li_year.append($h5_year));

    var $a = $("<a>", {
            class: "collection-item waves-effect waves-light",
            id: itemSaved[index1][index2][0],
            onclick: "popUpModal(this.id)"
        }),
        $div_chip = $("<div>", {
            class: "chip"
        }),
        $icon = $("<i>", {
            class: "material-icons"
        });
    if (itemSaved[index1][index2][4] == 'just_now') {
        var $span = $("<span>", {
            class: "new badge red"
        });
        $span.attr("data-badge-caption", "Activo");
        $a.append($span);
    }
    if (itemSaved[index1][index2][2] == '1') {
        $icon.append("looks_one");
        $div_chip.append($icon);
        $div_chip.append("Periodo");

    } else if (itemSaved[index1][index2][2] == '2') {
        $icon.append("looks_two");
        $div_chip.append($icon);
        $div_chip.append("Periodo");

    } else if (itemSaved[index1][index2][2] == '3') {
        $icon.append("looks_3");
        $div_chip.append($icon);
        $div_chip.append("Periodo");

    } else if (itemSaved[index1][index2][2] == '4') {
        $icon.append("looks_4");
        $div_chip.append($icon);
        $div_chip.append("Periodo");

    } else {
        $div_chip.append(itemSaved[index1][index2][2] + " Periodo");
    }
    $a.append($div_chip);
    $a.append(itemSaved[index1][index2][1]);
    $('#collection-modal').append($a);

    $('#modalPeriodo').modal('open');

}

/*--------------------------------------
   ACTUALIZAR FECHA FINAL DE PERIODO
--------------------------------------*/
function checkDate() {
    var flafggie = 0;
    if ($('#buildDateFin').val() == '') {
        flafggie = 1;
    } else {

    }
    return flafggie;
}

$('#updatePeriodo').submit(function () {
    if (checkDate() == 1) {
        Materialize.toast("¡Alerta!, Debes establecer una fecha de finalización", 3000);
    } else {
        kuonyesha(4);
        var url = "php/Consultas/periodos.php"; // El script a dónde se realizará la petición.
        var parametro = {
            "accion": "update_periodo",
            "pk_periodo": periodo,
            "new_date": $('#buildDateFin').val()
        };
        $.ajax({
            type: "POST",
            url: url,
            data: parametro,
            dataType: "JSON",
            success: function (data) {
                if (data.success) {
                    if (data.able) {
                        updatePage();
                        $('#modalPeriodo').modal('close');
                        $('#buildDateFin').val("");
                        Materialize.toast("¡Exito!, Periodo actualizado", 3000);
                        kuonyesha(5);
                    } else {
                        Materialize.toast("¡Alerta!, La fecha final debe ser mayor a la inicial", 3000);
                        kuonyesha(5);
                    }
                } else {
                    Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
                    kuonyesha(5);
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {

            }

        });
    }
    return false;
});

/*--------------------------------------
   AÑADIR PERIODO
--------------------------------------*/
function checkDate2() { //Función que verifica campos no vacios y fechas correctas
    var flafggie = [];
    flafggie[0] = 0;
    flafggie[1] = 0;
    flafggie[2] = 0;
    if ($('#fecha-inicio').val() == '') {
        flafggie[0] = 1;
    } else {

    }
    if ($('#fecha-fin').val() == '') {
        flafggie[1] = 1;
    } else { //Validación de fecha final más  grande que la fecha inicial
        var splitFechaInicio = $('#fecha-inicio').val().split(' ');
        var splitFechaFin = $('#fecha-fin').val().split(' ');


        if (parseInt(splitFechaInicio[2]) <= parseInt(splitFechaFin[2])) {
            if (parseInt(sacarNúmeroMes(splitFechaInicio[1])) < parseInt(sacarNúmeroMes(splitFechaFin[1]))) {
                //good
            } else if (parseInt(sacarNúmeroMes(splitFechaInicio[1])) == parseInt(sacarNúmeroMes(splitFechaFin[1]))) {
                if (parseInt(splitFechaInicio[0]) < parseInt(splitFechaFin[0])) {
                    //good
                } else {
                    flafggie[2] = 1;
                }
            } else {
                flafggie[2] = 1;
            }
        } else {
            flafggie[2] = 1;
        }
    }
    return flafggie;
}

$('#addPeriodo').submit(function () {
    if (checkDate2()[0] == 1) {
        Materialize.toast("¡Alerta!, Debes establecer una fecha de inicio", 3000);
    } else if (checkDate2()[1] == 1) {
        Materialize.toast("¡Alerta!, Debes establecer una fecha de finalización", 3000);
    } else if (checkDate2()[2] == 1) {
        Materialize.toast("¡Alerta!, La fecha de finalización debe ser mayor a la inicial", 3000);
    } else {
        kuonyesha(4);
        var url = "php/Consultas/periodos.php"; // El script a dónde se realizará la petición.
        var parametro = {
            "accion": "add_periodo",
            "pk_periodo": periodo,
            "inicio": $('#fecha-inicio').val(),
            "fin": $('#fecha-fin').val()
        };
        $.ajax({
            type: "POST",
            url: url,
            data: parametro,
            dataType: "JSON",
            success: function (data) {
                if (data.success) {
                    updatePage();
                    $('#fecha-inicio').val("");
                    $('#fecha-fin').val("");
                    Materialize.toast("¡Exito!, Periodo añadido", 3000);
                    kuonyesha(5);
                } else {
                    Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
                    kuonyesha(5);
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {

            }

        });
    }
    return false;
});

/*-----------------------------------
   Obtener número del mes
-----------------------------------*/
//Función para obtener el número del mes con el nombre del mes
function sacarNúmeroMes(mes) {
    var textMes;
    switch (mes) {
        case "Enero":
            textMes = 1;
            break;
        case "Febrero":
            textMes = 2;
            break;
        case "Marzo":
            textMes = 3;
            break;
        case "Abril":
            textMes = 4;
            break;
        case "Mayo":
            textMes = 5;
            break;
        case "Junio":
            textMes = 6;
            break;
        case "Julio":
            textMes = 7;
            break;
        case "Agosto":
            textMes = 8;
            break;
        case "Septiembre":
            textMes = 9;
            break;
        case "Octubre":
            textMes = 10;
            break;
        case "Noviembre":
            textMes = 11;
            break;
        case "Diciembre":
            textMes = 12;
            break;

    }
    return textMes;
}

/*-----------------------------------
   INICIALIZAR COMPONENTES
-----------------------------------*/
function inizializzare() {
    /* inizializzare == inicializar en Italiano, función que  inicializa ciertos  componentes de algunos frameworks como
       MATERIALIZE Y WOW ANIMATION*/
    $(".nuevo-periodo").removeClass("hide");
    $(".nuevo-periodo").fadeOut(50);
    /*-----------------------------------
       INIT MODAL
    -----------------------------------*/
    $(document).ready(function () {
        // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
        $('.modal').modal();
    });

    /*-----------------------------------
       INIT WOW ANIMATION
    -----------------------------------*/
    wow = new WOW({
        boxClass: 'wow', // default
        animateClass: 'animated', // default
        offset: 0, // default
        mobile: true, // default
        live: true // default
    })
    new WOW().init();


    /*-------------------------------------
    Iniciamos smoothScroll (Scroll Suave)
    -------------------------------------*/
    smoothScroll.init({
        speed: 1000, // Integer. How fast to complete the scroll in milliseconds
        offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
    });

    /*---------------------------------
      INIT DATEPICKER MATERIALIZE
  ----------------------------------*/

    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 10, // Creates a dropdown of 15 years to control year,
        today: 'Hoy',
        clear: 'Limpiar',
        close: 'Ok',
        closeOnSelect: false // Close upon selecting a date,
    });

    /* ---------------------------
      INIT SELECT MATERIALIZE
    ----------------------------*/
    initializzare_select();

    /* ---------------------------
      INIT DROPDOWN MATERIALIZE
    ----------------------------*/
    initializzare_dropdown();

    /* ---------------------------
      INIT MODALES MATERIALIZE
    ----------------------------*/
    $(document).ready(function () {
        $('#modal-pdf').modal();
    });

    $(document).ready(function () {
        $('#modal-evaluar').modal();
    });
  $(document).ready(function () {
        $('#modal-user-data').modal();
    });
// Initialize collapse button
  $(".button-collapse").sideNav();


    /*----------------------------------
      INIT PUSH SPIN MATERIALIZE
    ----------------------------------*/
    initializzare_pushpin();
}

/*----------------------------------
      INICIAR SELECT MATERIALIZE
----------------------------------*/
function initializzare_select() {
    $(document).ready(function () {
        $('select').material_select();
    });
}

/*----------------------------------
      INICIAR DROPDOWN MATERIALIZE
----------------------------------*/
function initializzare_dropdown() {
    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false, // Does not change width of dropdown to that of the activator
        hover: true, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: false, // Displays dropdown below the button
        alignment: 'left', // Displays dropdown with edge aligned to the left of button
        stopPropagation: false // Stops event propagation
    });
}


/*----------------------------------
      INICIALIZAR PUSHPIN
----------------------------------*/
function initializzare_pushpin() {
    $(document).ready(function () {
        if (screen.width > 992) {
            $('.pin-top').pushpin({
                top: 527,
                offset: 0
            });
        } else if (screen.width > 600) {
            $('.pin-top').pushpin({
                top: 510,
                offset: 0
            });
        } else {
            $('.pin-top').pushpin({
                top: 920,
                offset: 0
            });
        }
    });
}

/*--------------------------------------
    EVITAR ONCLICK POR DEFAULT EN LINK
 -------------------------------------*/
$(document).on("click", "a", function (e) {
    e.preventDefault();
})
/*--------------------------------------
TAB HECHO EN CHINA
---------------------------------------*/
$(document).ready(function () {
    $(".li-nuevo-periodo").click(function () {
        $(".consultar-periodos").hide(10);
        $(".nuevo-periodo").show("slow");
        $(".li-consultar-periodos").removeClass("k", 1000, "easeInBack");
        $(".li-nuevo-periodo").addClass("k", 1000, "easeInBack");

    });
    $(".li-consultar-periodos").click(function () {
        $(".nuevo-periodo").hide(10);
        $(".consultar-periodos").show("slow");
        $(".li-consultar-periodos").addClass("k", 1000, "easeInBack");
        $(".li-nuevo-periodo").removeClass("k", 1000, "easeInBack");

    });
});

/*----------------------------------
      MOSTRAR U OCULTAR CONTENIDO
----------------------------------*/
function kuonyesha(almostflag) { /* kuonyesha == mostrar en Suajili, función que oculta o muestra el div de carga que tiene un gif súper cool,también activa o des activa el scroll*/
    if (almostflag == 2) {
        $(document).ready(function () {
            $('.wait').removeClass('hide');
            $('.wait').fadeIn('fast');
            $('body').css({
                'overflow': 'hidden'
            });
        });
    } else if (almostflag == 3) {
        $(document).ready(function () {
            $('.wait').fadeOut('fast');
            $('body').css({
                'overflow': 'visible'
            });
        });
    } else if (almostflag == 4) {
        $(document).ready(function () {
            $('.overlay').removeClass("hide");
            $('.overlay').fadeIn('fast');
            $('body').css({
                'overflow': 'hidden'
            });
        });
    } else if (almostflag == 5) {
        $(document).ready(function () {
            $('.overlay').fadeOut('fast');
            $('body').css({
                'overflow': 'visible'
            });
        });
    }
}
