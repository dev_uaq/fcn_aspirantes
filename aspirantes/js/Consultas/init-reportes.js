/*---------------------------------------
       DEEP MINT WAS HERE  <(°_°)> 
---------------------------------------*/
//VARIABLES GLOBALES

/*---------------------------------------
            VALIDAR SESIÓN 
---------------------------------------*/
function Kakunin() { /* Kakunin == Validar en japonés --- función que valida la sesión y hace que el resto de la pagina continue*/
    var url = "php/Consultas/sesion-validate.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: null,
        success: function (data) { /* Data contendra el valor true encaso de que se valide la sesión o nanay en caso contrario a travéz del archivo php validar-sesion */
            if (data.success) {
                fatarat(data.level);
            } else { //Si no hay sesión nos vamos al login
                //De regreso al login
                location.href = "login.php";
            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error nos vamos al login
            //De regreso al login
            location.href = "login.php";
        }

    });
}
Kakunin(); //Ejecutamos la función Kakunin

/*---------------------------------------
            CERRAR SESIÓN 
---------------------------------------*/
function sesionDie() { /*  --- función que aniquila la sesión activa*/
    kuonyesha(2);
    var url = "php/Consultas/sesion-die.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: null,
        success: function (data) {
            if (data.success) {
                //De regreso al login
                location.href = "login.php";
            } else {
                Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
                kuonyesha(3);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) { //Si ocurre un error nos vamos al login
            //De regreso al login
            Materialize.toast("¡Error!, vuelva a intentarlo", 3000);
            kuonyesha(3);
        }

    });
}


/*--------------------------------------------
    CARGAR PERIODOS y NOMBRE DEL PROGRAMA
---------------------------------------------*/
function fatarat(level) { /* fatarat == periodos en Árabe, Función que consulta todos los periodos*/
    var url = "php/Consultas/reportes.php"; // El script a dónde se realizará la petición.
    var parametro = {
        "accion": "consultar-periodos"
    };
    $.ajax({
        type: "POST",
        url: url,
        data: parametro,
        dataType: "JSON",
        success: function (data) {
            if (data.success) {
                $('#usuario_name').append(data.nombre);
                $('#programa_name').text(
                    data.programa
                );
                /*///////////////////////////////////////////////////////////////////////*/

                var flagSeparador = 0; //Bandera para poner separador de año en lista de periodos
                $.each(data.periodos, function (index) {
                    flagSeparador = 0;
                    var $optgroup = $("<optgroup>", {});
                    $optgroup.empty();
                    $.each(data.periodos[index], function (index2) {
                        if (flagSeparador == 0) {
                            $optgroup.attr("label", data.periodos[index][index2][3]);
                            flagSeparador = 1;
                        }

                        var $a = $("<option>", {
                            class: "waves-effect waves-light left circle",
                            value: data.periodos[index][index2][0]
                        });
                        if (data.periodos[index][index2][4] == 'just_now') {

                        }
                        if (data.periodos[index][index2][2] == '1') {
                            $a.attr("data-icon", "img/numbers/one.svg");
                        } else if (data.periodos[index][index2][2] == '2') {
                            $a.attr("data-icon", "img/numbers/two.svg");
                        } else if (data.periodos[index][index2][2] == '3') {
                            $a.attr("data-icon", "img/numbers/three.svg");
                        } else if (data.periodos[index][index2][2] == '4') {
                            $a.attr("data-icon", "img/numbers/four.svg");
                        } else if (data.periodos[index][index2][2] == '5') {
                            $a.attr("data-icon", "img/numbers/five.svg");
                        } else if (data.periodos[index][index2][2] == '6') {
                            $a.attr("data-icon", "img/numbers/six.svg");
                        }
                        $a.append(data.periodos[index][index2][1]);
                        $optgroup.append($a);

                    });
                    $('#period_ajax').append($optgroup);
                });
                kuonyesha(3);
                /*////////////////////////////////////////////////////////////////////////*/
                initializzare_select();
                reportes($("#period_ajax option:selected").val());

            } else {
                $('#usuario_name').append(data.nombre);
                kuonyesha(3);
            }
        },
        error: function (jqXhr, textStatus, errorThrown) {
            Materialize.toast("¡Falló!, vuelva a intentarlo", 3000);
            kuonyesha(3);
        }

    });


}
/*-----------------------------------
   EVENTO PERIODO
-----------------------------------*/
//Evento encargado de actualizar la información al cambiar de perido 
$(document).ready(function () {
    $("#period_ajax").change(function () {

        $('.wait').removeClass("hide");
        kuonyesha(2);
        reportes($("#period_ajax option:selected").val());

    });
});
/*--------------------------------------------
    CARGAR REPORTES
---------------------------------------------*/
function reportes(fkPeriodo) {
//    grafica de sexo
    var chart = AmCharts.makeChart("chartSexo", {
        "type": "pie",
        "theme": "light",
        "dataLoader": {
            "url": "php/graficas.php?funcion=sexo&periodo="+fkPeriodo,
            "format": "json"
        },
        "valueField": "cantidad",
        "titleField": "sexo",
        "balloon": {
            "fixedPosition": false
        },
        "outlineAlpha": 0.4,
        "depth3D": 15,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "angle": 30,
        "export": {
            "enabled": true
        }
    });
    
//    grafica de universidad
     var chart = AmCharts.makeChart("chartUniversidad", {
        "theme": "light",
        "type": "serial",
        "startDuration": 2,
        "dataLoader": {
            "url": "php/graficas.php?funcion=universidades&periodo="+fkPeriodo,
            "format": "json"
        },
        "valueAxes": [{
            "position": "left",
            "title": "Aspirantes"
        }],
        "graphs": [{
            "balloonText": "[[category]]: <b>[[value]]</b>",
            "fillColorsField": "color",
            "fillAlphas": 1,
            "lineAlpha": 0.1,
            "type": "column",
            "valueField": "Aspirantes"
        }],
        "depth3D": 20,
        "angle": 30,
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        }, 
        "categoryField": "Universidad",
        "categoryAxis": {
            "autoWrap": true, 
            "inside": true,
            "gridPosition": "start",
            "labelRotation": 80
        },
        "export": {
            "enabled": true
        }

    });
    
    //    grafica de nacionalidad
     var chart = AmCharts.makeChart("chartNacionalidad", {
        "theme": "light",
        "type": "serial",
        "startDuration": 2,
        "dataLoader": {
            "url": "php/graficas.php?funcion=nacionalidad&periodo="+fkPeriodo,
            "format": "json"
        },
        "valueAxes": [{
            "position": "left",
            "title": "Aspirantes"
        }],
        "graphs": [{
            "balloonText": "[[category]]: <b>[[value]]</b>",
            "fillColorsField": "color",
            "fillAlphas": 1,
            "lineAlpha": 0.1,
            "type": "column",
            "valueField": "Aspirantes"
        }],
        "depth3D": 20,
        "angle": 30,
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "Pais",
        "categoryAxis": {
            "gridPosition": "start",
            "labelRotation": 90
        },
        "export": {
            "enabled": true
        }

    });

//    grafica de resultads
    var chart = AmCharts.makeChart("chartResultados", {
        "type": "pie",
        "theme": "none",
        "dataLoader": {
            "url": "php/graficas.php?funcion=resultados&periodo="+fkPeriodo,
            "format": "json"
        },
        "valueField": "Aspirantes",
        "titleField": "Estado",
        "balloon": {
            "fixedPosition": false
        },
        "outlineAlpha": 0.4,
        "depth3D": 15,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "angle": 30,
        "export": {
            "enabled": true
        }
    });
    
    //    grafica de modalidad
     var chart = AmCharts.makeChart("chartModalidad", {
        "theme": "light",
        "type": "serial",
        "startDuration": 2,
        "dataLoader": {
            "url": "php/graficas.php?funcion=modalidad&periodo="+fkPeriodo,
            "format": "json"
        },
        "valueAxes": [{
            "position": "left",
            "title": "Aspirantes"
        }],
        "graphs": [{
            "balloonText": "[[category]]: <b>[[value]]</b>",
            "fillColorsField": "color",
            "fillAlphas": 1,
            "lineAlpha": 0.1,
            "type": "column",
            "valueField": "Aspirantes"
        }],
        "depth3D": 20,
        "angle": 30,
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "Modalidad",
        "categoryAxis": {
            "gridPosition": "start",
            "labelRotation": 90
        },
        "export": {
            "enabled": true
        }

    });
    
    kuonyesha(3);

}



/*-----------------------------------
   INICIALIZAR COMPONENTES
-----------------------------------*/
function inizializzare() {
    /* inizializzare == inicializar en Italiano, función que  inicializa ciertos  componentes de algunos frameworks como
       MATERIALIZE Y WOW ANIMATION*/



    /*-----------------------------------
       INIT WOW ANIMATION
    -----------------------------------*/
    wow = new WOW({
        boxClass: 'wow', // default
        animateClass: 'animated', // default
        offset: 0, // default
        mobile: true, // default
        live: true // default
    })
    new WOW().init();


    /*-------------------------------------
    Iniciamos smoothScroll (Scroll Suave)
    -------------------------------------*/
    smoothScroll.init({
        speed: 1000, // Integer. How fast to complete the scroll in milliseconds
        offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
    });



    /* ---------------------------
      INIT SELECT MATERIALIZE
    ----------------------------*/
    initializzare_select();

    /* ---------------------------
      INIT DROPDOWN MATERIALIZE
    ----------------------------*/
    initializzare_dropdown();

    /* ---------------------------
      INIT MODALES MATERIALIZE
    ----------------------------*/
    $(document).ready(function () {
        $('#modal-user-data').modal();
    });


    /*----------------------------------
      INIT PUSH SPIN MATERIALIZE
----------------------------------*/
    initializzare_pushpin();
}

/*----------------------------------
      INICIAR SELECT MATERIALIZE
----------------------------------*/
function initializzare_select() {
    $(document).ready(function () {
        $('select').material_select();
    });
}

/*----------------------------------
      INICIAR DROPDOWN MATERIALIZE
----------------------------------*/
function initializzare_dropdown() {
    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false, // Does not change width of dropdown to that of the activator
        hover: true, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: false, // Displays dropdown below the button
        alignment: 'left', // Displays dropdown with edge aligned to the left of button
        stopPropagation: false // Stops event propagation
    });
}


/*----------------------------------
      INICIALIZAR PUSHPIN
----------------------------------*/
function initializzare_pushpin() {
    $(document).ready(function () {
        if (screen.width > 992) {
            $('.pin-top').pushpin({
                top: 527,
                offset: 0
            });
        } else if (screen.width > 600) {
            $('.pin-top').pushpin({
                top: 510,
                offset: 0
            });
        } else {
            $('.pin-top').pushpin({
                top: 920,
                offset: 0
            });
        }
    });
}

/*--------------------------------------
    EVITAR ONCLICK POR DEFAULT EN LINK
 -------------------------------------*/
$(document).on("click", "a", function (e) {
    e.preventDefault();
})


/*----------------------------------
      MOSTRAR U OCULTAR CONTENIDO
----------------------------------*/
function kuonyesha(almostflag) { /* kuonyesha == mostrar en Suajili, función que oculta o muestra el div de carga que tiene un gif súper cool,también activa o des activa el scroll*/
    if (almostflag == 2) {
        $(document).ready(function () {
            $('.wait').fadeIn('fast');
            $('body').css({
                'overflow': 'hidden'
            });
        });
    } else if (almostflag == 3) {
        $(document).ready(function () {
            $('.wait').fadeOut('fast');
            $('body').css({
                'overflow': 'visible'
            });
        });
    } else if (almostflag == 4) {
        $(document).ready(function () {
            $('.overlay').removeClass("hide");
            $('.overlay').fadeIn('fast');
            $('body').css({
                'overflow': 'hidden'
            });
        });
    } else if (almostflag == 5) {
        $(document).ready(function () {
            $('.overlay').fadeOut('fast');
            $('body').css({
                'overflow': 'visible'
            });
        });
    }
}
